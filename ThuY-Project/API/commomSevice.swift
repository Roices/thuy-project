//
//  commomSevice.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 18/05/2022.
//

import Foundation
import UIKit
import Alamofire
//
class commomSevice {
    static let BaseDomain = "http://10.10.104.214:8083/mock3-backend/api/v1/"
    
    func getApiCustome(page: String, size: String, completionHandler: @escaping ([String : Any])->(),failure:@escaping (String)->()) {
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["page": page, "size": size]  as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)customer", params: params, headerXtra:headerXtra, meThod: .GetApi, completionHandler: { (response) in
            completionHandler(response)
        }) { (error) in
            failure(error.localizedDescription)
        }
    }
    
    func getApiPets(page: String, size: String, completionHandler: @escaping ([String : Any])->(),failure:@escaping (String)->()) {
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["page": page, "size": size]  as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)pet", params: params,headerXtra:headerXtra, meThod: .GetApi, completionHandler: { (response) in
            completionHandler(response)
        }) { (error) in
            failure(error.localizedDescription)
        }
    }
    
    func getApiBreed(page: String, size: String, completionHandler: @escaping ([String : Any])->(),failure:@escaping (String)->()) {
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["page": page, "size": size]  as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)species", params: params,headerXtra:headerXtra, meThod: .GetApi, completionHandler: { (response) in
            completionHandler(response)
        }) { (error) in
            failure(error.localizedDescription)
        }
    }
    
    func getApiSpecies(page: String, size: String, completionHandler: @escaping ([String : Any])->(),failure:@escaping (String)->()) {
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["page": page, "size": size]  as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)type", params: params,headerXtra:headerXtra, meThod: .GetApi, completionHandler: { (response) in
            completionHandler(response)
        }) { (error) in
            failure(error.localizedDescription)
        }
    }
    
    func getApiSpeciesSearch(page: String, size: String,name: String, completionHandler: @escaping ([String : Any])->(),failure:@escaping (String)->()) {
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["page": page, "size": size ,"name": name]  as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)type/search", params: params,headerXtra:headerXtra, meThod: .GetApi, completionHandler: { (response) in
            completionHandler(response)
        }) { (error) in
            failure(error.localizedDescription)
        }
    }
    
    func getApiCustomerSearch(page: String, size: String,name: String, completionHandler: @escaping ([String : Any])->(),failure:@escaping (String)->()) {
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        guard let idBranch = UserDefaults.standard.string(forKey: "branchID") else {return}
        let params = ["page": page, "size": size ,"name": name, "branch": idBranch]  as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)customer/search", params: params,headerXtra:headerXtra, meThod: .GetApi, completionHandler: { (response) in
            completionHandler(response)
        }) { (error) in
            failure(error.localizedDescription)
        }
    }
    
    func getApiPetSearch(page: String, size: String,name: String, completionHandler: @escaping ([String : Any])->(),failure:@escaping (String)->()) {
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["page": page, "size": size ,"name": name]  as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)pet/search", params: params,headerXtra:headerXtra, meThod: .GetApi, completionHandler: { (response) in
            completionHandler(response)
        }) { (error) in
            failure(error.localizedDescription)
        }
    }
    
    func getApiBreedSearch(page: String, size: String,name: String, completionHandler: @escaping ([String : Any])->(),failure:@escaping (String)->()) {
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["page": page, "size": size ,"name": name]  as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)species/search", params: params,headerXtra:headerXtra, meThod: .GetApi, completionHandler: { (response) in
            completionHandler(response)
        }) { (error) in
            failure(error.localizedDescription)
        }
    }
    
    func postApiSpecies(name: String, note: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["name": name, "note": note] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpBodyRequestAPI(url: "\(commomSevice.BaseDomain)type", params: params, headerXtra: headerXtra , meThod: .PostApi) { response in
//            SpeciesViewController.sharedInstance.getSpeciesDataModel(page: 1, size: 20)
        } failure: { response in
        }
    }

    func putApiSpecies(id: String, name: String, note: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["name": name, "note": note] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpBodyRequestAPI(url: "\(commomSevice.BaseDomain)type/\(id)", params: params, headerXtra: headerXtra, meThod: .PutApi) { response in
        } failure: { response in
        }
    }
    
    func deleteApiSpecies(id: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["": nil] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)type/\(id)", params: params, headerXtra : headerXtra, meThod: .DeleteApi) { response in
        } failure: { response in
        }
    }
    
    func postApiBreed(name: String, note: String , id: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["name": name, "note": note, "typeId": id] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpBodyRequestAPI(url: "\(commomSevice.BaseDomain)species", params: params, headerXtra: headerXtra, meThod: .PostApi) { response in
  
        } failure: { response in
            
        }
    }
    
    func putApiBreed(id: String, name: String, note: String, idSpecies: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["name": name, "note": note, "typeId": idSpecies] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpBodyRequestAPI(url: "\(commomSevice.BaseDomain)species/\(id)", params: params, headerXtra: headerXtra, meThod: .PutApi) { response in
        } failure: { response in
        }
    }
    
    func deleteApiBreed(id: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["": nil] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)species/\(id)", params: params, headerXtra: headerXtra, meThod: .DeleteApi) { response in
        } failure: { response in
        }
    }
    
    func postApiCustomer(name: String, address: String, phoneNumber: String, email: String,  type: Int,  idBranch: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["name": name, "address": address, "phoneNumber": phoneNumber, "email": email, "type": type, "branches": [idBranch]] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpBodyRequestAPI(url: "\(commomSevice.BaseDomain)customer", params: params, headerXtra: headerXtra, meThod: .PostApi) { response in
  
        } failure: { response in
            
        }
    }
    
    func postApiPets(name: String, Idsex: Int, birthCerti: String, note: String, breed: String, customerId: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let paramIdBreed = [breed]
        let params = ["name": name , "sex": Idsex, "birthCerti": "", "note": note, "species": paramIdBreed, "customerId": customerId] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)pet", params: params, headerXtra: headerXtra, meThod: .PostApi) { response in
        } failure: { response in
    
        }
    }
    
    func postApiPetFormData(name: String, Idsex: Int, birthCerti: Data, note: String, breed: String, customerId: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["name": name , "sex": Idsex,  "note": note, "species": breed, "customerId": customerId] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        
        LotusAPIService.sharedInstance.httpRequestUploadAPI(url: "\(commomSevice.BaseDomain)pet", params: params, dataImage: birthCerti, headerXtra: headerXtra, meThod: .PostApi) { response in
            
        } failure: { response in
            
        }

    }
    
    func putApiCustomer(idCustomer: String,name: String, address: String, phoneNumber: String, email: String, type: Int, idBranch: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["name": name, "address": address, "phoneNumber": phoneNumber, "email": email, "type": type, "branches": [idBranch]] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpBodyRequestAPI(url: "\(commomSevice.BaseDomain)customer/\(idCustomer)", params: params, headerXtra: headerXtra, meThod: .PutApi) { response in
        } failure: { response in
            
        }
    }
    
    func putApiPets(idPet: String,name: String, sex: Int, birthCerti: String, note: String, customerId: String, idBreed: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let paramIdBreed = [idBreed]
        let params = ["name": name, "sex": sex, "birthCerti": birthCerti, "note": note, "customerId": customerId, "species": paramIdBreed] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpBodyRequestAPI(url: "\(commomSevice.BaseDomain)pet/\(idPet)", params: params, headerXtra: headerXtra, meThod: .PutApi) { response in
  
        } failure: { response in
            
        }
    }
    
    func putApiPetsFormData(idPet: String,name: String, sex: Int, birthCerti: Data, note: String, customerId: String, idBreed: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["name": name, "sex": sex, "note": note, "customerId": customerId, "species": idBreed] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestUploadAPI(url: "\(commomSevice.BaseDomain)pet/\(idPet)", params: params, dataImage: birthCerti, headerXtra: headerXtra, meThod: .PutApi) { response in
  
        } failure: { response in
            
        }
    }
    
    func deleteApiCustomer(id: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["": nil] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)customer/\(id)", params: params, headerXtra: headerXtra, meThod: .DeleteApi) { response in
            
        } failure: { response in
            
        }

    }
    
    func deleteApiPets(id: String){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["": nil] as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)pet/\(id)", params: params, headerXtra: headerXtra, meThod: .DeleteApi) { response in
        } failure: { response in
        }
    }
    
    func getApiBranches(page: String, size: String, completionHandler: @escaping ([String : Any])->(),failure:@escaping (String)->()){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["page": page, "size": size]  as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)branches", params: params, headerXtra:headerXtra, meThod: .GetApi, completionHandler: { (response) in
            completionHandler(response)
        }) { (error) in
            failure(error.localizedDescription)
        }
    }
    
    func getApiCustomerDetail(id: String, completionHandler: @escaping ([String : Any])->(),failure:@escaping (String)->()){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let params = ["": nil]  as [String : Any]
        let headerXtra = ["Authorization": "Bearer \(token)"]
        LotusAPIService.sharedInstance.httpRequestAPI(url: "\(commomSevice.BaseDomain)customer/\(id)", params: params, headerXtra:headerXtra, meThod: .GetApi, completionHandler: { (response) in
            completionHandler(response)
        }) { (error) in
            failure(error.localizedDescription)
        }
    }
}
