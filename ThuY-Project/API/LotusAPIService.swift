//
//  APIService.swift
//  Ringtones
//
//  Created by Trương Thắng on 10/7/18.
//  Copyright © 2018 Trương Thắng. All rights reserved.
//
//
import Foundation
import Alamofire
import UIKit

open class LotusAPIService {

    static let sharedInstance = LotusAPIService()
    typealias CompletionHandler = (_ response: [String: Any]) -> Void
    typealias ErrorHandler = (_ response: Error) -> Void
    
    func httpRequestAPI(url:String,params:[String:Any],isSticker:Bool? = false, headerXtra: [String:String]? = nil,meThod:MethodApi,completionHandler: @escaping CompletionHandler,failure:@escaping ErrorHandler) {
        var headers = HTTPHeaders()
        if !isSticker! {
            headers["Content-Type"] = "application/json"
        }
        if let head = headerXtra {
            for (key,value) in head {
                headers[key] = value
            }
        }
        if meThod == .PostApi {
            AF.request(url, method: .post, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response:AFDataResponse<Any>) in
               
                switch(response.result) {
                case .success(let JSON):
                    if  let json = JSON as? [String: Any]{
                        
                            completionHandler (json)
                        
                    }
                    break
                    
                case .failure(let error):
                    failure(error)
                    break
                }
            }
            
            
        }else if meThod == .GetApi {
            
            
            AF.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response:AFDataResponse<Any>) in
               
                switch(response.result) {
                case .success(let JSON):
                   if  let json = JSON as? [String: Any]{
                  
                        completionHandler (json)

                   }
                    break
                    
                case .failure(let error):
                    failure(error)
                    break
                }
            }
        }else if meThod == .PutApi {
            AF.request(url, method: .put, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response:AFDataResponse<Any>) in
                switch(response.result) {
                case .success(let JSON):
                    if  let json = JSON as? [String: Any]{
                        
                        
                            completionHandler (json)
                        
                    }
                    break
                    
                case .failure(let error):
                    failure(error)
                    break
                }
            }
        } else if meThod == .DeleteApi {
            AF.request(url, method: .delete, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response:AFDataResponse<Any>) in
                switch(response.result) {
                case .success(let JSON):
                    if  let json = JSON as? [String: Any]{
                        
                      
                            completionHandler (json)
                        
                    }
                    break
                    
                case .failure(let error):
                    failure(error)
                    break
                }
            }
        } else if meThod == .OptionsApi{
            AF.request(url, method: .options, parameters: params, encoding: URLEncoding.default, headers: headers).responseJSON { (response:AFDataResponse<Any>) in
                switch(response.result) {
                case .success(let JSON):
                    if  let json = JSON as? [String: Any]{
                      
                            completionHandler (json)
                        
                    }
                    break
                    
                case .failure(let error):
                    failure(error)
                    break
                }
            }
        }
    }
    
    func httpRequestUploadAPI(url:String,params:[String:Any], dataImage :Data, headerXtra: [String:String]? = nil,meThod:MethodApi,completionHandler: @escaping CompletionHandler,failure:@escaping ErrorHandler) {
        var headers = HTTPHeaders()
        
            headers["Content-Type"] = "application/json"
        
        
        if let head = headerXtra {
            for (key,value) in head {
                headers[key] = value
            }
        }
        var methodCall :HTTPMethod = .post
        if meThod == .PostApi{
            methodCall = .post
        }else if meThod == .PutApi{
            methodCall = .put
        }
        
        AF.upload(multipartFormData: { multipartFormData in
            let randomString = UUID().uuidString
            for (key,value) in params {
                if key == "species"{
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }else{
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key)
                }
            }
            multipartFormData.append( dataImage, withName: "birthCerti", fileName: "\(randomString).jpg",mimeType: "image/jpeg")
        }, to: url,method: methodCall,headers: headers)
        .uploadProgress{progress in
            
        }.response{ response in
        
        }.responseJSON { response in
         
        }
        
    }
    
    
    func httpBodyRequestAPI(url:String,params:Any, headerXtra: [String:String]? = nil,meThod:MethodApi,completionHandler: @escaping CompletionHandler,failure:@escaping ErrorHandler) {
        var request = URLRequest(url: URL(string: url)!)
        if meThod == .PutApi {
            request.httpMethod = "put"
        }else if meThod == .PostApi {
            request.httpMethod = "POST"
        }
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
       

        if let head = headerXtra {
            for (key,value) in head {
                request.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        let json = try! JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
        
        
        request.httpBody = json
        
        AF.request(request).responseJSON { (response) in
            
            switch(response.result) {
            case .success(let JSON):
                if  let json = JSON as? [String: Any]{
                   
                        completionHandler (json)
                    
                }
                break
                
            case .failure(let error):
                failure(error)
                break
            }
        }
    }
    
}
