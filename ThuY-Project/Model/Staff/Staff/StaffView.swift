//
//  Staff.swift
//  ThuY-Project
//
//  Created by Viettelimex on 28/04/2022.
//

import Foundation
import UIKit


class StaffView: UIView{

    var root = StaffViewController()

    @IBAction func segmentControl(_ sender: UISegmentedControl) {
        switch segmentControl.selectedSegmentIndex {
        case 0:
//            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//                let cellDoc = tableView.dequeueReusableCell(withIdentifier: "StaffCell", for: indexPath) as! StaffCell
//                cellDoc.textLabel?.textColor = UIColor.white
//                return cellDoc
//            }
            StaffTableView.reloadData()
            StaffTableView.backgroundColor = .white
        case 1:
//            func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//                let cellDoc = tableView.dequeueReusableCell(withIdentifier: "StaffCellDoc", for: indexPath) as! StaffCellDoc
//                cellDoc.textLabel?.textColor = UIColor.white
//                return cellDoc
//            }
            
            StaffTableView.reloadData()
            StaffTableView.backgroundColor = .red
        default:
            break
        }
    }
    
    
    
    @IBOutlet weak var StaffTableView: UITableView!
    @IBOutlet weak var backGround: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    

    @IBOutlet weak var btnAdd: UIButton!
    
    @IBAction func btnAddStaff(_ sender: UIButton) {

    }
    
   
    override func awakeFromNib() {
        super.awakeFromNib()

        
        backGround.layer.cornerRadius = 15
        backGround.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]

        
    }
}
