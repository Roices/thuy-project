//
//  Noti.swift
//  ThuY-Project
//
//  Created by Tuan on 02/06/2022.
//

import Foundation

struct Noti: Codable {
    
    let title: String
    let message: String
    let topic: String
    let date: String
    
}

