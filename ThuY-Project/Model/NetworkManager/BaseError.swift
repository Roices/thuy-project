//
//  BaseError.swift
//  ThuY-Project
//
//  Created by AnhLe on 03/05/2022.
//

import Foundation

enum BaseError: Error{
    
    case requestError, parsedDataError, urlError
    
    public var title: String {
        switch self {
        case .requestError:
            return "Request Error"
        case .parsedDataError:
            return "Parsed Data Error"
        case .urlError:
            return "Url Error"
        }
    }
    
    public var description: String {
        switch self {
        case .requestError:
            return "Request to server error. Please try again"
        case .parsedDataError:
            return "Parsed data from server failed. Please try again"
        case .urlError:
            return "URL failed. Please try again"
        }
    }
}
