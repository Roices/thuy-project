import Foundation

struct BaseURL {
    public static let branchURL = "http://10.10.104.214:8083/mock3-backend/api/v1/branches"
    public static let addressURL = "https://provinces.open-api.vn/api/"
    public static let mapKey = "AIzaSyCdsuxv9HfYCS83YvNa0m22NXVQhC6J5Ww"
    public static let mapURL = "https://maps.googleapis.com/maps/api/place/textsearch/json"
    public static let loginURL = "http://10.10.104.214:8083/mock3-backend/api/v1/login"
    public static let refreshTokenURL = "http://10.10.104.214:8083/mock3-backend/api/v1/user/token/refresh"
    public static let scheduleURL = "http://10.10.104.214:8083/mock3-backend/api/v1/schedule"
    public static let userURL = "http://10.10.104.214:8083/mock3-backend/api/v1/user"
    public static let forgotPasswordURL = "http://10.10.104.214:8083/mock3-backend/api/v1/user/forgot"
    public static let staffURL = "http://10.10.104.214:8083/mock3-backend/api/v1/user"
    public static let petURL = "http://10.10.104.214:8083/mock3-backend/api/v1/pet"
    public static let customerURL = "http://10.10.104.214:8083/mock3-backend/api/v1/customer"
    public static let resetPasswordURL = "http://10.10.104.214:8083/mock3-backend/api/v1/user/change"
    public static let filterBranchURL = "http://10.10.104.214:8083/mock3-backend/api/v1/branches/filtering-dashboard/"
    public static let pushTokenToServerURL = "http://10.10.104.214:8083/mock3-backend/api/v1/notification/device"
    public static let sendingNotiURL = "http://10.10.104.214:8083/mock3-backend/api/v1/notification"
    public static let unPushTokenToServerURL = "http://10.10.104.214:8083/mock3-backend/api/v1/notification"
    public static let searchInBranch = "http://10.10.104.214:8083/mock3-backend/api/v1/"

}

