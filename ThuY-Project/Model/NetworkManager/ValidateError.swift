//
//  ValidateError.swift
//  ThuY-Project
//
//  Created by Tuan on 25/05/2022.
//

import Foundation

struct validateError: Codable {
    let status: Int
    let message: String
}
