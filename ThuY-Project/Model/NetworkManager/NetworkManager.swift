//
//  NetworkManager.swift
//  ThuY-Project
//
//  Created by AnhLe on 27/04/2022.
//
import UIKit
import Alamofire
class NetworkManager{
    static let shared = NetworkManager()
    func setHeader(token: String) -> HTTPHeaders{
        
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(token)"]
        return headers
    }
    
    
    //[GET]
    func fetchBranches(token: String, page: Int, size: Int,completion: @escaping(Result<[BranchData], Error>) -> Void){
        let param: Parameters = ["page": page, "size": size]
        AF.request(BaseURL.branchURL, method: .get, parameters: param, headers: setHeader(token: token)).responseDecodable(of: Branch.self) { response in
            switch response.result {
            case .success(let branch):
               let data = branch.data
                completion(.success(data))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    // [POST] save branch
    func saveBranches(token: String,branch: BranchData, completion: @escaping(Result<Void, Error>) -> Void){
        let url = "\(BaseURL.branchURL)"
        AF.request(url, method: .post, parameters: branch, encoder: JSONParameterEncoder.default, headers: setHeader(token: token)).response { response in
            switch response.result{
            case .success(_):
                completion(.success(()))
            case .failure(let error):
                completion(.failure(error))
            }
            
        }
    }

    //[PUT] update branch
    func updateBranch(token: String,branch: BranchData, completion: @escaping(Result<Void, Error>) -> Void){
        guard let id = branch.id else { return}
        let url = "\(BaseURL.branchURL)/\(id)"
        AF.request(url, method: .put, parameters: branch, encoder: JSONParameterEncoder.default, headers: setHeader(token: token)).validate(statusCode: 200...201).response { response in
            switch response.result{
            case .success(_):
                completion(.success(()))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    //[DELETE] delete branch
    func deleteBranch(token: String, branch: BranchData, completion: @escaping(Result<Int, Error>) -> Void) {
        guard let id = branch.id else {return}
        
        
        
        let url = "\(BaseURL.branchURL)/\(id)"
        
        AF.request(url, method: .delete, headers: setHeader(token: token)).responseDecodable(of: MessageResponse.self) { response in
            switch response.result {
            case .success(let message):
                completion(.success(message.status))
            case .failure(let error):
                completion(.failure(error))
            }
        }
        
//        AF.request(url, method: .delete,headers: setHeader(token: token)).response { response in
//            switch response.result {
//            case .success(_):
//
//                completion(.success(()))
//            case .failure(let error):
//                completion(.failure(error))
//            }
//        }
    }
    
    func searchBranches(token: String,
                        page: Int,
                        size: Int,
                        name: String,
                        address: String?=nil,
                        phone: String?=nil ,
                        completion: @escaping(Result<[BranchData], Error>) -> Void){
        
        let url = "\(BaseURL.branchURL)/search"
        let param: Parameters = ["name": name,
                                 "page": page,
                                 "phone": "",
                                 "address": "",
                                 "size": size]
        AF.request(url, method: .get, parameters: param, headers: setHeader(token: token)).responseDecodable(of: Branch.self) { response in
            switch response.result {
            case .success(let branch):
               let data = branch.data
                completion(.success(data))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    //[GET] get staffs
    func fetchStaffs(token: String, page: Int, size: Int,completion: @escaping(Result<[StaffData], Error>) -> Void){
        let param: Parameters = ["page": page, "size": size]
        AF.request(BaseURL.staffURL, method: .get, parameters: param, headers: setHeader(token: token)).responseDecodable(of: Staffv2.self) { response in
            switch response.result {
            case .success(let staffs):
                let data = staffs.data
                completion(.success(data))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func fetchPets(token: String, page: Int, size: Int,completion: @escaping(Result<[PetData], Error>) -> Void){
        let param: Parameters = ["page": page, "size": size]
        AF.request(BaseURL.petURL, method: .get, parameters: param, headers: setHeader(token: token)).responseDecodable(of: Petv2.self) { response in
            switch response.result {
            case .success(let pets):
                let data = pets.data
                completion(.success(data))
            case .failure(let error):

                completion(.failure(error))
            }
        }
    }
    
    func fetchCustomers(token: String, page: Int, size: Int,completion: @escaping(Result<Int, Error>) -> Void){
        let param: Parameters = ["page": page, "size": size]
        AF.request(BaseURL.customerURL, method: .get, parameters: param, headers: setHeader(token: token)).responseDecodable(of: Customerv2.self) { response in
            switch response.result {
            case .success(let data):
                let tot = data.total
                completion(.success(tot))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func resetPassword(token: String, oldPassword: String, newPassword: String, userName: String, completion: @escaping(Result<MessageResponse, Error>)->Void){
        let param: Parameters = ["username": userName, "oldPassword": oldPassword, "newPassword": newPassword]

        
        AF.request(BaseURL.resetPasswordURL, method: .put, parameters: param, headers: setHeader(token: token)).responseDecodable(of: MessageResponse.self) { response in
            switch response.result{
            case .success(let message):
                completion(.success(message))
            case .failure(let error):
                
                completion(.failure(error))
            }
        }
    }
    
    func filterBranch(token: String, id: String, completion: @escaping(Result<BranchFilterData, Error>) -> Void){
        let url = "\(BaseURL.filterBranchURL)\(id)"
        AF.request(url, method: .get, headers: setHeader(token: token)).responseDecodable(of: BranchFilter.self) { response in
            switch response.result {
            case .success(let branch):
               let data = branch.data
                completion(.success(data[0]))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    //search user in branch
    func searchUserInBranch(token: String,
                            page: Int,
                            size: Int,
                            branch: String,
                            completion: @escaping(Result<[StaffData], Error>)->Void){
        let param: Parameters = ["page": page,
                                 "size": size,
                                 "name": "",
                                 "phone": "",
                                 "role": "",
                                 "branch": branch]
        let url = "\(BaseURL.searchInBranch)user/search"
        AF.request(url, method: .get,parameters: param, headers: setHeader(token: token)).responseDecodable(of: Staffv2.self) { response in
            switch response.result {
            case .success(let staff):
               let data = staff.data
                completion(.success(data))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    //search pets in branch
    func searchPetsInBranch(token: String, page: Int, size: Int, branchID: String, completion: @escaping(Result<[PetData], Error>) -> Void){
        var petData = [PetData]()
        let url = "\(BaseURL.searchInBranch)customer/search"
        let param: Parameters = ["page": page,
                                 "size": size,
                                 "name": "",
                                 "branch": branchID]
        
        AF.request(url, method: .get,parameters: param, headers: setHeader(token: token)).responseDecodable(of: Customerv2.self) { response in
            switch response.result {
            case .success(let customer):
                let data = customer.data
                for customer in data {
                    let pet = customer.pets
                    petData.append(contentsOf: pet)
                }
                completion(.success(petData))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
}

// MARK: - Address API
extension NetworkManager {
    
    func fetchProvinces(completion: @escaping(Result<[Province], Error>)->Void){
        let url = "\(BaseURL.addressURL)/p"
        AF.request(url).validate(statusCode: 200...201).responseDecodable(of: [Province].self) { response in
            switch response.result {
            case .success(let provinces):
                completion(.success(provinces))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func fetchDistricts(cityCode: Int,completion: @escaping(Result<[District], Error>)->Void){
        let url = "\(BaseURL.addressURL)/p/\(cityCode)?depth=2"
        AF.request(url).validate(statusCode: 200...201).responseDecodable(of: Province.self) { response in
            switch response.result {
            case .success(let province):
                let districts = province.districts
                completion(.success(districts))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func fetchWards(districtCode: Int, completion: @escaping(Result<[Ward], Error>) -> Void){
        let url = "\(BaseURL.addressURL)/d/\(districtCode)?depth=2"
        AF.request(url).validate(statusCode: 200...201).responseDecodable(of: District.self) { response in
            switch response.result{
            case .success(let district):
                let wards = district.wards
                completion(.success(wards))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    func searchAddress(isProvince: Bool,address: String, completion: @escaping(Result<Int, Error>) -> Void){
        
        let url = isProvince ? "\(BaseURL.addressURL)p/search/" : "\(BaseURL.addressURL)d/search/"
        let param: Parameters = ["q": address]
        AF.request(url,method: .get ,parameters: param).responseDecodable(of: [AddressSearch].self) { response in
            switch response.result{
            case .success(let addresses):
                let address = addresses[0]
                completion(.success(address.code))
            case .failure(let error):
                completion(.failure(error))
            }
        }
        
    }
    

}

