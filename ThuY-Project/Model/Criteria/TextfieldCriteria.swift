//
//  TextfieldCriteria.swift
//  ThuY-Project
//
//  Created by AnhLe on 06/05/2022.
//

import Foundation

struct TextfieldCriteria {
    static func lengthPhoneCriteriaMet(_ text: String) -> Bool{
        let numbers = text.filter({$0 >= "0" && $0 <= "9"})
        let includingNotNumber = text.count == numbers.count
        
        return text.count == 10 && includingNotNumber
    }
    
    static func emptyTextCriteriaMet(_ text: String) -> Bool{
        return text.isEmpty
    }
    
    static func includingCommaCriteriaMet(_ text: String) -> Bool{
        return text.contains(",");
    }
}

enum CriteriaDescription{
    case emptyText, invalidPhoneNumber
    
    var description: String {
        switch self {
        case .emptyText:
            return "Bạn chưa điền đầy đủ thông tin"
        case .invalidPhoneNumber:
            return "Số điện thoại không hợp lệ"
        }
    }
}

