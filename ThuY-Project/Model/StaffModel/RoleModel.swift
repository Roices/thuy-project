//
//  EditStaffModel.swift
//  ThuY-Project
//
//  Created by Viettelimex on 23/05/2022.
//

import Foundation


// MARK: - Role
struct RoleModel: Codable {
    let id: Int
    let name: String
    
    enum CodingKeys: String, CodingKey{

        case id
        case name
    }
}
