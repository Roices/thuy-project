//
//  InfoStaffModel.swift
//  ThuY-Project
//
//  Created by Viettelimex on 06/05/2022.
//

import Foundation

struct InfoAPI: Codable {
    let id: String?

    let name: String
        
    let sex: String
    
    let birth: Date
    
    let identifyCard: String
    
    let phoneNumber: String
    
    let address: String
    
    let specialization: String
    
    let email: String
    
    let none: String
    

}
