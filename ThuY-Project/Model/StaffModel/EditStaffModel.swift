//
//  AddStaffModel.swift
//  ThuY-Project
//
//  Created by Viettelimex on 11/05/2022.
//

import Foundation


struct EditStaffModel: Codable {
    let name: String
    let sex: Int
    let birth, identityCard, phoneNumber, address: String
    let specialization, experience, username, password: String
    let roles: RoleModel
    let branchID : BranchData
    let email: String

    enum CodingKeys: String, CodingKey {
        case name, sex, birth, identityCard, phoneNumber, address, specialization, experience, username, password
        case roles
        case branchID
        case email
    }
}
