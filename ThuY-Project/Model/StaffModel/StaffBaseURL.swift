//
//  BaseURL.swift
//  ThuY-Project
//
//  Created by Viettelimex on 20/05/2022.
//

import Foundation

import Foundation

struct StaffBaseURL {
    public static let staffURL = "http://10.10.104.214:8083/mock3-backend/api/v1/user"
    public static let addURL = "http://10.10.104.214:8083/mock3-backend/api/v1/user"
    public static let editStaffURL = "http://10.10.104.214:8083/mock3-backend/api/v1/user"
    public static let searchStaffURL = "http://10.10.104.214:8083/mock3-backend/api/v1/user/search"
    
}
