//
//  StaffModel.swift
//  ThuY-Project
//
//  Created by Viettelimex on 06/05/2022.
//

import Foundation


import Foundation


struct StaffModel: Codable {
    
    let id, name, sex, birth: String
    let identityCard, phoneNumber, address, specialization: String
    let experience, username, email: String
    let roles: [RoleModel]
    let branch: BranchData
    
        enum CodingKeys: String, CodingKey{
    
            case id, name, sex, birth
            case identityCard, phoneNumber, address, specialization
            case experience, username, email
            case roles
            case branch
        }
}



