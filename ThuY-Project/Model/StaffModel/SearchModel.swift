//
//  SearchModel.swift
//  ThuY-Project
//
//  Created by Viettelimex on 11/05/2022.
//

import Foundation

struct branchSearch: Codable{
    let id: Int?
    let name: String?
    let address: String?
    let phoneNumber: String?
    let note: String?
    
    enum branchSearch: String, CodingKey{
        case id = "id"
        case name = "name"
        case address = "address"
        case phoneNumber = "phoneNumber"
        case note = "note"
    }
    init(from decoder: Decoder) throws {
        let valuesBranch = try decoder.container(keyedBy: CodingKeys.self)
        id = try valuesBranch.decodeIfPresent(Int.self, forKey: .id)
        name = try valuesBranch.decodeIfPresent(String.self, forKey: .name)
        address = try valuesBranch.decodeIfPresent(String.self, forKey: .address)
        phoneNumber = try valuesBranch.decodeIfPresent(String.self, forKey: .phoneNumber)
        note = try valuesBranch.decodeIfPresent(String.self, forKey: .note)
    }
}

struct roleSearch: Codable{
    let id: Int?
    let name: String?
    
    enum roleSearch: String, CodingKey{
        case id = "id"
        case name = "name"
    }
    init(from decoder: Decoder) throws {
        let valuesRole = try decoder.container(keyedBy: CodingKeys.self)
        id = try valuesRole.decodeIfPresent(Int.self, forKey: .id)
        name = try valuesRole.decodeIfPresent(String.self, forKey: .name)
    }
}

struct SearchModel: Codable{
    let id: String?
    let name: String?
    let sex: String?
    let birth: Date?
    let identifyCard: String?
    let address: String?
    let phoneNumber: String?
    let specialization: String?
    let email: String?
    let experience: String?
    let role: String?
    let note: String?
    
    enum CodingKeys: String, CodingKey{

        case id = "id"
        case name = "name"
        case sex = "sex"
        case birth = "birth"
        case identifyCard = "identifyCard"
        case address = "address"
        case phoneNumber = "phoneNumber"
        case specialization = "specialization"
        case email = "email"
        case experience = "experience"
        case role = "role"
        case note = "note"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        sex = try values.decodeIfPresent(String.self, forKey: .sex)
        birth = try values.decodeIfPresent(Date.self, forKey: .birth)
        identifyCard = try values.decodeIfPresent(String.self, forKey: .identifyCard)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        phoneNumber = try values.decodeIfPresent(String.self, forKey: .phoneNumber)
        specialization = try values.decodeIfPresent(String.self, forKey: .specialization)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        experience = try values.decodeIfPresent(String.self, forKey: .experience)
        role = try values.decodeIfPresent(String.self, forKey: .role)
        note = try values.decodeIfPresent(String.self, forKey: .note)
    }
    
}


