//
//  InfoModel.swift
//  ThuY-Project
//
//  Created by Viettelimex on 19/05/2022.
//

import Foundation



struct InfoModel: Codable{
    let idInfo: String?
    let name: String?
    let sex: String?
    let birth: Date?
    let identifyCard: String?
    let address: String?
    let phoneNumber: String?
    let specialization: String?
    let email: String?
    let experience: String?
    let role: String?
    let note: String?
    
    enum CodingKeys: String, CodingKey{

        case idInfo = "idInfo"
        case name = "name"
        case sex = "sex"
        case birth = "birth"
        case identifyCard = "identifyCard"
        case address = "address"
        case phoneNumber = "phoneNumber"
        case specialization = "specialization"
        case email = "email"
        case experience = "experience"
        case role = "role"
        case note = "note"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        idInfo = try values.decodeIfPresent(String.self, forKey: .idInfo)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        sex = try values.decodeIfPresent(String.self, forKey: .sex)
        birth = try values.decodeIfPresent(Date.self, forKey: .birth)
        identifyCard = try values.decodeIfPresent(String.self, forKey: .identifyCard)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        phoneNumber = try values.decodeIfPresent(String.self, forKey: .phoneNumber)
        specialization = try values.decodeIfPresent(String.self, forKey: .specialization)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        experience = try values.decodeIfPresent(String.self, forKey: .experience)
        role = try values.decodeIfPresent(String.self, forKey: .role)
        note = try values.decodeIfPresent(String.self, forKey: .note)
    }
}
