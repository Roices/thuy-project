//
//  APIService.swift
//  ThuY-Project
//
//  Created by Viettelimex on 11/05/2022.
//

import Foundation

struct APIServiceStaff{
    
    static var URL_API = "http://10.10.66.227:1405/api/v1/user" // GET
    
    static func getAPI (getCompleted: @escaping (Data?, Bool, String) -> Void){
        let url = URL(string: self.URL_API)
        URLSession.shared.dataTask(with: url!) { (data, respone, error) in
            if let httpResponse =  respone as? HTTPURLResponse {
                if(httpResponse.statusCode != 200) {
                    getCompleted(nil, false, String(httpResponse.statusCode))
                }else {
                    getCompleted(data, true, String(httpResponse.statusCode))
                }
            }
            
        }.resume()
        
    }
    
}
