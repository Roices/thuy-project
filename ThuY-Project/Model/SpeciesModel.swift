//
//  SpeciesModel.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 09/05/2022.
//

import Foundation
import ObjectMapper

class BreedModel: NSObject, Mappable{
    var total: Int = 0
    var status: Int = 0
    var message: String = ""
    var data: [BreedDataModel] = []
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        total <- map["total"]
        status <- map["status"]
        message <- map["message"]
        data <- map["data"]
    }
}

class SpeciesModel: NSObject, Mappable{
    
    var status: Int = 0
    var total: Int = 0
    var message: String = ""
    var data: [SpeciesDataModel] = []
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        status <- map["status"]
        total <- map["total"]
        message <- map["message"]
        data <- map["data"]
    }
    
}

class SpeciesDataModel: NSObject, Mappable {
    var id: String = ""
    var name: String = ""
    var note: String = ""
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        note <- map["note"]
    }
}

class BreedDataModel: NSObject, Mappable{
    
    var id: String = ""
    var name: String = ""
    var note: String = ""
    var type: SpeciesDataModel?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        note <- map["note"]
        type <- map["type"]
    }
}
