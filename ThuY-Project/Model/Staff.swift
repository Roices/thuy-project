//
//  Staff.swift
//  ThuY-Project
//
//  Created by AnhLe on 17/05/2022.
//

import UIKit
struct Staff: Codable{
    let id, name, sex, birth: String
    let identifyCard, address, phoneNumber, specialization: String
    let email, username, experience: String
    let role: [Role]
    let branch: Branch
}

struct Role: Codable {
    let id: String
    let name: String
}
