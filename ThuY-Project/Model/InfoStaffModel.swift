//
//  InfoStaffModel.swift
//  ThuY-Project
//
//  Created by Viettelimex on 06/05/2022.
//

import Foundation

struct InfoStaffModel: Identifiable, Decodable {
    var name: String
    
    var avatar: String
    
    var id: String
    
    var sex: String
    
    var dayofbirth: String
    
    var cmnd: String
    
    var phonenumber: String
    
    var role: String
    
    var address: String
    
    var email: String
    
    var exp: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case avatar
        case sex
        case role
        case dayofbirth
        case cmnd
        case phonenumber
        case address
        case email
        case exp
    }
}
