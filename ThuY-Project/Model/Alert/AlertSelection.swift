//
//  AlertSelection.swift
//  ThuY-Project
//
//  Created by AnhLe on 08/05/2022.
//

import Foundation

enum AlertSelection {
    case delete, update, tokenExprired, normal
    var title: String {
        switch self {
        case .delete:
            return "Xoá "
        case .update:
            return "Lưu thay đổi"
        case .normal:
            return ""
        case .tokenExprired:
            return "Phiên đăng nhập hết hạn"
        }
    }
}
