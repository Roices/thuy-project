//
//  Customer.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 09/05/2022.
//

import Foundation
import ObjectMapper

class CustomerModel: NSObject, Mappable{
    var total: Int = 0
    var status: Int = 0
    var message: String = ""
    var data: [CustomerDataModel] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        total <- map["total"]
        status <- map["status"]
        message <- map["message"]
        data <- map["data"]
    }
    
    
}


class CustomerDataModel: NSObject,Mappable{
    var id: String = ""
    var name: String = ""
    var address: String = ""
    var phoneNumber: String = ""
    var email: String = ""
    var type: String = ""
    var pets: [PetDataModel] = []
    var branches : [BranchesDataModel] = []
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        address <- map["address"]
        phoneNumber <- map["phoneNumber"]
        email <- map["email"]
        type <- map["type"]
        pets <- map["pets"]
        branches <- map["branches"]
    }
    
    
}

class PetDataModel: NSObject,Mappable{
 
    var id: String = ""
    var name: String = ""
    var sex: String = ""
    var birthCerti: String = ""
    var note: String = ""
    var customerId: String = ""
    var species: [BreedDataModel] = []
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        sex <- map["sex"]
        birthCerti <- map["birthCerti"]
        note <- map["note"]
        customerId <- map["customerId"]
        species <- map["species"]
    }
    
   
}

class PetModel: NSObject, Mappable{
    
    var total: Int = 0
    var status: Int = 0
    var message: String = ""
    var data: [PetDataModel] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        total <- map["total"]
        status <- map["status"]
        message <- map["message"]
        data <- map["data"]
    }
    
    
}

class BranchesModel: NSObject, Mappable{
    
    var total: Int = 0
    var status: Int = 0
    var message: String = ""
    var data: [BranchesDataModel] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        total <- map["total"]
        status <- map["status"]
        message <- map["message"]
        data <- map["data"]
    }
    
    
}

class BranchesDataModel: NSObject, Mappable{
    
    var id: String = ""
    var name: String = ""
    var address: String = ""
    var phoneNumber: String = ""
    var note: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        address <- map["address"]
        phoneNumber <- map["phoneNumber"]
        note <- map["note"]
    }
    
    
}
