import Foundation

struct LoginModel: Codable {
    
    let access_token: String
    let refresh_token: String
    let user: UserLogin

    
    enum CodingKeys: String, CodingKey {
        case access_token
        case refresh_token
        case user
       
    }
}

struct UserLogin: Codable {
    let id: String
    let name: String
    let authorities: [Authority]
    let branch: BranchData
    let address: String
    let identityCard: String
    let phoneNumber: String
    let username: String
    let birth: String
}


// MARK: - Authority
struct Authority: Codable {
    let authority: String
}
