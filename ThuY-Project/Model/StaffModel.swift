//
//  StaffModel.swift
//  ThuY-Project
//
//  Created by Viettelimex on 06/05/2022.
//

import Foundation

struct StaffModel: Identifiable, Decodable {
    var id: String
    
    var name: String
    
    var avatar: String
    
    var createdAt: String
    
    var role: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case avatar
        case createdAt
        case role
    }
}
