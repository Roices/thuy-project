//
//  Pet.swift
//  ThuY-Project
//
//  Created by Tuan on 16/05/2022.
//

import Foundation

struct Pet: Codable {
    
    let id, name, sex: String
    let note: String?
    let customerId: String?
    let species: [Species1]

    enum CodingKeys: String, CodingKey {
        case id, name, sex
        case note, customerId
        case species
    }
}
