//
//  Booking.swift
//  ThuY-Project
//
//  Created by Tuan on 16/05/2022.
//

import Foundation

struct Booking: Codable {

    let id: Int
    let date: String
    let time: String
    let symptom: String
    let note: String
    let status: String
    let pet: Pet
    let user: User
    let additionalDetail: AdditionalDetail?
  

    enum CodingKeys: String, CodingKey {
        case id
        case date
        case time
        case symptom
        case note
        case status
        case pet, user
        case additionalDetail
 
    }
    
}


struct AdditionalDetail: Codable {
    let planDocId: String?
    let planDocName: String?
    let planDocphone: String?
    let planDocSpec: String?
    let realDocId: String?
    let realDocName: String?
    let realDocPhone: String?
    let realDocSpec: String?
    let customerId: String?
    let customerName: String?
}


