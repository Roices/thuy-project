//
//  AddingBooking.swift
//  ThuY-Project
//
//  Created by Tuan on 23/05/2022.
//

import Foundation

struct Base: Codable {
    let status: Int
    let message: String
    let data: [AddingBooking]
}

struct AddingBooking: Codable {
    
    let id: Int
    let user: UserResponse
    
    enum CodingKeys: String, CodingKey {
        case id
        case user
    
    }

}

struct UserResponse: Codable {
    let id: String
}
