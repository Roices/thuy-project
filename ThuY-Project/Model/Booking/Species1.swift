//
//  Species.swift
//  ThuY-Project
//
//  Created by Tuan on 19/05/2022.
//

import Foundation

struct Species1: Codable {
    let id, name, note: String

    enum CodingKeys: String, CodingKey {
        case id, name, note
    
    }
}
