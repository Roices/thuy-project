//
//  BaseResponse.swift
//  ThuY-Project
//
//  Created by Tuan on 11/05/2022.
//

import Foundation

struct BaseResponse<T:Codable>: Codable {
    
    let status: Int
    let total: Int?
    let message: String
    let data: [T]
    
    enum CodingKeys: String, CodingKey {
        case status
        case total
        case message
        case data
    
    }
}
