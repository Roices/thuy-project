/////

import UIKit
import Alamofire

typealias CompleteHandlJSONCode = (_ isSuccess: Bool, _ json: Any?, _ statusCode: Int?)->()

class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

class APIService {
    static let shared = APIService()

    func requestWith<T: Codable>(url: String,
                                method: HTTPMethod,
                                parameters: [String: Any]?,
                                objectTyple: T.Type?,
                                headers: HTTPHeaders?,
                                encoding: ParameterEncoding,
                                complete: @escaping CompleteHandlJSONCode) {
        
        if !Connectivity.isConnectedToInternet() {

            complete(false, "Vui lòng kiểm tra lại internet!", nil)
        } else {
        
            AF.request(url,
                       method: method,
                       parameters: parameters,
                       encoding: encoding,
                       headers: headers).responseDecodable(of: T.self, completionHandler:{ (response) in

                
                switch response.result {
                case .success(let value):
                    
                    complete(true, value, response.response?.statusCode)
                   
                case .failure(let error):
                    
                
                    if error.localizedDescription == "URLSessionTask failed with error: The request timed out." {
                        complete(false,"The request timed out.", -1001)
                    }
                    
                    guard let status = response.response?.statusCode else {return}
                     
                    switch status {
                    case 401:
                        complete(false, "Sai mã nhân viên hoặc mật khẩu", status)
                    case 403:
                        complete(false, "Đã có lỗi xảy ra bạn vui lòng thử lại", status)
                    default:
                        complete(false, error, status)
                        break

                    }
                    complete(false, error, status)
                }
                
            })
            
            }

      }
    
    func getAccessTokenWhenExpired() {
        let refreshToken = UserDefaults.standard.string(forKey: "refresh_token")
        let url = "\(BaseURL.refreshTokenURL)"
        guard let _refreshToken = refreshToken else {
            return
        }
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(_refreshToken)"]
        AF.request(url,
                   method: .get,
                   parameters: nil,
                   headers: headers,
                   interceptor: nil).responseString(completionHandler:{ (response) in
           
            
            switch response.result {
            case .success(let value):
                UserDefaults.setValue(value, forKey: "access_token")
            case .failure(let error):
                print(error)

            }
            
        })

    }
   
    func getHeader() -> HTTPHeaders {
        var headers: HTTPHeaders = ["Authorization": "Bearer "]
        if let accessToken = UserDefaults.standard.string(forKey: "access_token"){
             headers = ["Authorization": "Bearer \(accessToken)"]
        } else {
            return headers
        }
            return headers

    }

}


