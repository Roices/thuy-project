//
//  Customer1.swift
//  ThuY-Project
//
//  Created by Tuan on 16/05/2022.
//

import Foundation

struct Customer: Codable {
    
    let id: String
    let name: String
    let phoneNumber: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name
        case phoneNumber
    }
}
