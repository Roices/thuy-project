//
//  User.swift
//  ThuY-Project
//
//  Created by Tuan on 16/05/2022.
//

import Foundation

struct User: Codable {
    
    let id: String
    let name: String
    let phoneNumber: String
    let specialization: String
    let experience: String
    let branch: BranchData
    let note: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name
        case phoneNumber
        case specialization
        case experience
        case branch
        case note

    }
    
}


