//
//  ActionSheetBranchCell.swift
//  ThuY-Project
//
//  Created by AnhLe on 22/05/2022.
//

import UIKit

class ActionSheetBranchCell: UITableViewCell {
    // MARK: - Subview
    var selectAllButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("Chọn tất cả", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.greenTY, for: .normal)
        return button
    }()
    
    var unSelectAllButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("Bỏ chọn tất cả", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .body)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.redTY, for: .normal)
        return button
    }()
    
    // MARK: - Properties
    static let reuseIdentifier = "ActionSheetBranchCell"
    
    // MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Selector
    @objc func didSelectAllButtonTapped(_ sender: UIButton){
        NotificationCenter.default.post(name: .selectAllTapped, object: nil)
    }
    
    @objc func didUnSelectAllButtonTapped(_ sender: UIButton){
        NotificationCenter.default.post(name: .unSelectAllTapped, object: nil)
    }
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension

extension ActionSheetBranchCell {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        
        selectAllButton.addTarget(self, action: #selector(didSelectAllButtonTapped(_:)), for: .touchUpInside)
        unSelectAllButton.addTarget(self, action: #selector(didUnSelectAllButtonTapped(_:)), for: .touchUpInside)
    }
    
    private func layout(){
        contentView.addSubview(selectAllButton)
        contentView.addSubview(unSelectAllButton)
        
        NSLayoutConstraint.activate([
            selectAllButton.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 8),
            selectAllButton.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),

        ])
        
        NSLayoutConstraint.activate([
            unSelectAllButton.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 8),
            unSelectAllButton.trailingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.trailingAnchor),
        ])
    }
}

