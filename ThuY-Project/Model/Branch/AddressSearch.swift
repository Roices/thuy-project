//
//  AddressSearch.swift
//  ThuY-Project
//
//  Created by AnhLe on 02/06/2022.
//

import Foundation

struct AddressSearch: Codable {
    let name: String
    let code: Int
    let matches: [String: [Int]]
}

typealias AddressSearchAlias = [AddressSearch]

