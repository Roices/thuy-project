//
//  BranchTextFields.swift
//  ThuY-Project
//
//  Created by AnhLe on 04/05/2022.
//

import Foundation

enum BranchTextFields{
    case city, district, ward, name, id, phone, street
    
    var title: String {
        switch self {
        case .city:
            return "Tỉnh/Thành phố"
        case .district:
            return "Quận/Huyện"
        case .ward:
            return "Xã/ Phường/ Thị trấn"
        case .name:
            return "Tên chi nhánh"
        case .id:
            return "Mã chi nhánh"
        case .phone:
            return "Số điện thoại"
        case .street:
            return "Số nhà/ Đường"
        }
    }
}
