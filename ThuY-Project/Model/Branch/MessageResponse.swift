//
//  MessageResponse.swift
//  ThuY-Project
//
//  Created by AnhLe on 26/05/2022.
//

import Foundation

struct MessageResponse: Codable {
    let status: Int
    let message: String
}
