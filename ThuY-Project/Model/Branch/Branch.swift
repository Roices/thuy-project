//
//  Branch.swift
//  ThuY-Project
//
//  Created by AnhLe on 03/05/2022.
//

import Foundation

struct Branch: Codable {
    let status, total: Int
    let message: String
    let data: [BranchData]
    
}

// MARK: - Datum
struct BranchData: Codable {
    let id: String?
    let name, address, phoneNumber: String
    let note: String
    var customerNumber: Int?=nil
    var petNumber: Int?=nil
    var typeNumber: Int?=nil
    var speciesNumber: Int?=nil
    var userNumber: Int?=nil
}

// MARK: - Welcome
struct BranchFilter: Codable {
    let status: Int
    let message: String
    let data: [BranchFilterData]
}

// MARK: - Datum
struct BranchFilterData: Codable {
    let id, name, address, phoneNumber: String
    let note: String
    let customerNumber, petNumber, typeNumber, speciesNumber: Int
    let userNumber: Int
}

