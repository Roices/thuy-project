//
//  ActionSheetBranch.swift
//  ThuY-Project
//
//  Created by AnhLe on 21/05/2022.
//

import UIKit

class ActionSheetBranch: NSObject {
    // MARK: - Subview
    private var window: UIWindow?
        
    private let tableView: UITableView = UITableView()
    
    // MARK: - Properties
    var height: CGFloat = 0
    // MARK: - Lifecycle
    init(tabBarHeight: CGFloat) {
        super.init()
        self.height = tabBarHeight
        
    }

    
    // MARK: - Selector

    
    // MARK: - API
    
    // MARK: - Helper
    func configureTableView(){
        tableView.separatorStyle = .none
        tableView.rowHeight = 44
        tableView.backgroundColor = .white
        tableView.isScrollEnabled = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(ActionSheetBranchCell.self, forCellReuseIdentifier: ActionSheetBranchCell.reuseIdentifier)
    }
    
    func showSheet(_ shouldShow: Bool){
        guard let window = window else {return}
        let y = shouldShow ? window.frame.height - height : window.frame.height
        tableView.frame.origin.y = y

    }
    func show(){
        guard let window = UIApplication.shared.windows.first(where: {$0.isKeyWindow}) else {return}
        self.window = window

        window.addSubview(tableView)
        
        tableView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.size.width, height: height)
       
        configureTableView()
        
        UIView.animate(withDuration: 0.3) {
            self.showSheet(true)
            
        }
    }
    
    func dimissActionSheet(){
        
        UIView.animate(withDuration: 0.3) {
            self.showSheet(false)
        } completion: { done in
            if done{

            }
        }
    }
}
// MARK: - Extension
extension ActionSheetBranch: UITableViewDelegate{
    
}

extension ActionSheetBranch: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = (tableView.dequeueReusableCell(withIdentifier: ActionSheetBranchCell.reuseIdentifier, for: indexPath) as? ActionSheetBranchCell)!
        return cell
    }
    
    
}

