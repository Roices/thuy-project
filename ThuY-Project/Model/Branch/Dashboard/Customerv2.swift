struct Customerv2: Codable {
    let status, total: Int
    let message: String
    let data: [CustomerData]
}

// MARK: - Datum
struct CustomerData: Codable {
    let id, name, address, phoneNumber: String
    let email: String
    let type: String?
    let pets: [PetData]
    let branches: [BranchCustomer]
}

struct BranchCustomer: Codable {
    let id, name, address, phoneNumber: String
    let note: String

}
