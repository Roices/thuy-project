//
//  Petv2.swift
//  ThuY-Project
//
//  Created by Tuan on 26/05/2022.
//

import Foundation

struct Petv2: Codable {
    let status, total: Int
    let message: String
    let data: [PetData]
}

// MARK: - Datum
struct PetData: Codable {
    let id, name: String
    let sex: String
    let birthCerti, note: String
    let customerID: String?
    let species: [SpeciesData]

    enum CodingKeys: String, CodingKey {
        case id, name, sex, birthCerti, note
        case customerID = "customerId"
        case species
    }
}

// MARK: - Species
class SpeciesData: Codable {
    let id, name, note: String
    let type: SpeciesData?

    init(id: String, name: String, note: String, type: SpeciesData?) {
        self.id = id
        self.name = name
        self.note = note
        self.type = type
    }
}
