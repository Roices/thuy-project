//
//  Staffv2.swift
//  ThuY-Project
//
//  Created by Tuan on 26/05/2022.
//

import Foundation

struct Staffv2: Codable {
    let status, total: Int
    let message: String
    let data: [StaffData]
}

struct StaffData: Codable {

    let id, name, sex, birth: String
    let identityCard, phoneNumber, address, specialization: String
    let experience, username, email: String
    let roles: [RoleModel]
    let branch: BranchData
    let note: String?
    
    enum CodingKeys: String, CodingKey{
        case id, name, sex, birth
        case identityCard, phoneNumber, address, specialization
        case experience, username, email
        case roles
        case branch
        case note
    }
}

