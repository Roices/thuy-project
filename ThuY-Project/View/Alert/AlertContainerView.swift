//
//  AlertContainerView.swift
//  ThuY-Project
//
//  Created by AnhLe on 08/05/2022.
//

import UIKit

class AlertContainerView: UIView {
    // MARK: - Subview
    
    // MARK: - Properties
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 200, height: 200)
    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension

extension AlertContainerView {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = 15
        layer.borderWidth = 2
        layer.borderColor = UIColor.lightGray.cgColor
        backgroundColor = .systemBackground
    }
    
    private func layout(){
        
    }
}

