//
//  InfoStaffViewModel.swift
//  ThuY-Project
//
//  Created by Viettelimex on 18/05/2022.
//

import Foundation
import UIKit
import Alamofire

class InfoStaffViewModel{
    
    weak var vc: InfoStaff?
    var arrInfoStaff = [StaffModel]()

    func getAllInfoStaffData(){
        
        let parameters = [
            "page":"1",
            "size":"10"
        ]
        let header = [
            "Authorization" : "Bearer token"
        ]
        
        guard let url = URL(string: "http://10.10.66.243:1404/api/v1/user") else {return}
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValu("Application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ0aG9hcHJvIiwicm9sZXMiOlsiRU1QIl0sImlzcyI6Imh0dHA6Ly8xMC4xMC42Ni4yNDM6MTQwNC9hcGkvdjEvbG9naW4iLCJleHAiOjE2NTI5NTQ3ODF9.44zygskfRxz_cjkwzcHeP-5i29Ew_VE9MPWObmmJ3U4", forHTTPHeaderField: "Authorization")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: parameters, options: [])else{return}
        request.httpBody = httpBody
        
        let session = URLSession.shared
        session.dataTask(with: url){
             (data, response, error) in
            if error == nil{
                if let data = data {
                    do{
                        let staffResponse = try JSONDecoder().decode([StaffModel].self, from: data)
                        self.arrInfoStaff.append(contentsOf: staffResponse)
                        DispatchQueue.main.async {
                            print(data)
                        }
                    }catch let err{
                        print(err.localizedDescription)
                    }

            }

            }else{
                print(error?.localizedDescription)
            }
        }.resume()
        
        
        
    }

}
