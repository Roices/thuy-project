//
//  StaffViewModel.swift
//  ThuY-Project
//
//  Created by Viettelimex on 18/05/2022.
//

import Foundation
import UIKit

class StaffViewModel{
    weak var vc: Staff?
    var arrStaff = [StaffModel]()

    func getAllStaffData(){
        URLSession.shared.dataTask(with: URL(string: "https://6284a4703060bbd3473c68a1.mockapi.io/dataStaff")!){
             (data, response, error) in
            if error == nil{
                if let data = data {
                    do{
                        let staffResponse = try JSONDecoder().decode([StaffModel].self, from: data)
                        self.arrStaff.append(contentsOf: staffResponse)
                        DispatchQueue.main.async {
                            self.vc?.StaffTableView.reloadData()

                        }
 
                    }catch let err{
                        print(err.localizedDescription)
                    }

            }

            }else{
                print(error?.localizedDescription)
            }
        }.resume()

    }
}
