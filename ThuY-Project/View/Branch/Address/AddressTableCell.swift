//
//  ProvinceTableCell.swift
//  ThuY-Project
//
//  Created by AnhLe on 05/05/2022.
//

import UIKit

class AddressTableCell: UITableViewCell {
    // MARK: - Subview
    let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 5
        view.backgroundColor = .white
        return view
    }()
    
    let titleLabel = LabelCustom(color: .black, fontFamily: UIFont.boldSystemFont(ofSize: 16), alignment: .left)
    // MARK: - Properties
    
    
    static let reuseIdentifier = "BranchTableCell"
    static let rowHeight: CGFloat = 72
    
    // MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
    //    override var intrinsicContentSize: CGSize {
    //        return CGSize(width: 200, height: 200)
    //    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
    func bindingProvinceData(province: Province){
//        idView.idLabel.text = branch.id
        titleLabel.text = province.name
    }
    
    func bingdingDistrictData(district: District){
        titleLabel.text = district.name
    }
    
    func bindingWardData(ward: Ward){
        titleLabel.text = ward.name
    }
}
// MARK: - Extension

extension AddressTableCell {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = "Anh Le"
        
        layer.cornerRadius = 5
        selectionStyle = .none
        backgroundColor = .clear
    }
    
    private func layout(){
        contentView.addSubview(containerView)
        containerView.addSubview(titleLabel)
        
        //containerView
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalToSystemSpacingBelow: contentView.topAnchor, multiplier: 1),
            containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            contentView.bottomAnchor.constraint(equalToSystemSpacingBelow: containerView.bottomAnchor, multiplier: 1)
            
        ])
        
        //titleLabel
        NSLayoutConstraint.activate([
            titleLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            titleLabel.leadingAnchor.constraint(equalToSystemSpacingAfter: containerView.leadingAnchor, multiplier: 1)
        
        ])
        
    }
}

