//
//  AddButton.swift
//  ThuY-Project
//
//  Created by AnhLe on 04/05/2022.
//

import UIKit

class AddButton: UIButton {
    // MARK: - Subview
    
    // MARK: - Properties
    let buttonHeight: CGFloat = 40
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
    override var intrinsicContentSize: CGSize {
        return CGSize(width: buttonHeight, height: buttonHeight)
    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension

extension AddButton {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        backgroundColor = .greenTY
        tintColor = .white
        self.layer.cornerRadius = buttonHeight / 2
        setImage(Image.add, for: .normal)
    }
    
    private func layout(){
        
    }
}

