//
//  BranchTextView.swift
//  ThuY-Project
//
//  Created by AnhLe on 04/05/2022.
//

import UIKit

class BranchTextView: UIView {
    // MARK: - Subview
    private let titleLabel = LabelCustom(color: .greenTY, fontFamily: UIFont.boldSystemFont(ofSize: 16), alignment: .left)
    
    let textView: UITextView = {
        let textView = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        
        textView.font = UIFont.systemFont(ofSize: 15)
        textView.layer.cornerRadius = 5
        return textView
    }()
    
    // MARK: - Properties
    var placeholder: String!
    
    // MARK: - Lifecycle
    convenience init(label: String) {
        self.init(frame: .zero)
        
        titleLabel.text = label
        textView.text = label
        placeholder = label
        textView.textColor = .lightGray
        
//        textView.becomeFirstResponder()
        textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        textView.autocorrectionType = .no
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 200, height: 150)
    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension

extension BranchTextView {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        textView.delegate = self
        
    }
    
    private func layout(){
        addSubview(titleLabel)
        addSubview(textView)
        //title label
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor)
        ])
        
        //textview
        NSLayoutConstraint.activate([
            textView.topAnchor.constraint(equalToSystemSpacingBelow: titleLabel.bottomAnchor, multiplier: 1),
            textView.bottomAnchor.constraint(equalTo: bottomAnchor),
            textView.leadingAnchor.constraint(equalTo: leadingAnchor),
            textView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
        
        titleLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
    }
}

extension BranchTextView: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText: String =  textView.text
        let updatedText = (currentText as NSString).replacingCharacters(in: range, with: text)
        
        if updatedText.isEmpty {
            textView.text = placeholder
            textView.textColor = .lightGray
            textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
        }
        else if textView.textColor == UIColor.lightGray && !text.isEmpty{
            textView.textColor = .darkGray
            textView.text = text
        }else{
            return true
        }
        return false
        
    }
    
    //prevent user move changing postion of cursor then detect selectedTextRange incorrect
    func textViewDidChangeSelection(_ textView: UITextView) {
        if self.window != nil{
            if textView.textColor == UIColor.lightGray {
                textView.selectedTextRange = textView.textRange(from: textView.beginningOfDocument, to: textView.beginningOfDocument)
            }
        }
        
    }
}

