//
//  BranchTextFieldView.swift
//  ThuY-Project
//
//  Created by AnhLe on 04/05/2022.
//

import UIKit

protocol BranchTextFieldViewDelegate: AnyObject {
    func didTextFieldChanged(view: BranchTextFieldView)
    func didWarningButtonTapped(view: BranchTextFieldView)
}
class BranchTextFieldView: UIView {
    // MARK: - Subview
    private let titleLabel = LabelCustom(color: .greenTY, fontFamily: UIFont.boldSystemFont(ofSize: 16), alignment: .left)
    
    let textField: UITextField = {
       let textfield = UITextField()
        textfield.textColor = .darkGray
        textfield.backgroundColor = .white
        textfield.translatesAutoresizingMaskIntoConstraints = false
        //Create space at the beginning of a UITextField
        //width = 8 is left padding from beginning
        
        textfield.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: textfield.frame.height))
        textfield.leftViewMode = .always
        textfield.layer.borderWidth = 1
        textfield.layer.borderColor = UIColor.clear.cgColor
        return textfield
    }()
    
    let errorLabel = LabelCustom(color: .redTY, fontFamily: UIFont.preferredFont(forTextStyle: .callout), alignment: .left)
    
    // MARK: - Properties
    let textfieldHeight: CGFloat = 44
    weak var delegate: BranchTextFieldViewDelegate?
    // MARK: - Lifecycle
    convenience init(label: String) {
        self.init(frame: .zero)
        titleLabel.text = label
        textField.attributedPlaceholder = makeTextfieldPlaceholder(placeholder: label)
        textField.autocorrectionType = .no
        textField.delegate = self

        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: 75)
    }
    
    // MARK: - Selector
    @objc func errorAction(){
        delegate?.didWarningButtonTapped(view: self)
    }
    
    // MARK: - API
    
    // MARK: - Helper
    func reset(){
        textField.layer.borderColor = UIColor.clear.cgColor
        if textField.rightView != nil {
            textField.rightView = nil
            textField.rightView?.removeFromSuperview()
        }
        
    }
    
    func showWarningRightView(){
        let errorButton = UIButton(type: .custom)
        errorButton.tag = 999
        errorButton.setImage(Image.warning, for: .normal)
        errorButton.frame = CGRect(x: 0, y: 0, width: frame.size.height, height: frame.size.height)
        errorButton.tintColor = .redTY
        errorButton.addTarget(self, action: #selector(errorAction), for: .touchUpInside)
        textField.rightView = errorButton
        textField.rightViewMode = .always
    }

}
// MARK: - Extension

extension BranchTextFieldView {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        textField.layer.cornerRadius = 5
        errorLabel.text = "Error"
        errorLabel.adjustsFontSizeToFitWidth = true
        errorLabel.minimumScaleFactor = 0
        
        
    }
    
    private func layout(){
        addSubview(titleLabel)
        addSubview(textField)
        //title label
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            textField.topAnchor.constraint(equalToSystemSpacingBelow: titleLabel.bottomAnchor, multiplier: 1),
            textField.bottomAnchor.constraint(equalTo: bottomAnchor),
            textField.leadingAnchor.constraint(equalTo: leadingAnchor),
            textField.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])
        
        titleLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
    }
    
    private func makeTextfieldPlaceholder(placeholder: String) -> NSAttributedString {
        var attrs = [NSAttributedString.Key: AnyObject]()
        attrs[.foregroundColor] = UIColor.lightGray
        attrs[.font] = UIFont.systemFont(ofSize: 15)
        
        let placeholderAttrs = NSAttributedString(string: placeholder, attributes: attrs)
        
        return placeholderAttrs
    }
}

extension BranchTextFieldView: UITextFieldDelegate{
    func textFieldDidChangeSelection(_ textField: UITextField) {
        delegate?.didTextFieldChanged(view: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
}


