//
//  InformationView.swift
//  ThuY-Project
//
//  Created by AnhLe on 08/05/2022.
//

import UIKit

class InformationBranchView: UIView {
    // MARK: - Subview
    private let titleLabel = LabelCustom(color: .greenTY, fontFamily: UIFont.preferredFont(forTextStyle: .headline), alignment: .left)
    
    let contentLabel = LabelCustom(color: .black, fontFamily: UIFont.boldSystemFont(ofSize: 16), alignment: .left)
    
    // MARK: - Properties
    var contenLabelLeadingConstant: CGFloat = UIScreen.main.bounds.size.width * 0.3 + 16 + 16
    
    // 16: padding from view leading to content leading
    // 16 : padding from title trailing to content leading
    static let contenLabelWidth = UIScreen.main.bounds.size.width - (UIScreen.main.bounds.size.width * 0.3 + 16 + 16)
    
    // MARK: - Lifecycle
    
    convenience init(title: String, content: String) {
        self.init(frame: .zero)
        titleLabel.text = title
        contentLabel.text = content
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: 44)
    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension

extension InformationBranchView {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        contentLabel.lineBreakMode = .byWordWrapping
        contentLabel.numberOfLines = 0

    }
    
    private func layout(){
        addSubview(titleLabel)
        addSubview(contentLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
        
        ])
        
        NSLayoutConstraint.activate([
            contentLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: contenLabelLeadingConstant),
            contentLabel.topAnchor.constraint(equalTo: titleLabel.topAnchor),
            contentLabel.trailingAnchor.constraint(equalTo: trailingAnchor)
        
        ])
    }
}

