//
//  BranchTableCell.swift
//  ThuY-Project
//
//  Created by AnhLe on 02/05/2022.
//

import UIKit
protocol BranchTableCellDelegate: AnyObject {
    func didCellSelected(_ cell: BranchTableCell, isSelected: Bool)
}
class BranchTableCell: UITableViewCell {
    // MARK: - Subview
    private let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 5
        view.backgroundColor = .white
        return view
    }()
    
    private let avatarImageView: UIView = {
        let imageView = UIView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .orangeTY
        imageView.clipsToBounds = true
        return imageView
    }()
    
    private let characterLabel = LabelCustom(color: .white, fontFamily: .preferredFont(forTextStyle: .largeTitle), alignment: .center)
    
    private let titleLabel = LabelCustom(color: .black, fontFamily: UIFont.boldSystemFont(ofSize: 16), alignment: .left)
    
    private let addressLabel = LabelCustom(color: .black, fontFamily: UIFont.preferredFont(forTextStyle: .footnote), alignment: .left)
    
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.alignment = .top
        
        stackView.spacing = 8
        return stackView
    }()
    
    private let labelStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 8
        return stackView
    }()
    
    let circleButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        let configuration = UIImage.SymbolConfiguration(scale: .large)
        button.setImage(Image.circle.withConfiguration(configuration), for: .normal)
        
        button.setImage(Image.checkmark.withConfiguration(configuration), for: .selected)
        button.tintColor = .greenTY
        
        return button
    }()
    
    // MARK: - Properties
    private let avatarHeight: CGFloat = 50
    
    static let reuseIdentifier = "BranchTableCell"
    static let rowHeight: CGFloat = 90
    
    var containerViewLeadingConstraint: NSLayoutConstraint!
    
    var cellSelected: Bool = false
    
    weak var delegate: BranchTableCellDelegate?
    
    // MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Selector
    @objc func didCellLongPressed(_ sender: UILongPressGestureRecognizer){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            // 24 + 8 + 8 = cirle size + circle leading + circle trailing
            self.containerViewLeadingConstraint.constant = 24 + 8 + 8
            self.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc func cancelCellLongPressed(_ sender: UILongPressGestureRecognizer){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            // 24 + 8 + 8 = cirle size + circle leading + circle trailing
            self.containerViewLeadingConstraint.constant = 0
            self.layoutIfNeeded()
        }, completion: nil)
    }
    
    @objc func didCircleButtonTapped(_ sender: UIButton){
        //hide & unhide checkmark
        self.circleButton.isSelected.toggle()
        cellSelected.toggle()
        delegate?.didCellSelected(self, isSelected: cellSelected)
    }
    
    // MARK: - API
    
    // MARK: - Helper
    
    func relayoutCell(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseInOut, animations: {
            // 24 + 8 + 8 = cirle size + circle leading + circle trailing
            self.containerViewLeadingConstraint.constant = 0
            self.layoutIfNeeded()
        }, completion: nil)
    }
    
    func bindingData(branch: BranchData){
//        idView.idLabel.text = branch.id
        titleLabel.text = branch.name
        addressLabel.text = "\(branch.address)"
        characterLabel.text = String(branch.name.prefix(1)).uppercased()

    }
    
    private func registerLongPressGesture(){
        NotificationCenter.default.addObserver(self, selector: #selector(didCellLongPressed(_:)), name: .longCellPressed, object: nil)
    }
    
    private func registerCancelLongPressGesture(){
        NotificationCenter.default.addObserver(self, selector: #selector(cancelCellLongPressed(_:)), name: .cancelLongPressed, object: nil)
    }
}
// MARK: - Extension
extension BranchTableCell {
    
    func setup(){
        layer.cornerRadius = 5
        selectionStyle = .none
        backgroundColor = .clear
        
        addressLabel.text = "To dan pho 3, Thi Tran Nham Bien, Yen Dung, Bac Giang"
        
        titleLabel.text = "Pet Hospital"
        
        characterLabel.text = "A"
        
        avatarImageView.layer.cornerRadius = avatarHeight / 2
        
        circleButton.addTarget(self, action: #selector(didCircleButtonTapped(_:)), for: .touchUpInside)
        
        registerLongPressGesture()
        registerCancelLongPressGesture()
    }
    func layout(){
        contentView.addSubview(circleButton)
        contentView.addSubview(containerView)
        
        avatarImageView.addSubview(characterLabel)
        
        
         containerViewLeadingConstraint = containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor)
        
        NSLayoutConstraint.activate([
            circleButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            circleButton.leadingAnchor.constraint(equalToSystemSpacingAfter: contentView.leadingAnchor, multiplier: 1),
            circleButton.widthAnchor.constraint(equalToConstant: 24),
            circleButton.heightAnchor.constraint(equalTo: circleButton.widthAnchor, multiplier: 1)
        ])
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalToSystemSpacingBelow: contentView.topAnchor, multiplier: 1),
            containerViewLeadingConstraint,
            containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            contentView.bottomAnchor.constraint(equalToSystemSpacingBelow: containerView.bottomAnchor, multiplier: 1)
            
        ])
        
        
        //avatarImageView
        NSLayoutConstraint.activate([
            avatarImageView.heightAnchor.constraint(equalToConstant: avatarHeight),
            avatarImageView.widthAnchor.constraint(equalTo: avatarImageView.heightAnchor, multiplier: 1)
        
        ])
        
        //CharacterLabel
        NSLayoutConstraint.activate([
            characterLabel.centerYAnchor.constraint(equalTo: avatarImageView.centerYAnchor),
            characterLabel.centerXAnchor.constraint(equalTo: avatarImageView.centerXAnchor)
        ])
        
        //stackView
        containerView.addSubview(stackView)
        stackView.addArrangedSubview(avatarImageView)
        stackView.addArrangedSubview(labelStackView)
//        stackView.addArrangedSubview(idView)
        
        //label stackview
        labelStackView.addArrangedSubview(titleLabel)
        labelStackView.addArrangedSubview(addressLabel)
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalToSystemSpacingAfter: containerView.leadingAnchor, multiplier: 1),
            containerView.trailingAnchor.constraint(equalToSystemSpacingAfter: stackView.trailingAnchor, multiplier: 1),
            stackView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)
        ])
        
        //set hug priority
        titleLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
        addressLabel.setContentHuggingPriority(.defaultLow, for: .vertical)
        
        avatarImageView.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        labelStackView.setContentHuggingPriority(.defaultLow, for: .horizontal)
//        idView.setContentHuggingPriority(.defaultHigh, for: .horizontal)
    }
}
