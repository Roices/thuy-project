//
//  SearchView.swift
//  ThuY-Project
//
//  Created by AnhLe on 02/05/2022.
//

import UIKit

protocol SearchViewDelegate: AnyObject {
    func textFieldDidChangeSelection(_ text: String)
    
    func textFieldDidEndEditing(_ text: String)
}

class SearchView: UIView {
    // MARK: - Subview
    let searchTextField: UITextField = {
        let textfield = UITextField()
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.placeholder = "Tên chi nhánh"
        textfield.textColor = .darkGrayTY
        textfield.clearButtonMode = .whileEditing
        return textfield
    }()
    
    let searchImage = ImageViewCustom(frame: .zero)
    
    
    // MARK: - Properties
    weak var delegate: SearchViewDelegate?
    
    // MARK: - Lifecycle
    convenience init(placeholder: String) {
        self.init(frame: .zero)
        self.searchTextField.placeholder = placeholder
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: 50)
    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension

extension SearchView {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = 5
        backgroundColor = .white
        
        searchTextField.delegate = self
        
        searchImage.image = Image.search
        searchImage.tintColor = .darkGray
        

    }
    
    private func layout(){
        addSubview(searchTextField)
        addSubview(searchImage)
        
        //search image
        NSLayoutConstraint.activate([
            searchImage.heightAnchor.constraint(equalToConstant: 24),
            searchImage.widthAnchor.constraint(equalTo: searchImage.heightAnchor, multiplier: 1),
            searchImage.centerYAnchor.constraint(equalTo: centerYAnchor),
            searchImage.leadingAnchor.constraint(equalToSystemSpacingAfter: leadingAnchor, multiplier: 1)
        ])
        
        //search text field
        NSLayoutConstraint.activate([
            searchTextField.topAnchor.constraint(equalToSystemSpacingBelow: topAnchor, multiplier: 1),
            bottomAnchor.constraint(equalToSystemSpacingBelow: searchTextField.bottomAnchor, multiplier: 1),
            searchTextField.leadingAnchor.constraint(equalToSystemSpacingAfter: searchImage.trailingAnchor, multiplier: 1),
            trailingAnchor.constraint(equalToSystemSpacingAfter: searchTextField.trailingAnchor, multiplier: 0)
        ])
    }
}

extension SearchView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        

        guard let text = textField.text else {
            return true
        }
        delegate?.textFieldDidEndEditing(text)
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        guard let text = textField.text else {
            return
        }
        delegate?.textFieldDidChangeSelection(text)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard textField.text != nil else {
            return
        }
    }
}

