//
//  IDView.swift
//  ThuY-Project
//
//  Created by AnhLe on 02/05/2022.
//

import UIKit

class IDBranchView: UIView {
    // MARK: - Subview
    let idLabel = LabelCustom(color: .white, fontFamily: UIFont.systemFont(ofSize: 10), alignment: .center)
    
    // MARK: - Properties
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 67, height: 18)
    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension

extension IDBranchView {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = (10 + 8) / 2
        backgroundColor = .greenTY
    }
    
    private func layout(){
        addSubview(idLabel)
        idLabel.text = "1232131"
        
        NSLayoutConstraint.activate([
            idLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            idLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
            idLabel.topAnchor.constraint(equalTo: topAnchor, constant: 4),
            idLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4)
        ])
    }
}

