//
//  AddSpecies.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 29/04/2022.
//

import UIKit

class AddSpecies: UIView {
    @IBOutlet weak var txtNameSpecies: UITextField!
    @IBOutlet weak var txtNote: UITextField!
    @IBOutlet weak var btnAdd: UIButton!
    var root : UIViewController!
    override func awakeFromNib() {
        super.awakeFromNib()
        txtNameSpecies.layer.cornerRadius = 10
        txtNote.layer.cornerRadius = 20
        btnAdd.layer.cornerRadius = btnAdd.frame.height/2
    }
    
    @IBAction func btnAddClick(_ sender: Any) {
//        self.root.navigationController?.popViewController(animated: true)
    }
}
