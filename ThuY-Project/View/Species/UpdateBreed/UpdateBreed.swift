//
//  UpdateBreed.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 29/04/2022.
//

import UIKit
import iOSDropDown

class UpdateBreed: UIView {
    var id : String?
    @IBOutlet weak var txtNameBreed: UITextField!
    @IBOutlet weak var txtNameSpecies: DropDown!
    @IBOutlet weak var txtNote: UITextField!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    var root : UIViewController!
    override func awakeFromNib() {
        super.awakeFromNib()
        txtNameBreed.layer.cornerRadius = 10
        txtNameSpecies.layer.cornerRadius = 20
        btnUpdate.layer.cornerRadius = btnUpdate.frame.height/2
        btnCancel.layer.cornerRadius = btnCancel.frame.height/2
    }
    
    func setData(obj: BreedDataModel){
        txtNameBreed.text = obj.name
        txtNameSpecies.text = obj.type!.name + " - " + obj.type!.id
        txtNote.text = obj.note
    }

    @IBAction func btnBackClick(_ sender: Any) {
        root.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnUpdateClick(_ sender: Any) {
    }
    
    @IBAction func btnCancelClick(_ sender: Any) {
        if let idDelete = id {
            commomSevice().deleteApiBreed(id: idDelete)
        }
        self.root.navigationController?.popViewController(animated: true)
    }
    
}
