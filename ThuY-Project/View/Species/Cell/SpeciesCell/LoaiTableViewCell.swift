//
//  LoaiTableViewCell.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 28/04/2022.
//

import UIKit

class LoaiTableViewCell: UITableViewCell {

    @IBOutlet weak var viewLoai: UIView!
    @IBOutlet weak var lbLoai: UILabel!
    @IBOutlet weak var viewNumberLoai: UIView!
    @IBOutlet weak var lbNumber: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewLoai.layer.cornerRadius = 20
        viewNumberLoai.layer.cornerRadius = viewNumberLoai.frame.height/2
    }

    func setData(obj: SpeciesDataModel){
        lbLoai.text = obj.name
        lbNumber.text = obj.id
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
