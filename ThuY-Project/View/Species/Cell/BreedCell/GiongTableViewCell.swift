//
//  GiongTableViewCell.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 28/04/2022.
//

import UIKit

class GiongTableViewCell: UITableViewCell {
    @IBOutlet weak var viewGiong: UIView!
    @IBOutlet weak var viewLbNumber: UIView!
    @IBOutlet weak var lb_Giong: UILabel!
    @IBOutlet weak var lb_Loai: UILabel!
    @IBOutlet weak var lb_NumberGiong: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        viewGiong.layer.cornerRadius = 20
        viewLbNumber.layer.cornerRadius = viewLbNumber.frame.height/2
    }

    func setData(obj: BreedDataModel){
        lb_Giong.text = obj.name
        lb_Loai.text = obj.type?.name
        lb_NumberGiong.text = obj.id
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
