//
//  UpdateSpecies.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 29/04/2022.
//

import UIKit

class UpdateSpecies: UIView {
    var id : String?
    @IBOutlet weak var txtNameSpecies: UITextField!
    @IBOutlet weak var txtNote: UITextField!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    var root : UIViewController!
    override func awakeFromNib() {
        txtNameSpecies.layer.cornerRadius = 10
        txtNote.layer.cornerRadius = 20
        btnUpdate.layer.cornerRadius = btnUpdate.frame.height/2
        btnCancel.layer.cornerRadius = btnCancel.frame.height/2
        super.awakeFromNib()
    }
    
    func setData(obj: SpeciesDataModel){
        txtNameSpecies.text = obj.name
        txtNote.text = obj.note
    }
    
    @IBAction func btnUpdateClick(_ sender: Any) {
//        self.root.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCancelClick(_ sender: Any) {
       
    }
}
