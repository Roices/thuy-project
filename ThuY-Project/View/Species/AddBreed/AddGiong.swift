//
//  AddGiong.swift
//  ThuY-Project
//
//  Created by Tuan on 16/05/2022.
//

import UIKit
import iOSDropDown

class AddGiong: UIView {
    @IBOutlet weak var txtNameBreed: UITextField!
    @IBOutlet weak var txtNameSpecies: DropDown!
    @IBOutlet weak var txtNote: UITextField!
    @IBOutlet weak var btnAdd: UIButton!
    var root : UIViewController!
    override func awakeFromNib() {
        super.awakeFromNib()
        txtNameBreed.layer.cornerRadius = 10
        txtNameSpecies.layer.cornerRadius = 10
        txtNote.layer.cornerRadius = 20
        btnAdd.layer.cornerRadius = btnAdd.frame.height/2
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.root.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnAddClick(_ sender: Any) {
       
    }
}
