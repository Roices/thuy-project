//
//  Species.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 28/04/2022.
//

import Foundation
import UIKit

class Species: UIView {
    @IBOutlet weak var btnGiong: UIButton!
    @IBOutlet weak var btnLoai: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var btnSearch: UIButton!
    var arr : [String] = []
    var root : UIViewController!
    var objSpecies: SpeciesModel?
    var objBreed: BreedModel?
    override func awakeFromNib() {
        super.awakeFromNib()
        btnAdd.layer.cornerRadius = btnAdd.frame.height/2
        txtSearch.layer.cornerRadius = 10
        btnGiong.layer.cornerRadius = 10
        btnLoai.layer.cornerRadius = 10
        uiView()
        
    }
  
    
    @IBAction func btnSearchClick(_ sender: Any) {
    }
    @IBAction func btnGiongClick(_ sender: Any) {
        btnLoai.layer.backgroundColor = UIColor.white.cgColor
        btnGiong.layer.backgroundColor = UIColor.init(hexString:"70B08D").cgColor
        SpeciesViewController.type = .giong
        tbView.reloadData()
    }
    
    func uiView(){
        if SpeciesViewController.type == .giong{
            btnLoai.layer.backgroundColor = UIColor.white.cgColor
            btnGiong.layer.backgroundColor = UIColor.init(hexString:"70B08D").cgColor
        }else{
            btnGiong.layer.backgroundColor = UIColor.white.cgColor
            btnLoai.layer.backgroundColor = UIColor.init(hexString:"70B08D").cgColor
        }
    }
    
    @IBAction func btnLoaiClick(_ sender: Any) {
        btnGiong.layer.backgroundColor = UIColor.white.cgColor
        btnLoai.layer.backgroundColor = UIColor.init(hexString:"70B08D").cgColor
        SpeciesViewController.type = .loai
        
        tbView.reloadData()
    }
    
    @IBAction func btnAddClick(_ sender: Any) {
        if SpeciesViewController.type == .giong {
            HandlePush().gotoAddBreedViewController(root: root,arr: arr)
        }else {
            HandlePush().gotoAddSpeciesViewController(root: root)
        }
        
    }
    
}
