//
//  FooterSideMenuView.swift
//  ThuY-Project
//
//  Created by AnhLe on 29/04/2022.
//

import UIKit
protocol FooterSideMenuViewDelegate: AnyObject {
    func didUserClicked()
    func didLogoutButtonTapped()
}
class FooterSideMenuView: UIView {
    // MARK: - Subview
    let avatarImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = .orangeTY
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        return imageView
    }()
    
    let roleLabel = LabelCustom(color: .white, fontFamily: UIFont.boldSystemFont(ofSize: 16), alignment: .left)
    let idRoleLabel = LabelCustom(color: .white, fontFamily: UIFont.preferredFont(forTextStyle: .footnote), alignment: .left)
    
    let logOutButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
//        let configuration = UIImage.SymbolConfiguration(pointSize: 24, weight: .medium)
        let image = Image.logout
        button.setImage(image, for: .normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(didLogoutButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    private let stackView: UIStackView = {
       let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = 12
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        return stackView
    }()
    
    private let labelStackView: UIStackView = {
       let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 0
        stackView.axis = .vertical
        return stackView
    }()
    
    private let dividerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.heightAnchor.constraint(equalToConstant: 1).isActive = true
        return view
        
    }()
    // MARK: - Properties
    let imageViewHeight: CGFloat = 45
    
    weak var delegate: FooterSideMenuViewDelegate?
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: 80)
    }
    
    // MARK: - Selector
    @objc func didLogoutButtonTapped(_ sender: UIButton){
        delegate?.didLogoutButtonTapped()
    }
    
    @objc func didInforUserTapped(_ sender: UIButton){
        delegate?.didUserClicked()
    }
    
    @objc func didAvatarTapped(_ sender: UIButton){
        delegate?.didUserClicked()
    }
    
    // MARK: - API
    func bindingData(model: LoginModel){
        roleLabel.text =  model.user.name
        idRoleLabel.text = model.user.id
    }
    // MARK: - Helper
    
}
// MARK: - Extension

extension FooterSideMenuView {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        
 
        backgroundColor = .clear
        
        avatarImageView.layer.cornerRadius = imageViewHeight / 2
        
        let userGesture = UITapGestureRecognizer(target: self, action: #selector(didInforUserTapped(_:)))
        
        let avatarGesture = UITapGestureRecognizer(target: self, action: #selector(didAvatarTapped(_:)))
//        roleLabel.isUserInteractionEnabled = true
//        roleLabel.addGestureRecognizer(userGesture)
        
//        idRoleLabel.isUserInteractionEnabled = true
//        idRoleLabel.addGestureRecognizer(userGesture)
        labelStackView.isUserInteractionEnabled = true
        labelStackView.addGestureRecognizer(userGesture)
//
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.addGestureRecognizer(avatarGesture)
        
    }
    
    private func layout(){
        addSubview(stackView)
        addSubview(dividerView)
        
        stackView.addArrangedSubview(avatarImageView)
        stackView.addArrangedSubview(labelStackView)
        stackView.addArrangedSubview(logOutButton)
        
        //dividerView
        NSLayoutConstraint.activate([
            dividerView.topAnchor.constraint(equalTo: topAnchor),
            dividerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            dividerView.trailingAnchor.constraint(equalTo: trailingAnchor)
        ])

        //labelStackView
        labelStackView.addArrangedSubview(roleLabel)
        labelStackView.addArrangedSubview(idRoleLabel)

        //logout button
        NSLayoutConstraint.activate([
            logOutButton.heightAnchor.constraint(equalToConstant: 24),
            logOutButton.widthAnchor.constraint(equalTo: logOutButton.heightAnchor, multiplier: 1)

        ])
        
        //imageView
        NSLayoutConstraint.activate([
            avatarImageView.heightAnchor.constraint(equalToConstant: imageViewHeight),
            avatarImageView.widthAnchor.constraint(equalTo: avatarImageView.heightAnchor, multiplier: 1)
        
        ])
        
        //stackView
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor)
            
        ])


        roleLabel.setContentHuggingPriority(.defaultLow, for: .vertical)
        idRoleLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)

        avatarImageView.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        logOutButton.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        labelStackView.setContentHuggingPriority(.defaultLow, for: .horizontal)
    }
}

