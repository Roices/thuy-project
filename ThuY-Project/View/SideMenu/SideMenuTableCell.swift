//
//  SideMenuTableCell.swift
//  ThuY-Project
//
//  Created by AnhLe on 30/04/2022.
//

import UIKit

class SideMenuTableCell: UITableViewCell {
    // MARK: - Subview
    let imgView = ImageViewCustom(frame: .zero)
    let titleLabel = LabelCustom(color: .white, fontFamily: UIFont.preferredFont(forTextStyle: .headline), alignment: .left)
    
    private let stackView: UIStackView = {
       let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 16
        stackView.axis = .horizontal
        stackView.alignment = .leading
        return stackView
    }()
    
    // MARK: - Properties
    static let reuseIdentifier = "SideMenuTableCell"
    static let rowHeight: CGFloat = 72
    
    // MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension
extension SideMenuTableCell {
    
    func setup(){
        selectionStyle = .none
        backgroundColor = .clear
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.5
    }
    func layout(){
        addSubview(stackView)
                
        //imgView
        NSLayoutConstraint.activate([
            imgView.heightAnchor.constraint(equalToConstant: 24),
            imgView.widthAnchor.constraint(equalTo: imgView.heightAnchor, multiplier: 1)
        ])
        
        //stackView
        stackView.addArrangedSubview(imgView)
        stackView.addArrangedSubview(titleLabel)
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalToSystemSpacingAfter: contentView.leadingAnchor, multiplier: 2),
            stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor)
        
        ])
        
        imgView.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        titleLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)
    }
}
