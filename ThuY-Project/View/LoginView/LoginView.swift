//
//  Login.swift
//  ThuY-Project
//
//  Created by Tuan on 04/05/2022.
//
///
import Foundation
import UIKit

protocol LoginViewDelegate: NSObject {
    func loginToApp(username: String, password: String)
    
    func forgotPassword()
}


class LoginView: UIView {
    //
    // MARK: - Outlet
    //
    @IBOutlet weak var staffID: UITextField!
    
    @IBOutlet weak var password: UITextField!
    
    let secureTextEntryBtn = UIButton()
    
    weak var delegate: LoginViewDelegate!
    
    @IBOutlet weak var scrollView: UIScrollView!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
        keyboardNotification()
    }
    
    
    @IBAction private func loginAction() {
        
        guard let staffID = staffID.text, let password = password.text else { return }
        
        self.delegate?.loginToApp(username: staffID, password: password)

    }
    
    @IBAction func forgotPassword() {
        self.delegate.forgotPassword()
    }
    
    func setup() {
        // Delegate for textfields ---------------
        staffID.delegate = self
        password.delegate = self
        
        // Set up for secureTextEntryBtn ----------------
        secureTextEntryBtn.setImage(UIImage(named: "btn_EyeOn"), for: .normal)
        secureTextEntryBtn.imageEdgeInsets = UIEdgeInsets(top: 0, left: -20, bottom: 0, right: 0)
        secureTextEntryBtn.frame = CGRect(x: CGFloat(password.frame.size.width - 20), y: CGFloat(5), width: CGFloat(20), height: CGFloat(20))
        secureTextEntryBtn.addTarget(self, action: #selector(self.refresh), for: .touchUpInside)
        
        // Set right icon button for password Textfield ---------------
        password.rightView = secureTextEntryBtn
        password.rightViewMode = .always
        
        self.addSubview(secureTextEntryBtn)
    }
    
    // Button Action Secure Text Entry -----------------
    @objc func refresh() {
        if password.isSecureTextEntry {
           secureTextEntryBtn.setImage(UIImage(named: "btn_EyeOff"), for: .normal)
           password.isSecureTextEntry = !password.isSecureTextEntry
        }else {
           secureTextEntryBtn.setImage(UIImage(named: "btn_EyeOn"), for: .normal)
           password.isSecureTextEntry = !password.isSecureTextEntry
        }
    }
}


// MARK: - Config keyboard

extension LoginView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Config Keyboard

    // Keyboard notification ---------------------------
    func keyboardNotification() {
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }

    // increase height of scrollView when tap to textfield ------------------
    @objc func keyboardWillShow(notification: NSNotification) {
            guard let userInfo = notification.userInfo else { return }
            let keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue

            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height/3
            scrollView.contentInset = contentInset

        }
    // decrease height of scrollView when tap to textfield --------------------
    @objc func keyboardWillHide(notification: NSNotification) {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            scrollView.contentInset = contentInset

        }

    
}
