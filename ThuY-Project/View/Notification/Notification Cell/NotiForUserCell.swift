//
//  CanceledCell.swift
//  ThuY-Project
//
//  Created by Tuan on 06/05/2022.
//

import UIKit

class NotiForUserCell: UITableViewCell {

    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var time: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        guard let branch = UserDefaults.standard.string(forKey: "branchName") else { return }
        address.text = branch
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
