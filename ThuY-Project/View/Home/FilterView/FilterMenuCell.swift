//
//  FilterMenuCell.swift
//  ThuY-Project
//
//  Created by AnhLe on 26/05/2022.
//

import UIKit

class FilterMenuCell: UITableViewCell {
    // MARK: - Subview
    let selectionLabel: UILabel = LabelCustom(color: .greenTY, fontFamily: UIFont.preferredFont(forTextStyle: .callout), alignment: .left)
    
    let selectMark: UIImageView = {
       let imageView = UIImageView()
        imageView.image = Image.checkmark
        imageView.tintColor = .greenTY
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    // MARK: - Properties
    
    var didSelected: Bool = false{
        didSet{
            selectMark.isHidden = !didSelected
        }
    }
    
    static let reuseIdentifier = "FilterMenuCell"
    static let rowHeight = 44
    
    // MARK: - Lifecycle
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    //give default size, if they put into stackview, this view will know how to size itself
//    override var intrinsicContentSize: CGSize {
//        return CGSize(width: 200, height: 200)
//    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
    func bindingData(branch: BranchData){
        selectionLabel.text = branch.name
    }
}
// MARK: - Extension

extension FilterMenuCell {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        selectionStyle = .none
        backgroundColor = .white
        selectionLabel.text = "Item 1"
        selectionLabel.adjustsFontSizeToFitWidth = true
        selectionLabel.minimumScaleFactor = 0.5
        
        selectMark.isHidden = true
    }
    
    private func layout(){
        contentView.addSubview(selectionLabel)
        contentView.addSubview(selectMark)
        NSLayoutConstraint.activate([
            selectionLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            selectionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16),

        ])
        
        NSLayoutConstraint.activate([
            selectMark.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            selectMark.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8)
        ])
    }
}

