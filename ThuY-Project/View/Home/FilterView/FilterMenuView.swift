//
//  FilterMenuView.swift
//  ThuY-Project
//
//  Created by AnhLe on 26/05/2022.
//

import UIKit
protocol FilterMenuViewDelegate: AnyObject {
    func didFilterCellSelected(branch: BranchData?, isAll: Bool)
}
class FilterMenuView: UIView {
    // MARK: - Subview
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.layer.cornerRadius = 10
        return tableView
    }()
    
    let selectMark: UIImageView = {
       let imageView = UIImageView()
        imageView.image = Image.checkmark
        imageView.tintColor = .greenTY
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    var footerView: UIView!
    
    // MARK: - Properties
    var branches: [BranchData]?{
        didSet{
            tableView.reloadData()
        }
    }
    
    var showMenu: Bool?{
        didSet{}
    }
    
    weak var delegate: FilterMenuViewDelegate?
    
    var cellSelected = [IndexPath]()
    
    var filterCell: FilterMenuCell?
    
//    var indexPathSelected: IndexPath?
    // MARK: - Lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 200, height: 50)
    }
    
    // MARK: - Selector
    @objc func footerViewTapped(_ sender: UITapGestureRecognizer){
        if let cell = filterCell{
            cell.didSelected = false

        }
        delegate?.didFilterCellSelected(branch: nil, isAll: true)
        selectMark.isHidden = false
        
    }
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension

extension FilterMenuView {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        configureTableView()
        backgroundColor = .clear

        
    }
    
    private func layout(){
        addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leadingAnchor.constraint(equalTo: leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        
        ])
    }
    
    private func configureTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(FilterMenuCell.self, forCellReuseIdentifier: FilterMenuCell.reuseIdentifier)
        footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 30))
        tableView.rowHeight = CGFloat(FilterMenuCell.rowHeight)
        let alllabel = LabelCustom(color: .redTY, fontFamily: UIFont.preferredFont(forTextStyle: .body), alignment: .left)
        footerView.addSubview(alllabel)
        footerView.addSubview(selectMark)
        alllabel.text = "Tất cả"
        
        alllabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            alllabel.centerYAnchor.constraint(equalTo: footerView.centerYAnchor),
            alllabel.leadingAnchor.constraint(equalToSystemSpacingAfter: footerView.leadingAnchor, multiplier: 2)
        ])
        
        NSLayoutConstraint.activate([
            selectMark.centerYAnchor.constraint(equalTo: footerView.centerYAnchor),
            selectMark.trailingAnchor.constraint(equalTo: footerView.trailingAnchor, constant: -8),
        ])
        
        let userGesture = UITapGestureRecognizer(target: self, action: #selector(footerViewTapped(_:)))
        footerView.addGestureRecognizer(userGesture)
        footerView.isUserInteractionEnabled = true
        selectMark.isHidden = true
        tableView.tableFooterView = footerView
        
    }
}

extension FilterMenuView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let cell = self.filterCell {
            cell.didSelected = false
        }
        guard let branches = self.branches else {return}
        filterCell = tableView.cellForRow(at: indexPath) as? FilterMenuCell
        guard let cell = filterCell else {return}
        if cell.isSelected {
            tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            cell.didSelected = true
            delegate?.didFilterCellSelected(branch: branches[indexPath.row], isAll: false)
            selectMark.isHidden = true
        }else{
            cell.didSelected = false
            
        }
    }
}

extension FilterMenuView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return branches?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FilterMenuCell.reuseIdentifier, for: indexPath) as! FilterMenuCell
        guard let branch = branches?[indexPath.row] else {return UITableViewCell()}

        cell.bindingData(branch: branch)
        return cell
    }
    
    
}

