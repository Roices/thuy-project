//
//  TranslucentImageView.swift
//  ThuY-Project
//
//  Created by AnhLe on 29/04/2022.
//
import UIKit

class TranslucentImageView: UIView {
    // MARK: - Subview
    let imageView = ImageViewCustom(frame: .zero)
    
    // MARK: - Properties
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 30, height: 30)
    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension

extension TranslucentImageView {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        self.layer.cornerRadius = 5
        backgroundColor = UIColor.white.withAlphaComponent(0.3)
        imageView.image = Image.people
    }
    
    private func layout(){
        addSubview(imageView)
        
        //imageview
        NSLayoutConstraint.activate([
            imageView.heightAnchor.constraint(equalToConstant: 18),
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 1),
            imageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            imageView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
}
