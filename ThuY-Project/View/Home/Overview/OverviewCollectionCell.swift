//
//  OverviewCollectionCell.swift
//  ThuY-Project
//
//  Created by AnhLe on 27/04/2022.
//

import UIKit

class OverviewCollectionCell: UICollectionViewCell {
    // MARK: - Subview
    let titleLabel = LabelCustom(color: .white, fontFamily: UIFont.preferredFont(forTextStyle: .callout), alignment: .left)
    let quatityLabel = LabelCustom(color: .white, fontFamily: UIFont.boldSystemFont(ofSize: 22), alignment: .left)
    let translucentImage = TranslucentImageView()
    
    private let stackView: UIStackView = {
       let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 12
        stackView.axis = .horizontal
        stackView.alignment = .center
        return stackView
    }()
    
    private let labelStackView: UIStackView = {
       let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.axis = .vertical
        stackView.spacing = 0
        
        return stackView
    }()
    // MARK: - Properties
    static let reuseIdentifier = "OverviewCollectionCell"
    static let cellHeight: CGFloat = 70
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper

}
// MARK: - Extension
extension OverviewCollectionCell {
    
    func setup(){
        layer.cornerRadius = 5
    }
    func layout(){
        addSubview(stackView)
        titleLabel.text = "Chi nhánh"
        quatityLabel.text = "100"
        stackView.addArrangedSubview(translucentImage)
        //labelstackView
        labelStackView.addArrangedSubview(quatityLabel)
        labelStackView.addArrangedSubview(titleLabel)
        
        stackView.addArrangedSubview(labelStackView)
        //stackView
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: centerYAnchor),
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        quatityLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
        titleLabel.setContentHuggingPriority(.defaultLow, for: .vertical)
        
        
        
    }
}
