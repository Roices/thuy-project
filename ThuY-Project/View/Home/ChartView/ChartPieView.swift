//
//  DonutChartViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 29/04/2022.
//

import UIKit
import Charts

protocol ChartPieViewDelegate: AnyObject {
    func didDetailButtonTapped(view: UIView)
}

class ChartPieView: UIView {
    // MARK: - Subview
    let titleLabel = LabelCustom(color: .greenTY, fontFamily: UIFont.preferredFont(forTextStyle: .headline), alignment: .left)

    lazy var viewDetailButton: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("Xem chi tiết", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.darkGray, for: .normal)
        button.titleLabel?.font = UIFont.preferredFont(forTextStyle: .footnote)
        button.addTarget(self, action: #selector(didViewDetailTapped(_:)), for: .touchUpInside)
        return button
    }()
    lazy var pieChartView: PieChartView = {
       let view = PieChartView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
        
    // MARK: - Properties
    weak var delegate: ChartPieViewDelegate?

    
    // MARK: - Lifecycle
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Selector
    @objc func didViewDetailTapped(_ sender: UIButton){
        delegate?.didDetailButtonTapped(view: self)
        
    }
    
    // MARK: - API
    
    // MARK: - Helper
    func customizeChart(dataPoints: [String], values: [Double]){
        var dataEntries: [ChartDataEntry] = []
        
        //set Chart data entry
        for i in 0..<dataPoints.count {
            let dataEntry = PieChartDataEntry(value: values[i], label: dataPoints[i], data: dataPoints[i] as AnyObject)
            dataEntries.append(dataEntry)
        }
        
        //set chart dataset
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: "")
        pieChartDataSet.colors = colorsOfChart(numberOfColor: dataPoints.count)

        //set chartData
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        //add %
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 1.0
        
        pieChartDataSet.valueFormatter = ChartValueFormatter(numberFormatter: formatter) as ValueFormatter

        //assign to chart view
        pieChartView.data = pieChartData
        pieChartView.usePercentValuesEnabled = true
        pieChartView.legend.enabled = false
        pieChartView.rotationEnabled = false
        
    }
    
    func colorsOfChart(numberOfColor: Int) -> [UIColor]{
        let colors: [UIColor] = [.yellowTY, .blueTY, .greenTY, .orangeTY, .systemPink, .systemPurple, .systemIndigo, .systemTeal]
        var result = [UIColor]()
        for i in 0..<numberOfColor{
            result.append(colors[i])
        }
        return result
    }
}
// MARK: - Extension
extension ChartPieView {
    
    func setup(){
        titleLabel.text = "Thú nuôi"
        pieChartView.isUserInteractionEnabled = false
        
    }
    func layout(){
        addSubview(titleLabel)
        addSubview(viewDetailButton)
        addSubview(pieChartView)

        //titleLabel
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalToSystemSpacingBelow: topAnchor, multiplier: 1),
            titleLabel.leadingAnchor.constraint(equalToSystemSpacingAfter: leadingAnchor, multiplier: 1)
        ])
        
        //viewDetailLabel
        NSLayoutConstraint.activate([
            viewDetailButton.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
            viewDetailButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8)
        ])
        
        
//        //pieChartView
        NSLayoutConstraint.activate([
            pieChartView.topAnchor.constraint(equalToSystemSpacingBelow: titleLabel.bottomAnchor, multiplier: 0),
            pieChartView.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            pieChartView.trailingAnchor.constraint(equalTo: viewDetailButton.trailingAnchor),
            pieChartView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4)
        ])
    }
}

class ChartValueFormatter: NSObject, ValueFormatter {
    fileprivate var numberFormatter: NumberFormatter?

    convenience init(numberFormatter: NumberFormatter) {
        self.init()
        self.numberFormatter = numberFormatter
    }

    func stringForValue(_ value: Double, entry: ChartDataEntry, dataSetIndex: Int, viewPortHandler: ViewPortHandler?) -> String {
        guard let numberFormatter = numberFormatter
            else {
                return ""
        }
        return numberFormatter.string(for: value)!
    }
}

