//
//  UserAvatarView.swift
//  ThuY-Project
//
//  Created by AnhLe on 25/05/2022.
//

import UIKit

class UserAvatarView: UIView {
    // MARK: - Subview
    private let avatarView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        
        return view
    }()
    
    let characterLabel = LabelCustom(color: .white, fontFamily: .preferredFont(forTextStyle: .largeTitle), alignment: .center)

    
    let nameLabel = LabelCustom(color: .black, fontFamily: UIFont.boldSystemFont(ofSize: 21), alignment: .center)
    // MARK: - Properties
    private let radius: CGFloat = 68
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: 120)
    }
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension

extension UserAvatarView {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        avatarView.backgroundColor = .greenTY
        
        avatarView.layer.cornerRadius = radius/2
        nameLabel.text = "Le Viet Anh"
        
        
        
    }
    
    private func layout(){
        addSubview(avatarView)
        addSubview(nameLabel)
        
        avatarView.addSubview(characterLabel)
        
        NSLayoutConstraint.activate([
            avatarView.heightAnchor.constraint(equalToConstant: radius),
            avatarView.widthAnchor.constraint(equalTo: avatarView.heightAnchor, multiplier: 1),
            avatarView.centerXAnchor.constraint(equalTo: centerXAnchor),
            avatarView.topAnchor.constraint(equalToSystemSpacingBelow: topAnchor, multiplier: 1)
        ])
        
        //CharacterLabel
        NSLayoutConstraint.activate([
            characterLabel.centerYAnchor.constraint(equalTo: avatarView.centerYAnchor),
            characterLabel.centerXAnchor.constraint(equalTo: avatarView.centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalToSystemSpacingBelow: avatarView.bottomAnchor, multiplier: 2),
            nameLabel.centerXAnchor.constraint(equalTo: centerXAnchor)
        
        ])
    }
}

