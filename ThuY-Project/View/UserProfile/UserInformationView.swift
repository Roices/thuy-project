//
//  UserInformationView.swift
//  ThuY-Project
//
//  Created by AnhLe on 25/05/2022.
//

import UIKit

class UserInformationView: UIView {
    // MARK: - Subview
    private let titleLabel = LabelCustom(color: .darkGray, fontFamily: UIFont.preferredFont(forTextStyle: .body), alignment: .left)
    
    private let dividerView: UIView = {
        let view = UIView()
        view.heightAnchor.constraint(equalToConstant: 2).isActive = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .greenTY
        return view
    }()
    
    let contentLabel = LabelCustom(color: .black, fontFamily: UIFont.preferredFont(forTextStyle: .headline), alignment: .left)
        
    // MARK: - Properties
    
    // MARK: - Lifecycle
    convenience init(title: String, content: String) {
        self.init(frame: .zero)
        titleLabel.text = title
        contentLabel.text = content
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
        override var intrinsicContentSize: CGSize {
            return CGSize(width: UIView.noIntrinsicMetric, height: 62)
        }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension

extension UserInformationView {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = "Username"
        contentLabel.text = "thoapro"
        
        contentLabel.adjustsFontSizeToFitWidth = true
        contentLabel.minimumScaleFactor = 0.3
    }
    
    private func layout(){
        addSubview(titleLabel)
        addSubview(contentLabel)
        addSubview(dividerView)
        
        //title label
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
        ])
        
        //content label
        NSLayoutConstraint.activate([
            contentLabel.topAnchor.constraint(equalToSystemSpacingBelow: titleLabel.bottomAnchor, multiplier: 1),
            contentLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            contentLabel.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor)
        ])
        
        //dividerView
        NSLayoutConstraint.activate([

            dividerView.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            dividerView.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            dividerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4)
        
        ])
        
        
    }
    
}

