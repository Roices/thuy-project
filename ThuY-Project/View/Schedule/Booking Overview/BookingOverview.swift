//
//  Schedule.swift
//  ThuY-Project
//
//  Created by Tuan on 28/04/2022.
//

import Foundation
import UIKit
import iOSDropDown

protocol BookingOverviewDelegate: NSObject {
    
    func addingSchedule()
    
    func searchData(data: String)
    
    func dateDidChange(date: String, time: String)
}

class BookingOverview: UIView {

    // MARK: - Outlet

    @IBOutlet weak var addingButton: UIButton!
    
    @IBOutlet weak var dateField: UIDatePicker!
    
    @IBOutlet weak var searchSchedule: UISearchBar!
    @IBOutlet weak var tableSchedule: UITableView!
    @IBOutlet weak var add: UIButton!
    weak var delegate: BookingOverviewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
    }
    
    
}

// MARK: - Function
extension BookingOverview: UISearchBarDelegate {

    func setupView() {
        dateField.minuteInterval = 15
        
        searchSchedule.delegate = self

        add.setTitle("", for: .normal)
    }

    
    @IBAction func addingSchedule() {
        self.delegate?.addingSchedule()

    }
    
    @IBAction func dateDidChange() {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let date = formatter.string(from: dateField.date)
        formatter.dateFormat = "HH:mm"
        let time = formatter.string(from: dateField.date)

        self.delegate?.dateDidChange(date: date, time: time)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let data = searchBar.text else { return }
        
        self.delegate?.searchData(data: data)
    }
    
}
