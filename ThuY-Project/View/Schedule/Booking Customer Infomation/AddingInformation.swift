//
//  AddingInfomation.swift
//  ThuY-Project
//
//  Created by Tuan on 23/05/2022.
//

import Foundation
import UIKit

protocol AddingInfomationDelegate: NSObject {
    func searchBarEnter(data: String)
}

class AddingInformation: UIView, UISearchBarDelegate {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableData: UITableView!
    
    weak var delegate: AddingInfomationDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        
        searchBar.delegate = self
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        guard let data = searchBar.text else { return }
        
        self.delegate?.searchBarEnter(data: data)
    }
    
}
