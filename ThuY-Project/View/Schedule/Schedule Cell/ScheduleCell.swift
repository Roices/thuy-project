//
//  ScheduleCell.swift
//  ThuY-Project
//
//  Created by Tuan on 28/04/2022.
//
//
import UIKit

class ScheduleCell: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var titleSchedule: UILabel!
    @IBOutlet weak var timeBooking: UILabel!
    @IBOutlet weak var backGround: UIView!
    
    @IBOutlet weak var layoutCustomerName: NSLayoutConstraint!
    @IBOutlet weak var layoutDocterName: NSLayoutConstraint!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
           setupView()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: - Method
    private func setupView() {
        // customerName label
        customerName.text = "Nguyễn Anh Tuấn"
        customerName.layer.masksToBounds = true
        customerName.layer.cornerRadius = 5
        
        // doctorName label
        doctorName.layer.masksToBounds = true
        doctorName.layer.cornerRadius = 5
        
        // background
        backGround.layer.cornerRadius = 10
    }
    
}

