//
//  CustomerBookingDoctor.swift
//  ThuY-Project
//
//  Created by Tuan on 30/04/2022.
//


import UIKit
import iOSDropDown

protocol BookingDoctorDelegate: NSObject {
    func bookingSchedule()
}

class BookingDoctor: UIView {
    
    // MARK: - properties
    @IBOutlet weak var branchName: DropDown!
    @IBOutlet weak var doctorName: UITextField!
    @IBOutlet weak var specialist: UITextField!
    @IBOutlet weak var experience: UITextView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var bookingBtt: UIButton!
    @IBOutlet weak var dateBooking: UITextField!
    @IBOutlet weak var startTime: UITextField!
    @IBOutlet weak var endTime: UITextField!
    weak var delegate: BookingDoctorDelegate?
    var tfIsEmpty = true
    
    let datePicker = UIDatePicker()
    let startTimePicker = UIDatePicker()
    let endTimePicker = UIDatePicker()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupView()
        keyboardNotification()
        
    }
    
    func setupView() {
        branchName.delegate = self
        doctorName.delegate = self
        specialist.delegate = self
        experience.delegate = self
        startTime.delegate = self
        endTime.delegate = self
        dateBooking.delegate = self
        //specialist.layer.borderWidth = 0.7
       // specialist.layer.cornerRadius = 5
    

        experience.text = "Kinh nghiệm bác sĩ"
        experience.textColor = .lightGray
        experience.layer.borderWidth = 0.1
        experience.layer.cornerRadius = 5
        
        bookingBtt.layer.cornerRadius = 5
        bookingBtt.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        
        creatDatePicker()
        creatEndTimePicker()
        creatStartTimePicker()
        
    
    }
    
    @IBAction func bookingSchedule() {
        branchName.text?.isEmpty == true ? showError(textField: branchName):(tfIsEmpty = false)
        doctorName.text?.isEmpty == true ? showError(textField: doctorName):(tfIsEmpty = false)
        specialist.text?.isEmpty == true ? showError(textField: specialist):(tfIsEmpty = false)
        startTime.text?.isEmpty == true ? showError(textField: startTime):(tfIsEmpty = false)
        endTime.text?.isEmpty == true ? showError(textField: endTime):(tfIsEmpty = false)
        dateBooking.text?.isEmpty == true ? showError(textField: dateBooking):(tfIsEmpty = false)
        
        if tfIsEmpty == false {
            self.delegate?.bookingSchedule()
        }
    }
    
    
    func showError(textField: UITextField){
        if textField == branchName {
            textField.layer.borderColor = UIColor.red.cgColor
            textField.layer.borderWidth = 0.15
        }else{
        textField.withImage(direction: .Right, image: UIImage(named: "ic_Error")!)
        }
    }
}



// MARK: - Config Keyboard

extension BookingDoctor: UITextViewDelegate, UITextFieldDelegate {
    
    // Config Keyboard for Experience TextView ---------------------
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if text == "\n" {
                textView.resignFirstResponder()
        }
            return true
    }
    
    // Config PlaceHolder for Note TextView ----------------------
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Kinh nghiệm bác sĩ" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Kinh nghiệm bác sĩ"
            textView.textColor = UIColor.lightGray
        }
    }
    
    // Config for Textfields -----------------------------------------
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // Keyboard notification
    func keyboardNotification() {
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // increase height of scrollView when tap to textfield
    @objc func keyboardWillShow(notification: NSNotification) {
            guard let userInfo = notification.userInfo else { return }
            let keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue

            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height + 20
            scrollView.contentInset = contentInset
          
        }
    // decrease height of scrollView when tap to textfield
    @objc func keyboardWillHide(notification: NSNotification) {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            scrollView.contentInset = contentInset

        }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.withImage(direction: .Nil, image: nil)
    }
        
}


//
// MARK: - Config DatePicker --------
//
extension BookingDoctor {
    // MARK: - Time Booking

    // Creat datepicker for Date TextField ------------------------
    func creatDatePicker() {
        let toolbar = UIToolbar(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: scrollView.bounds.width, height: CGFloat(44))))
        toolbar.sizeToFit()

        let dontBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
        toolbar.setItems([dontBtn], animated: true)

        dateBooking.inputAccessoryView = toolbar
        dateBooking.inputView = datePicker

        datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = .wheels
        datePicker.locale = Locale(identifier: "Vietnamese")

    }

    // Done Button when complete Date ---------
    @objc func donePressed() {
        // formatter
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        // get date
        dateBooking.text = formatter.string(from: datePicker.date)

    }

    // MARK: - Start Time Booking
    // Creat datepicker for Start Time TextField ------------------------
    func creatStartTimePicker() {
        let toolbar = UIToolbar(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: scrollView.bounds.width, height: CGFloat(44))))
        toolbar.sizeToFit()

        let dontBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedForStartTime))
        toolbar.setItems([dontBtn], animated: true)

        startTime.inputAccessoryView = toolbar
        startTime.inputView = startTimePicker
        startTimePicker.datePickerMode = .time
        startTimePicker.minuteInterval = 15
        startTimePicker.preferredDatePickerStyle = .wheels
        startTimePicker.locale = Locale(identifier: "Vietnamese")


    }
    // Done Button when complete start time ---------
    @objc func donePressedForStartTime() {
        // formatter
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        // get date
        startTime.text = formatter.string(from: startTimePicker.date)

    }

    // MARK: - End Time Booking
    // Creat datepicker for End Time TextField ------------------------
    func creatEndTimePicker() {
        let toolbar = UIToolbar(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: scrollView.bounds.width, height: CGFloat(44))))
        toolbar.sizeToFit()

        let dontBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedForEndTime))
        toolbar.setItems([dontBtn], animated: true)

        endTime.inputAccessoryView = toolbar
        endTime.inputView = endTimePicker
        endTimePicker.datePickerMode = .time
        endTimePicker.minuteInterval = 15
        endTimePicker.minimumDate = startTimePicker.date
        endTimePicker.preferredDatePickerStyle = .wheels
        endTimePicker.locale = Locale(identifier: "Vietnamese")
      //


    }
    // Done Button when complete start time ---------
    @objc func donePressedForEndTime() {
        // formatter
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        // get date
        endTime.text = formatter.string(from: endTimePicker.date)

    }
}
