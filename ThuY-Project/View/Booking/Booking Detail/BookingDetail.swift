//
//  ScheduleDetail.swift
//  ThuY-Project
//
//  Created by Tuan on 02/05/2022.
//

import Foundation
import UIKit

protocol BookingDetailDelegate: NSObject {
    func cancelSchedule()
    func editSchedule()
}

class BookingDetail: UIView {
    
    @IBOutlet weak var doctorName: UILabel!
    @IBOutlet weak var doctorSpecialist: UILabel!
    @IBOutlet weak var doctorExperience: UILabel!
    
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var customer: UILabel!
    @IBOutlet weak var petName: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var symptom: UILabel!
    @IBOutlet weak var note: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var stackPetAndType: UIStackView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    weak var delegate: BookingDetailDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupView()
    }
    
    
    private func setupView() {
        guard let role = UserDefaults.standard.string(forKey: "role") else { return }
        if role == "DOC" {
            cancelButton.backgroundColor = .gray
            cancelButton.isEnabled = false
            editButton.setTitle("Nhận lịch", for: .normal)
        }else {
        
        }
    }
    
    @IBAction func cancelSchedule() {
        guard let delegate = delegate else { return }
        delegate.cancelSchedule()
    }
    
    @IBAction func editSchedule() {
        guard let delegate = delegate else { return }
        delegate.editSchedule()
    }
}
