//
//  CustomerBooking.swift
//  ThuY-Project
//
//  Created by Tuan on 29/04/2022.
//

import Foundation
import UIKit
import iOSDropDown

protocol CustomerInfomationDelegate: NSObject {
    func register(doctorID: String, petID: String, date: String, time: String, status: String, symptom: String, note: String)
    func didDocIDtfTapped()
    func didPetIDtfTapped()
}


class CustomerInfomation: UIView, UIPickerViewDelegate, UIPickerViewDataSource {
    
  // MARK: - Outlet

    @IBOutlet weak var doctorID: UITextField!
    @IBOutlet weak var petID: UITextField!
    @IBOutlet weak var date: UITextField!
    @IBOutlet weak var time: UITextField!
    @IBOutlet weak var status: UITextField!
    @IBOutlet weak var symptom: UITextField!
    @IBOutlet weak var note: UITextView!
    @IBOutlet weak var coutinueButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!

    
    let datePicker = UIDatePicker()
    let startTimePicker = UIDatePicker()
    let statusPicker = UIPickerView()
    
    let statusValue:[String] = ["Đã hủy","Đang tiến hành","Đã nhận"]
    
    weak var delegate: CustomerInfomationDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
            setupView()
            keyboardNotification()
        
    }
    
    var tfIsEmpty = true
  // MARK: - Method
    @IBAction func nextStepRegister() {
        //self.delegate?.nextStep()
        doctorID.text?.isEmpty == true ? showError(textField: doctorID):(tfIsEmpty = false)
        petID.text?.isEmpty == true ? showError(textField: petID):(tfIsEmpty = false)
        status.text?.isEmpty == true ? showError(textField: status):(tfIsEmpty = false)
        date.text?.isEmpty == true ? showError(textField: date):(tfIsEmpty = false)
        time.text?.isEmpty == true ? showError(textField: time):(tfIsEmpty = false)
        symptom.text?.isEmpty == true ? showError(textField: symptom):(tfIsEmpty = false)
        
        if tfIsEmpty == false {
            var noteByUser = ""
            
            if note.text == "Ghi chú" {
                noteByUser = "Không có"
            } else {
                noteByUser = note.text
            }
            
            let status = statusPicker.selectedRow(inComponent: 0)
            
            self.delegate?.register(doctorID: doctorID.text!, petID: petID.text!, date: date.text!, time: time.text!, status: "\(status + 1)", symptom: symptom.text!, note: noteByUser)
         
        }
    }
    
    func showError(textField: UITextField){

        textField.withImage(direction: .Right, image: UIImage(named: "warning")!)

    }
    
    func setupView() {
        // Config Atribute for note textview ---------------
        note.layer.borderWidth = 0.1
        note.layer.cornerRadius = 5
        note.text = "Ghi chú"
        note.textColor = .lightGray
        note.delegate = self
        
        // Config delegate for textfields ------------------
        doctorID.delegate = self
        petID.delegate = self
        date.delegate = self
        time.delegate = self
        symptom.delegate = self
        status.delegate = self
        
        // Config for button ---------------
        coutinueButton.layer.cornerRadius = 5
        coutinueButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17)
        
        creatDatePicker()
        creatStartTimePicker()
        creatStatusPicker()
        
    }
    
    
    
}

// MARK: - Config Keyboard ------------------------------------
extension CustomerInfomation: UITextViewDelegate, UITextFieldDelegate {
    // Config Keyboard for Note TextView ---------------------
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            if text == "\n" {
                textView.resignFirstResponder()
        }
            return true
    }
    // Config PlaceHolder for Note TextView ----------------------
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Ghi chú" {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Ghi chú"
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    // Config for Textfields -----------------------------------------
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true;
    }
    
    // Keyboard notification
    func keyboardNotification() {
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // increase height of scrollView when tap to textfield
    @objc func keyboardWillShow(notification: NSNotification) {
            guard let userInfo = notification.userInfo else { return }
            let keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue

            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height + 20
            scrollView.contentInset = contentInset
          
        }
    // decrease height of scrollView when tap to textfield
    @objc func keyboardWillHide(notification: NSNotification) {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            scrollView.contentInset = contentInset

        }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
            textField.withImage(direction: .Nil, image: nil)
        
    }
    
}


// MARK: - Config Datepicker
extension CustomerInfomation {
        func creatDatePicker() {
            let toolbar = UIToolbar(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: scrollView.bounds.width, height: CGFloat(44))))
            toolbar.sizeToFit()

            let dontBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressed))
            toolbar.setItems([dontBtn], animated: true)

            date.inputAccessoryView = toolbar
            date.inputView = datePicker

            datePicker.datePickerMode = .date
            datePicker.preferredDatePickerStyle = .wheels
            datePicker.locale = Locale(identifier: "Vietnamese")
            datePicker.minimumDate = Date()

        }

        // Done Button when complete Date ---------
        @objc func donePressed() {
            // formatter
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            // get date
            date.text = formatter.string(from: datePicker.date)
            date.resignFirstResponder()

        }

    func creatStatusPicker() {
        let toolbar = UIToolbar(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: scrollView.bounds.width, height: CGFloat(44))))
        toolbar.sizeToFit()
        
        statusPicker.delegate = self
        statusPicker.dataSource = self
        
        let dontBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedStatus))
        toolbar.setItems([dontBtn], animated: true)

        status.inputAccessoryView = toolbar
        status.inputView = statusPicker
    
    }

    // Done Button when complete Status ---------
    @objc func donePressedStatus() {
        status.text = statusValue[statusPicker.selectedRow(inComponent: 0)]
        status.resignFirstResponder()
        

    }
    
        // MARK: - Start Time Booking
        // Creat datepicker for Start Time TextField ------------------------
        func creatStartTimePicker() {
            let toolbar = UIToolbar(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: scrollView.bounds.width, height: CGFloat(44))))
            toolbar.sizeToFit()
            

            let dontBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedForStartTime))
            toolbar.setItems([dontBtn], animated: true)

            time.inputAccessoryView = toolbar
            time.inputView = startTimePicker
            startTimePicker.datePickerMode = .time
            startTimePicker.minuteInterval = 30
            startTimePicker.preferredDatePickerStyle = .wheels
            startTimePicker.locale = Locale(identifier: "Vietnamese")
            startTimePicker.minimumDate = Calendar.current.date(bySettingHour: 8,
                                                                minute: 0,
                                                                second: 0,
                                                                of: Date())
            startTimePicker.maximumDate = Calendar.current.date(bySettingHour: 17, minute: 0, second: 0, of: Date())
            


        }
        // Done Button when complete start time ---------
        @objc func donePressedForStartTime() {
            // formatter
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            // get date
            time.text = formatter.string(from: startTimePicker.date)
            time.resignFirstResponder()

        }
    @IBAction func docIdTfIsTapped() {
        doctorID.withImage(direction: .Nil, image: nil)
        self.delegate?.didDocIDtfTapped()
    }
    
    @IBAction func petIDIsTapped() {
        petID.withImage(direction: .Nil, image: nil)
        self.delegate?.didPetIDtfTapped()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return statusValue[row]
    }
    
}



