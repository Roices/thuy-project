//
//  ResetPassword.swift
//  ThuY-Project
//
//  Created by Tuan on 19/05/2022.
//

import Foundation
import UIKit

protocol ForgotPasswordDelegate: NSObject {
    func getPasswordAction(staffID: String, newPassword: String, email: String)
}

class ForgotPassword: UIView {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var staffID: UITextField!
    
    @IBOutlet weak var newPassword: UITextField!
    
    @IBOutlet weak var email: UITextField!
    
    weak var delegate: ForgotPasswordDelegate?
    
    var tfIsEmpty = true
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        staffID.delegate = self
        newPassword.delegate = self
        email.delegate = self
    }
    
    @IBAction func tappedToForgotBtn() {
        
        staffID.text?.isEmpty == true ? showError(textField: staffID):(tfIsEmpty = false)
        newPassword.text?.isEmpty == true ? showError(textField: newPassword):(tfIsEmpty = false)
        email.text?.isEmpty == true ? showError(textField: email):(tfIsEmpty = false)

        if tfIsEmpty == false {
            self.delegate?.getPasswordAction(staffID: staffID.text!, newPassword: newPassword.text!, email: email.text!)
        }
        
    }
    
    func showError(textField: UITextField){

        textField.withImage(direction: .Right, image: UIImage(named: "warning")!)

    }
    
}


extension ForgotPassword: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Config Keyboard

    // Keyboard notification ---------------------------
    func keyboardNotification() {
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
    }

    // increase height of scrollView when tap to textfield ------------------
    @objc func keyboardWillShow(notification: NSNotification) {
            guard let userInfo = notification.userInfo else { return }
            let keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue

            var contentInset:UIEdgeInsets = scrollView.contentInset
            contentInset.bottom = keyboardFrame.size.height/3
            scrollView.contentInset = contentInset

        }
    // decrease height of scrollView when tap to textfield --------------------
    @objc func keyboardWillHide(notification: NSNotification) {
            let contentInset:UIEdgeInsets = UIEdgeInsets.zero
            scrollView.contentInset = contentInset

        }
    func textFieldDidBeginEditing(_ textField: UITextField) {
            textField.withImage(direction: .Nil, image: nil)
        
    }
    
    
}
