//
//  StaffCell.swift
//  ThuY-Project
//
//  Created by Viettelimex on 29/04/2022.
//

import Foundation
import UIKit

class StaffCell: UITableViewCell{
    
    
    @IBOutlet weak var Avatar: UIImageView!
    @IBOutlet weak var UserName: UILabel!
    @IBOutlet weak var ID: UIImageView!
    @IBOutlet weak var Role: UILabel!
    @IBOutlet weak var IDLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    	
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
