//
//  Staff.swift
//  ThuY-Project
//
//  Created by Viettelimex on 28/04/2022.
//

import Foundation
import UIKit

protocol StaffDelegate: NSObject{
    
}

enum staffButton{
    case staffView
    case docView
}
class Staff: UIView, UISearchBarDelegate{

    var root = StaffViewController()

    @IBOutlet weak var StaffTableView: UITableView!
    @IBOutlet weak var backGround: UIView!
    @IBOutlet weak var btnStaff: UIButton!
    @IBOutlet weak var btnDoc: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    weak var delegate: StaffDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()

        
        searchBar.delegate = self


    }
    
    

    @IBAction func btnAddStaff(_ sender: UIButton) {
        
    }
    
    func setOptionSelection(_ isbtnStaffSelection: Bool){
        if isbtnStaffSelection{
            self.btnStaff.isSelected = true
            self.btnDoc.isSelected = false
            
        }else{
            self.btnDoc.isSelected = true
            self.btnStaff.isSelected = false

            
        }
    }
    
    @IBAction func btnStaffView(_ sender: Any) {

        setOptionSelection(true)
        StaffViewController.type = .staffView
        StaffTableView.reloadData()
    }
    @IBAction func btnDocView(_ sender: Any) {

        setOptionSelection(false)
        StaffViewController.type = .docView
        StaffTableView.reloadData()
    }
    
}



