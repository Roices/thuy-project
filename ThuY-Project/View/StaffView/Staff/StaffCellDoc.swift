//
//  StaffCellDoc.swift
//  ThuY-Project
//
//  Created by Viettelimex on 06/05/2022.
//

import Foundation
import UIKit

class StaffCellDoc: UITableViewCell{
    
    @IBOutlet weak var Avatar: UIImageView!
    @IBOutlet weak var UserName: UILabel!
    @IBOutlet weak var ID: UIImageView!
    @IBOutlet weak var Role: UILabel!
    @IBOutlet weak var IDLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
