//
//  InfoDocStaff.swift
//  ThuY-Project
//
//  Created by Viettelimex on 11/05/2022.
//

import Foundation
import UIKit

class InfoDocStaff: UIView {

    var root = InfoDocViewController()

    @IBOutlet weak var Avatar: UIImageView!
    @IBOutlet weak var userNameDoc: UILabel!
    @IBOutlet weak var idDoc: UILabel!
    @IBOutlet weak var sexDoc: UILabel!
    @IBOutlet weak var identifyCardDoc: UILabel!
    @IBOutlet weak var birthDoc: UILabel!
    @IBOutlet weak var phoneNumberDoc: UILabel!
    @IBOutlet weak var roleDoc: UILabel!
    @IBOutlet weak var addressDoc: UILabel!
    @IBOutlet weak var emailDoc: UILabel!
    @IBOutlet weak var specializationDoc: UILabel!
    @IBOutlet weak var experienceDoc: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }



}
