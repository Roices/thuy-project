//
//  InfoStaffViewController.swift
//  ThuY-Project
//
//  Created by Viettelimex on 04/05/2022.
//
import Foundation
import UIKit

protocol InfoStaffDelegate: NSObject{
    func deleteStaff(staffID: String)
    
    
}

class InfoStaff: UIView {

    var root = InfoStaffController()
    
    @IBOutlet weak var Avatar: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var idStaff: UILabel!
    @IBOutlet weak var sexStaff: UILabel!
    @IBOutlet weak var birthStaff: UILabel!
    @IBOutlet weak var identifyCardStaff: UILabel!
    @IBOutlet weak var phoneNumberStaff: UILabel!
    @IBOutlet weak var roleStaff: UILabel!
    @IBOutlet weak var addressStaff: UILabel!
    @IBOutlet weak var emailStaff: UILabel!
    @IBOutlet weak var specializationStaff: UILabel!
    @IBOutlet weak var experienceStaff: UILabel!
    weak var delegate : InfoStaffDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
    
    @IBAction func deleteStaff() {
        
        self.delegate?.deleteStaff(staffID: idStaff.text!)

        
    }
  
}

extension InfoStaffController{
    @IBAction func editStaff(_ sender: UIButton){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "EditStaffViewController") as! EditStaffViewController
                
        vc.idStaff = infoStaffView.idStaff.text
        vc.idStaff = infoStaffView.sexStaff.text
        vc.idStaff = infoStaffView.birthStaff.text
        vc.idStaff = infoStaffView.identifyCardStaff.text
        vc.idStaff = infoStaffView.phoneNumberStaff.text
        vc.idStaff = infoStaffView.addressStaff.text
        vc.idStaff = infoStaffView.emailStaff.text
        vc.idStaff = infoStaffView.experienceStaff.text
        vc.idStaff = infoStaffView.specializationStaff.text
        
        let info = Info(id: infoStaffView.idStaff.text!, sex: infoStaffView.sexStaff.text!, date: infoStaffView.birthStaff.text!, identify: infoStaffView.identifyCardStaff.text!, phoneNumber: infoStaffView.phoneNumberStaff.text!, address: infoStaffView.addressStaff.text!, email: infoStaffView.emailStaff.text!, experience: infoStaffView.experienceStaff.text!, specialization: infoStaffView.specializationStaff.text!)
        
        vc.pushData = info
        
        self.navigationController?.pushViewController(vc, animated: true)

        navigationItem.backButtonTitle = ""
    }
}
