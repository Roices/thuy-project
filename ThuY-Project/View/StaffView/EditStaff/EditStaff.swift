//
//  EditStaff.swift
//  ThuY-Project
//
//  Created by Viettelimex on 05/05/2022.
//

import Foundation
import UIKit
import iOSDropDown

protocol EditDelegate: NSObject{
    func editStaff(userNameEdit: String, sexEdit: String,birthEdit: String, identifyEdit: String, emailEdit: String, noteEdit: String, experienceEdit: String, specializationEdit: String, addressEdit: String, phoneNumberEdit: String)
}

class EditStaff: UIView, UITextFieldDelegate{
    
    let dropdown = DropDown()
    
    
    @IBOutlet weak var userNameEdit: UITextField!
    @IBOutlet weak var sexEdit: DropDown!
    @IBOutlet weak var birthEdit: UITextField!
    @IBOutlet weak var identifyEdit: UITextField!
    @IBOutlet weak var emailEdit: UITextField!
    @IBOutlet weak var noteEdit: UITextField!
    @IBOutlet weak var experienceEdit: UITextField!
    @IBOutlet weak var specializationEdit: UITextField!
    @IBOutlet weak var addressEdit: UITextField!
    @IBOutlet weak var phoneNumberEdit: UITextField!
    @IBOutlet weak var Staff: UIButton!
    @IBOutlet weak var Doctor: UIButton!
   
    var pushData: Info?
    weak var delegate: EditDelegate?
    
    var root = EditStaffViewController()
    
    override func awakeFromNib() {
        super.awakeFromNib()
            
        setup()
        
        setupTextField()
        
        
        
    }
    
    func setOptionSelection(_ isbtnStaffSelection: Bool){
        if isbtnStaffSelection{
            self.Staff.isSelected = true
            self.Doctor.isSelected = false
        }else{
            self.Doctor.isSelected = true
            self.Staff.isSelected = false

            
        }
    }
    @IBAction func btnStaff(_ sender: Any) {
        setOptionSelection(true)
    }
    @IBAction func btnDoc(_ sender: Any) {
        setOptionSelection(false)
         
    }
    

    
    var valid = true
    
    func setupTextField(){
        
        userNameEdit.text = ""
        sexEdit.text = ""
        birthEdit.text = ""
        identifyEdit.text = ""
        emailEdit.text = ""
        noteEdit.text = ""
        experienceEdit.text = ""
        specializationEdit.text = ""
        addressEdit.text = ""
        phoneNumberEdit.text = pushData?.phoneNumber
    }

    func setup(){
        
        userNameEdit.delegate = self
        sexEdit.delegate = self
        birthEdit.delegate = self
        identifyEdit.delegate = self
        phoneNumberEdit.delegate = self
        addressEdit.delegate = self
        emailEdit.delegate = self
        specializationEdit.delegate = self
        experienceEdit.delegate = self
        
    }
    
    @IBAction func btnEdit(_ sender: Any) {
        
        userNameEdit.text?.isEmpty == true ? showError(textField: userNameEdit):(valid = false)
            sexEdit.text?.isEmpty == true ? showError(textField: sexEdit):(valid = false)
            birthEdit.text?.isEmpty == true ? showError(textField: birthEdit):(valid = false)
            identifyEdit.text?.isEmpty == true ? showError(textField: identifyEdit):(valid = false)
            phoneNumberEdit.text?.isEmpty == true ? showError(textField: phoneNumberEdit):(valid = false)
            addressEdit.text?.isEmpty == true ? showError(textField: addressEdit):(valid = false)
            emailEdit.text?.isEmpty == true ? showError(textField: emailEdit):(valid = false)
            specializationEdit.text?.isEmpty == true ? showError(textField: specializationEdit):(valid = false)
            experienceEdit.text?.isEmpty == true ? showError(textField: experienceEdit):(valid = false)
        
        if valid == false{
            
            self.delegate?.editStaff(userNameEdit: userNameEdit.text!, sexEdit: sexEdit.text!, birthEdit: birthEdit.text!, identifyEdit: identifyEdit.text!, emailEdit: emailEdit.text!, noteEdit: noteEdit.text!, experienceEdit: experienceEdit.text!, specializationEdit: specializationEdit.text!, addressEdit: addressEdit.text!, phoneNumberEdit: phoneNumberEdit.text!)
        
        
        }
    }

    
    
    @IBAction func btnCancel(_ sender: Any) {
    }


    func showError(textField: UITextField){
    if textField == noteEdit {
        textField.setWhenTfIsEmpty()
    } else {
        
    textField.withImage(direction: .Right, image: UIImage(named: "warning")!)
        
    }
    
}

}
