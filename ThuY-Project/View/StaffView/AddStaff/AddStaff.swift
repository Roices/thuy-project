//
//  AddStaff.swift
//  ThuY-Project
//
//  Created by Viettelimex on 04/05/2022.
//

import Foundation
import UIKit
import SwiftUI
import iOSDropDown

protocol AddStaffDelegate: NSObject{
    func AddStaff(userNameAdd: String, sexAdd:String,birthAdd: String, identifyAdd: String, emailAdd: String, noteAdd: String, experienceAdd: String, specializationAdd: String, addressAdd: String, phoneNumberAdd: String)
}

class AddStaff: UIView, UITextFieldDelegate{
    
    let dropdown = DropDown()

    
    @IBOutlet weak var userNameAdd: UITextField!
    @IBOutlet weak var idAdd: UITextField!
    @IBOutlet weak var sexAdd: DropDown!
    @IBOutlet weak var birthAdd: UITextField!
    @IBOutlet weak var identifyAdd: UITextField!
    @IBOutlet weak var phoneNumberAdd: UITextField!
    @IBOutlet weak var addressAdd: UITextField!
    @IBOutlet weak var emailAdd: UITextField!
    @IBOutlet weak var specializationAdd: UITextField!
    @IBOutlet weak var experienceAdd: UITextField!
    @IBOutlet weak var noteAdd: UITextField!
    
    @IBOutlet weak var Staff: UIButton!
    @IBOutlet weak var Doctor: UIButton!
    
    @IBOutlet weak var btnAddStaff: UIButton!
    
    var root = AddStaffViewController()
    
    weak var delegate : AddStaffDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()

        setup()
        
        sexAdd.optionArray = ["1", "2"]
        sexAdd.optionIds = [1,2]
        sexAdd.didSelect{(selectedText , index ,id) in
        self.sexAdd.text = selectedText
    }
    }
    func setOptionSelection(_ isbtnStaffSelection: Bool){
        if isbtnStaffSelection{
            self.Staff.isSelected = true
            self.Doctor.isSelected = false
        }else{
            self.Doctor.isSelected = true
            self.Staff.isSelected = false

            
        }
    }
    @IBAction func btnStaff(_ sender: Any) {
        setOptionSelection(true)
 
    }
    @IBAction func btnDoc(_ sender: Any) {
        setOptionSelection(false)
         
    }
    
    
    var valid = true
        // MARK: Method
    @IBAction func btnAdd(_ sender: UIButton) {
    
        userNameAdd.text?.isEmpty == true ? showError(textField: userNameAdd):(valid = false)
            sexAdd.text?.isEmpty == true ? showError(textField: sexAdd):(valid = false)
            birthAdd.text?.isEmpty == true ? showError(textField: birthAdd):(valid = false)
            identifyAdd.text?.isEmpty == true ? showError(textField: identifyAdd):(valid = false)
            phoneNumberAdd.text?.isEmpty == true ? showError(textField: phoneNumberAdd):(valid = false)
            addressAdd.text?.isEmpty == true ? showError(textField: addressAdd):(valid = false)
            emailAdd.text?.isEmpty == true ? showError(textField: emailAdd):(valid = false)
            specializationAdd.text?.isEmpty == true ? showError(textField: specializationAdd):(valid = false)
            experienceAdd.text?.isEmpty == true ? showError(textField: experienceAdd):(valid = false)
        
        if valid == false{
            
            self.delegate?.AddStaff(userNameAdd: userNameAdd.text!, sexAdd: sexAdd.text!, birthAdd: birthAdd.text!, identifyAdd: identifyAdd.text!, emailAdd: emailAdd.text!, noteAdd: noteAdd.text!, experienceAdd: experienceAdd.text!, specializationAdd: specializationAdd.text!, addressAdd: addressAdd.text!, phoneNumberAdd: phoneNumberAdd.text!)
        
        
        }
    }
    
    func setup(){
        
        userNameAdd.delegate = self
        sexAdd.delegate = self
        birthAdd.delegate = self
        identifyAdd.delegate = self
        phoneNumberAdd.delegate = self
        addressAdd.delegate = self
        emailAdd.delegate = self
        specializationAdd.delegate = self
        experienceAdd.delegate = self
        
        
    }
    
    func showError(textField: UITextField){
        if textField == idAdd {
            textField.setWhenTfIsEmpty()
        } else {
            
        textField.withImage(direction: .Right, image: UIImage(named: "warning")!)
            
        }
        
    }
}

