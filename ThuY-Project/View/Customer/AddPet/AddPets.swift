//
//  AddPets.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 18/05/2022.
//

import UIKit
import iOSDropDown

class AddPets: UIView {
    @IBOutlet weak var btnAddImg: UIButton!
    @IBOutlet weak var imgBirthCerti: UIImageView!
    @IBOutlet weak var txtNamePets: UITextField!
    @IBOutlet weak var txtSex: DropDown!
    @IBOutlet weak var txtBreed: DropDown!
    @IBOutlet weak var txtNote: UITextField!
    @IBOutlet weak var btnAdd: UIButton!
    var root: UIViewController?
    var idCustomer: String!
    override func awakeFromNib() {
        super.awakeFromNib()
        imgBirthCerti.layer.cornerRadius = 20
        btnAddImg.layer.cornerRadius = btnAddImg.frame.height/2
        btnAdd.layer.cornerRadius = btnAdd.frame.height/2
        txtSex.optionArray = ["Đực", "Cái"]
        txtBreed.optionArray = SpeciesViewController.arrBreed
    }
    
    @IBAction func btnAddClick(_ sender: Any) {
        
    }
}
