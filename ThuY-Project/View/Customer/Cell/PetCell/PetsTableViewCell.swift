//
//  PetsTableViewCell.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 17/05/2022.
//

import UIKit

class PetsTableViewCell: UITableViewCell {

    @IBOutlet weak var lbCode: UILabel!
    @IBOutlet weak var viewAll: UIView!
    @IBOutlet weak var lbNamePet: UILabel!
    @IBOutlet weak var lbNameCustomer: UILabel!
    @IBOutlet weak var lbBreed: UILabel!
    @IBOutlet weak var viewCode: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewAll.layer.cornerRadius = 20
        viewCode.layer.cornerRadius = viewCode.frame.height/2
        
    }
    
    func setData(obj: PetDataModel){
        lbCode.text = obj.id
        lbNamePet.text = obj.name
        lbNameCustomer.text = obj.sex
        lbBreed.text = obj.species[0].name
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
