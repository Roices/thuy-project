//
//  PetCustomerTableViewCell.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 17/05/2022.
//

import UIKit

class PetCustomerTableViewCell: UITableViewCell {
    @IBOutlet weak var lbNamePet: UILabel!
    @IBOutlet weak var lbBreed: UILabel!
    @IBOutlet weak var viewAll: UIView!
    @IBOutlet weak var viewCode: UIView!
    @IBOutlet weak var lbCode: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewAll.layer.cornerRadius = 20
        viewCode.layer.cornerRadius = viewCode.frame.height/2
    }
    
    func setData(obj: PetDataModel){
        lbNamePet.text = obj.name
        if obj.species.count > 0 {
            lbBreed.text = obj.species[0].name
        }else{
            lbBreed.text = ""
        }
        
        lbCode.text = obj.id
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
