//
//  CustomerTableViewCell.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 03/05/2022.
//

import UIKit

class CustomerTableViewCell: UITableViewCell {
    @IBOutlet weak var viewCellCustomer: UIView!
    @IBOutlet weak var lbNameCustomer: UILabel!
    @IBOutlet weak var lbNamePet: UILabel!
    @IBOutlet weak var viewNumberPhone: UIView!
    @IBOutlet weak var lbNumberPhone: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
      
        viewCellCustomer.layer.cornerRadius = 20
        viewNumberPhone.layer.cornerRadius = viewNumberPhone.frame.height/2
    }
    
    func setData (obj: CustomerDataModel){
        lbNameCustomer.text = obj.name
        lbNumberPhone.text = obj.phoneNumber
        if obj.pets.count >= 1{
            var textLbPet : String = ""
            for i in obj.pets {
                if i.name == obj.pets[obj.pets.count - 1].name {
                    textLbPet += "\(i.name)"
                }else{
                    textLbPet += "\(i.name),"
                }
            }
            lbNamePet.text = textLbPet
        }else {
            lbNamePet.text = ""
        }
            
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
