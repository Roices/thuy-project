//
//  Customer.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 03/05/2022.
//

import UIKit

class CustomerView: UIView{
    @IBOutlet weak var btnSeach: UIButton!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnPet: UIButton!
    @IBOutlet weak var btnCustomer: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var tbView: UITableView!
    var root : UIViewController!
    var timer : Timer?
    override func awakeFromNib() {
        super.awakeFromNib()
        btnPet.layer.cornerRadius = btnPet.frame.height/2
        btnCustomer.layer.cornerRadius = btnCustomer.frame.height/2
        if CustomerViewController.roleCustomer == "DOC"{
            btnAdd.isHidden = true
        }else{
            if SpeciesViewController.type == .giong{
                btnAdd.isHidden = false
            }else{
                btnAdd.isHidden = true
            }
        }
        uiView()
    }

    @IBAction func btnSearchClick(_ sender: Any) {
    }
    
    @objc func timeImagerun(){
        tbView.reloadData()
    }
    
    @IBAction func btnAddClick(_ sender: Any) {
            HandlePush().gotoAddCustomerViewController(root: root)
    }
    
    func uiView(){
        if CustomerViewController.type == .customers{
            btnPet.layer.backgroundColor = UIColor.white.cgColor
            btnCustomer.layer.backgroundColor = UIColor.init(hexString:"70B08D").cgColor
            btnAdd.isHidden = false
        }else{
            btnCustomer.layer.backgroundColor = UIColor.white.cgColor
            btnPet.layer.backgroundColor = UIColor.init(hexString:"70B08D").cgColor
            btnAdd.isHidden = true
        }
    }
    
    @IBAction func btnCustomerClick(_ sender: Any) {
        btnPet.layer.backgroundColor = UIColor.white.cgColor
        btnCustomer.layer.backgroundColor = UIColor.init(hexString:"70B08D").cgColor
        CustomerViewController.type = .customers
        if CustomerViewController.roleCustomer == "DOC"{
            btnAdd.isHidden = true
        }else{
               btnAdd.isHidden = false
        }
        tbView.reloadData()
    }
    @IBAction func btnPetsClick(_ sender: Any) {
        btnCustomer.layer.backgroundColor = UIColor.white.cgColor
        btnPet.layer.backgroundColor = UIColor.init(hexString:"70B08D").cgColor
        CustomerViewController.type = .pets
        if CustomerViewController.roleCustomer == "DOC"{
            btnAdd.isHidden = true
        }else{
                btnAdd.isHidden = true
        }
        tbView.reloadData()
    }
}
