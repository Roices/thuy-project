//
//  AddCustomer.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 04/05/2022.
//

import UIKit
import iOSDropDown

class AddCustomer: UIView {

    @IBOutlet weak var txtBranch: DropDown!
    @IBOutlet weak var txtType: DropDown!
    @IBOutlet weak var txtNameCustomer: UITextField!
    @IBOutlet weak var txtNumberPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var btnAdd: UIButton!
    var root : UIViewController!

    override func awakeFromNib() {
        super.awakeFromNib()
        btnAdd.layer.cornerRadius = btnAdd.frame.height/2
        txtType.optionArray = ["normal","vip"]
        txtBranch.optionArray = CustomerViewController.arrBranch
    }
    
    @IBAction func btnAddClick(_ sender: Any) {
    }
}
