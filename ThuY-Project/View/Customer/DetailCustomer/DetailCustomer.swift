//
//  DetailCustomer.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 18/05/2022.
//

import UIKit

class DetailCustomer: UIView {
    @IBOutlet weak var btnUpdateCustomer: UIButton!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbCodeCustomer: UILabel!
    @IBOutlet weak var btnAddPet: UIButton!
    @IBOutlet weak var lbAddress: UILabel!
    @IBOutlet weak var lbPhoneNumber: UILabel!
    @IBOutlet weak var lbEmail: UILabel!
    @IBOutlet weak var lbTypeCustomer: UILabel!
    @IBOutlet weak var tbView: UITableView!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var lbBranch: UILabel!
    var root: UIViewController!
    var obj: CustomerDataModel?
    var idCustomer: String!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnUpdate.layer.cornerRadius = btnUpdate.frame.height/2
    }
    
    func setData(obj : CustomerDataModel){
        lbName.text = obj.name
        lbCodeCustomer.text = obj.id
        lbAddress.text = obj.address
        lbPhoneNumber.text = obj.phoneNumber
        lbEmail.text = obj.email
        lbTypeCustomer.text = obj.type
        lbBranch.text = obj.branches[0].name + " - " + obj.branches[0].id
    }
    
    @IBAction func btnAddClick(_ sender: Any) {
        HandlePush().gotoAddPetsViewController(root: root, id: self.idCustomer)
    }
    
    @IBAction func btnUpdateClick(_ sender: Any) {
        HandlePush().gotoUpdateCustomerViewController(root: root!, obj: obj!)
    }
}
