//
//  UpdatePets.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 18/05/2022.
//

import UIKit
import iOSDropDown
import SDWebImage

class UpdatePets: UIView {
    @IBOutlet weak var txtNamePets: UITextField!
    @IBOutlet weak var txtSex: DropDown!
    @IBOutlet weak var txtBreed: DropDown!
    @IBOutlet weak var txtNote: UITextField!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var imgBirthCerti: UIImageView!
    @IBOutlet weak var btnAddImg: UIButton!
    var root: UIViewController?
    var time : Timer?
    var id: String!
    var obj: PetDataModel!
    override func awakeFromNib() {
        super.awakeFromNib()
        imgBirthCerti.layer.cornerRadius = 20
        btnAddImg.layer.cornerRadius = btnAddImg.frame.height/2
        btnCancel.layer.cornerRadius = btnCancel.frame.height/2
        btnUpdate.layer.cornerRadius = btnUpdate.frame.height/2
        txtSex.optionArray = ["Đực", "Cái"]
        txtBreed.optionArray = SpeciesViewController.arrBreed
        
    }
    
    func setData(objData: PetDataModel){
        txtNamePets.text = objData.name
        txtSex.text = objData.sex
        if obj.species.count > 0 {
            txtBreed.text = (objData.species[0].name ?? "") + " - " + (objData.species[0].id ?? "")
        }else{
            txtBreed.text = ""
        }
        txtNote.text = objData.note
        imgBirthCerti.sd_setImage(with: URL(string: objData.birthCerti))
    }
    
    @IBAction func btnAddImgClicj(_ sender: Any) {
        
    }
    
    @IBAction func btnUpdateClick(_ sender: Any) {
        
    }
    
    @IBAction func btnCancelClick(_ sender: Any) {
        commomSevice().deleteApiPets(id: self.id)
        self.root?.navigationController?.popViewController(animated: true)
    }
}
