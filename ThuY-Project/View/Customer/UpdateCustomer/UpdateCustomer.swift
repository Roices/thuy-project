//
//  UpdateCustomer.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 03/05/2022.
//

import UIKit
import iOSDropDown
//
class UpdateCustomer: UIView {
    
    @IBOutlet weak var txtBranch: DropDown!
    @IBOutlet weak var txtType: DropDown!
    @IBOutlet weak var txtNameCustomer: UITextField!
    @IBOutlet weak var txtNumberPhone: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnUpdate: UIButton!
    var root : UIViewController!
    override func awakeFromNib() {
        super.awakeFromNib()
        btnUpdate.layer.cornerRadius = btnUpdate.frame.height/2
        btnCancel.layer.cornerRadius = btnCancel.frame.height/2
        txtType.optionArray = ["normal","vip"]
        txtBranch.optionArray = CustomerViewController.arrBranch
    }
    
    func setData(obj: CustomerDataModel){
        txtType.text = obj.type
        txtNameCustomer.text = obj.name
        txtNumberPhone.text = obj.phoneNumber
        txtEmail.text = obj.email
        txtAddress.text = obj.address
        txtBranch.text = obj.branches[0].name + " - " + obj.branches[0].id
    }
    
    @IBAction func btnUpdateClick(_ sender: Any) {
       
        
    }
    
    @IBAction func btnCancelClick(_ sender: Any) {
        
        
    }
}
