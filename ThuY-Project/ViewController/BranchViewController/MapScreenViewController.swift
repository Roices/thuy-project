//
//  MapScreenViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 09/05/2022.
//

import UIKit
import MapKit
import CoreLocation

class MapScreenViewController: UIViewController{
    // MARK: - Subview
    let  mapView: MKMapView = {
        let mapView = MKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.layer.cornerRadius = 5
        return mapView
    }()
    // MARK: - Properties
    let locationManager = CLLocationManager()
    
    let coordinate = CLLocationCoordinate2D(latitude: 40.728, longitude: -74)
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        layout()
//        checkLocationService()

        mapView.delegate = self
        centerMapOnUser(location: coordinate)
        addCustomPin()


    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
    
    private func addCustomPin(){
        let pin = MKPointAnnotation()
        pin.title = "Phong kham thu y"
        pin.coordinate = coordinate
        mapView.addAnnotation(pin)
    }
    func centerMapOnUser(location: CLLocationCoordinate2D){
        let region = MKCoordinateRegion(center: location, latitudinalMeters: 500, longitudinalMeters: 500)
        self.mapView.setRegion(region, animated: true)
//        self.mapView.showsUserLocation = true
    }
}
// MARK: - Extension
extension MapScreenViewController {
    
    func setup(){
        view.backgroundColor = .clear
 
    }
    func layout(){
        view.addSubview(mapView)
        
        NSLayoutConstraint.activate([
            mapView.topAnchor.constraint(equalTo: view.topAnchor),
            mapView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mapView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mapView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
    
    
}

extension MapScreenViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard !(annotation is MKUserLocation) else{
            return nil
        }
        let identifier = "Annotation"
        var annotationView: MKAnnotationView?
        annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView != nil {
            // create view
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView?.canShowCallout = true
        }else{
            annotationView?.annotation = annotation
        }

        
        annotationView?.image = Image.home
        return annotationView
    }
}
