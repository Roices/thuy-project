//
//  BranchViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 02/05/2022.
//

import UIKit

class BranchViewController: ViewControllerCustom {
    // MARK: - Subview
    let searchView = SearchView(placeholder: "Tên chi nhánh")
    
    var branches: [BranchData] = [BranchData](){
        didSet{
            self.branches = self.branches.sorted {$0.id!.getNumberFromID() > $1.id!.getNumberFromID()}
            tableView.reloadData()
        }
    }
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    lazy var addButton = AddButton(type: .custom)
    
    lazy var refreshControl = UIRefreshControl()
    
    var actionSheetBottom: ActionSheetBranch!
    
    var isSearching: Bool = false
    
    var isFound: Bool = true{
        didSet{
            displayNotFoundLabel()
        }
    }
    
    var filteredBranches: [BranchData] = [BranchData]()
    
    private let notFoundLabel = LabelCustom(color: .redTY, fontFamily: UIFont.preferredFont(forTextStyle: .headline), alignment: .center)
    
    // MARK: - Properties
    
    var didLongGesturesPressed: Bool = false{
        didSet{
            configNavBar()
        }
    }
    
    
    var hasMoreBranches: Bool = true
    //param gets API
    var page = 1
    var size = 10
    
    var cellSelection = [Int: Int]()
    
    var tapBarHeight: CGFloat = 0
    
    var isBranchInvalid: Bool = false
    
    var userInfor: LoginModel!
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        //        configBackButtonNavBar()
        layout()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Selector
    @objc func addButtonDidTapped(_ sender: UIButton){
        // move to AddNewBranchViewController()
        let vc = AddNewBranchViewController()
        vc.delegate = self
        let nc = UINavigationController(rootViewController: vc)
        self.present(nc, animated: true, completion: nil)
    }
    
    @objc func refreshTableView(_ sender: UIRefreshControl){
        //waiting for getBranch successful then end refreshing
        let group = DispatchGroup()
        group.enter()
        self.getBranches(page: 1, size: 999, didInView: true)
        group.leave()
        group.notify(queue: .main) {
            self.refreshControl.endRefreshing()
        }
    }
    
    @objc func didUpdatedBranch(){
        self.getBranches(page: 1, size: 999, didInView: false)
    }
    
    @objc func handleLongPress(_ sender: UILongPressGestureRecognizer){
        if (sender.state != .began) {
            return
        }
        let gestureIndex = sender.location(in: tableView)
        if let indexPath = tableView.indexPathForRow(at: gestureIndex) {
            let cell = tableView.cellForRow(at: indexPath) as! BranchTableCell
            cell.circleButton.sendActions(for: .touchUpInside)
            
        }
        actionSheetBottom.show()
        //long gesture mode on
        didLongGesturesPressed.toggle()
        
        NotificationCenter.default.post(name: .longCellPressed, object: nil)
    }
    
    @objc func didCancelButtonTapped(_ sender: UIBarButtonItem){
        backToDefaultBranchVC()
        
    }
    
    @objc func didDeleteButtonTapped(_ sender: UIBarButtonItem){
        
        // show pop up delete
        guard cellSelection.count > 0 else {return}
        let branchesDeleted = cellSelection.count
        presentAlertOnMainThread(content: "\(branchesDeleted) chi nhánh", selection: .delete) { [weak self] vc in
            guard let self = self else {return}
            vc.delegate = self
        }
    }
    
    @objc func selectAllBranchNotification(){
        let cells = getAllCells()
        for cell in cells {
            cell.circleButton.sendActions(for: .touchUpInside)
        }
    }
    
    @objc func unSelectAllBranchNotification(){
        for dict in cellSelection{
            let cell = tableView.cellForRow(at: IndexPath(row: dict.key, section: dict.value)) as! BranchTableCell
            cell.circleButton.sendActions(for: .touchUpInside)
        }
    }
    
    // MARK: - API
    private func deleteBulkBranches(){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else {return}
        guard cellSelection.count > 0 else {return}
        var branches = [BranchData]()
        branches = cellSelection.map({self.branches[$0.key]})
        let group = DispatchGroup()
        
        
        for branch in branches {
            DispatchQueue.main.async {
                group.enter()
                NetworkManager.shared.deleteBranch(token: token, branch: branch) { [weak self] result in
                    guard let self = self else {return}
                    group.leave()
                    switch result {
                    case .success(let status):
                        switch status {
                        case 200:
                            self.getBranches(page: 1, size: 100, didInView: true)
                        case 400:
                            self.isBranchInvalid = true
                            self.presentAlertNoOptionOnMainThread(content: "\(branch.name) tồn tại khách hàng hoặc nhân viên có liên quan đến chi nhánh. Không thể xoá!") { [weak self] vc in
                                guard let self = self else {return}
                                vc.delegate = self
                                
                            }
                        default:
                            break
                        }
                        
                    case .failure(_):
                        self.presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { [weak self] vc in
                            guard let self = self else {return}
                            vc.delegate = self
                        }
                    }
                }
            }
            
        }
        group.notify(queue: .main) {
            self.getBranches(page: 1, size: 10, didInView: false)
            //back to normal mode
            self.backToDefaultBranchVC()
            
        }
    }
    
    func searchBranch(name: String, completion: @escaping()->Void){
        
        guard let token = UserDefaults.standard.string(forKey: "access_token") else {return}
        showLoadingView()
        filteredBranches.removeAll()
        NetworkManager.shared.searchBranches(token: token,page: 1, size: 10, name: name) { [weak self] result in
            
            guard let self = self else {return}
            self.dismissLoadingView()
            switch result {
            case .success(let branch):
                self.isFound = branch.isEmpty ? false : true
                self.filteredBranches = branch
                completion()
            case .failure(_):
                break
            }
        }
    }
    
    
    
    
    // MARK: - Helper
    
    func backToDefaultBranchVC(){
        cancelAllCellSelected()
        self.didLongGesturesPressed = false
        NotificationCenter.default.post(name: .cancelLongPressed, object: nil)
        actionSheetBottom.dimissActionSheet()
    }
    
    private func registerSelectAllBranchNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(selectAllBranchNotification), name: .selectAllTapped, object: nil)
    }
    
    private func registerUnSelectAllBranchNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(unSelectAllBranchNotification), name: .unSelectAllTapped, object: nil)
    }
    
    func cancelAllCellSelected(){
        //reset all cell
        for dict in cellSelection {
            let indexPath = IndexPath(row: dict.key, section: dict.value)
            let cell = tableView.cellForRow(at: indexPath) as! BranchTableCell
            cell.circleButton.sendActions(for: .touchUpInside)
            
        }
        cellSelection.removeAll()
        
    }
    
    
    func getBranches(page: Int, size: Int, didInView: Bool){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        //didInView: decide display loadingview or not
        if !didInView {
            showLoadingView()
        }
        NetworkManager.shared.fetchBranches(token: token, page: page, size: size) { [weak self] result in
            guard let self = self else {return}
            if !didInView {
                self.dismissLoadingView()
            }
            switch result {
            case .success(let branches):
                self.branches = branches
            case .failure(_):
                self.presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { [weak self] vc in
                    guard let self = self else {return}
                    vc.delegate = self
                }
            }
        }
    }
    
    private func registerUpdatedBranchNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(didUpdatedBranch), name: .updated, object: nil)
    }
    
    private func setupLongGestureRecognizerOnTableView() {
        let longPressedGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(_:)))
        longPressedGesture.minimumPressDuration = 0.5
        longPressedGesture.delegate = self
        longPressedGesture.delaysTouchesBegan = true
        tableView.addGestureRecognizer(longPressedGesture)
    }
    
    private func configNavBar(){
        
        let backButton = NavCustomButton(image: Image.back)
        backButton.addTarget(self, action: #selector(didBackButtonTapped(_:)), for: .touchUpInside)
        let backButtonItem = UIBarButtonItem(customView: backButton)
        
        let cancelButtonItem = UIBarButtonItem(title: "Huỷ", style: .plain, target: self, action: #selector(didCancelButtonTapped(_:)))
        cancelButtonItem.tintColor = .greenTY
        
        let deleteButtonItem = UIBarButtonItem(title: "Xoá", style: .plain, target: self, action: #selector(didDeleteButtonTapped(_:)))
        deleteButtonItem.tintColor = .redTY
        
        self.navigationItem.leftBarButtonItem = didLongGesturesPressed ? cancelButtonItem : backButtonItem
        self.navigationItem.rightBarButtonItem = didLongGesturesPressed ? deleteButtonItem : nil
        
        title = didLongGesturesPressed ? "" : "Chi Nhánh"
    }
    
    func getAllCells() -> [BranchTableCell]{
        // get all cells but selected
        var cells = [BranchTableCell]()
        for i in 0..<tableView.numberOfSections {
            for j in 0..<tableView.numberOfRows(inSection: i) {
                if cellSelection[j] == nil {
                    let cell = tableView.cellForRow(at: IndexPath(row: j, section: i)) as! BranchTableCell
                    cells.append(cell)
                }
            }
        }
        return cells
    }
    
    func setup(){
        
        if let data = UserDefaults.standard.object(forKey: "user") as? Data {
            let decoder = JSONDecoder()
            if let infor = try? decoder.decode(LoginModel.self, from: data) {
                self.userInfor = infor
               
            }
        }
        
        if self.userInfor.user.authorities[0].authority == "EMP" {
            addButton.isHidden = false
        }else{
            addButton.isHidden = true
        }
        
        view.backgroundColor = .lightSecondaryGrayTY
        
        notFoundLabel.text = "Không tìm thấy!"
        notFoundLabel.alpha = 0
        notFoundLabel.isHidden = true
        
        self.getBranches(page: page, size: size, didInView: false)

        didLongGesturesPressed = false
        
        searchView.delegate = self
        
        tapBarHeight = tabBarController?.tabBar.frame.size.height ?? 83
        
        actionSheetBottom = ActionSheetBranch(tabBarHeight: tapBarHeight)
        
        configTableView()
        
        addButton.addTarget(self, action: #selector(addButtonDidTapped(_:)), for: .touchUpInside)
        
        refreshControl.addTarget(self, action: #selector(refreshTableView(_:)), for: .valueChanged)
        
        setupLongGestureRecognizerOnTableView()
        
        registerSelectAllBranchNotification()
        registerUnSelectAllBranchNotification()
        
        setupDismissKeyboard()
        registerUpdatedBranchNotification()
        
        
    }
}
// MARK: - Extension
extension BranchViewController {

    func layout(){
        view.addSubview(searchView)
        view.addSubview(tableView)
        view.addSubview(notFoundLabel)
        view.addSubview(addButton)
        
        //searchView
        NSLayoutConstraint.activate([
            searchView.topAnchor.constraint(equalToSystemSpacingBelow: view.safeAreaLayoutGuide.topAnchor, multiplier: 1),
            searchView.leadingAnchor.constraint(equalToSystemSpacingAfter: view.leadingAnchor, multiplier: 1),
            view.trailingAnchor.constraint(equalToSystemSpacingAfter: searchView.trailingAnchor, multiplier: 1)
            
        ])
        
        //tableView
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalToSystemSpacingBelow: searchView.bottomAnchor, multiplier: 1),
            tableView.leadingAnchor.constraint(equalTo: searchView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: searchView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            
        ])
        
        //addButton
        NSLayoutConstraint.activate([
            addButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -60),
            view.trailingAnchor.constraint(equalToSystemSpacingAfter: addButton.trailingAnchor, multiplier: 2)
            
        ])
        
        //not found label
        NSLayoutConstraint.activate([
            notFoundLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            notFoundLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    func configTableView(){
        tableView.register(BranchTableCell.self, forCellReuseIdentifier: BranchTableCell.reuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.rowHeight = BranchTableCell.rowHeight
        tableView.showsVerticalScrollIndicator = false
        tableView.refreshControl = refreshControl
    }
}

extension BranchViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let branch = isSearching ? filteredBranches[indexPath.row] : branches[indexPath.row]
        let vc = BranchDetailViewController()
        vc.delegate = self
        //add branch detail
        vc.branch = branch
        let cell = tableView.cellForRow(at: indexPath) as! BranchTableCell
        //if long gesture tapped, selected call or push to vc
        _ = didLongGesturesPressed ? cell.circleButton.sendActions(for: .touchUpInside)  : self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //scroll to load more
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let height = scrollView.frame.height
        
        if offsetY > contentHeight - height {
            size += 10
            getBranches(page: page, size: size, didInView: true)
        }
    }
}

extension BranchViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearching ? filteredBranches.count : branches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: BranchTableCell.reuseIdentifier, for: indexPath) as! BranchTableCell
        let branch = isSearching ? filteredBranches[indexPath.row] : branches[indexPath.row]
        cell.bindingData(branch: branch)
        cell.delegate = self
        return cell
    }
}

extension BranchViewController: AddNewBranchViewControllerDelegate {
    func didSaveBranch() {
        self.getBranches(page: 1, size: 10, didInView: false)
    }
}

extension BranchViewController: BranchDetailViewControllerDelegate {
    func didBranchDeleted() {
        self.getBranches(page: 1, size: 10, didInView: false)
    }
}

extension BranchViewController: UIGestureRecognizerDelegate{
    
}

extension BranchViewController: BranchTableCellDelegate{
    func didCellSelected(_ cell: BranchTableCell, isSelected: Bool) {
        let point = cell.frame.origin
        if let indexPath = tableView.indexPathForRow(at:point) {
            if isSelected {
                cellSelection[indexPath.row] = indexPath.section
            }else{
                cellSelection[indexPath.row] = nil
            }
        }
    }
}

// /Yes/No question
extension BranchViewController: AlertViewControllerDelegate{
    func didYesButtonTapped() {
        self.dismiss(animated: true, completion: nil)
        deleteBulkBranches()
    }
    
    func didNoButtonTapped() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func displayNotFoundLabel(){

        if isFound {
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseInOut) {
                self.notFoundLabel.alpha = 0
            } completion: { _ in
                self.notFoundLabel.isHidden = true
            }
        }else{
            self.notFoundLabel.isHidden = false
            self.notFoundLabel.alpha = 1
        }
        

    }
}

//search
extension BranchViewController: SearchViewDelegate{
    func textFieldDidEndEditing(_ text: String) {
        guard !text.isEmpty else{
            self.isSearching = false
            self.isFound = true
            self.tableView.reloadData()
            return
        }
        
        searchBranch(name: text) {
            self.tableView.reloadData()
        }
        
        self.isSearching = true
    }
    
    func textFieldDidChangeSelection(_ text: String) {}
    
    
}

extension BranchViewController: AlertNoOptionViewControllerDelegate {
    func didOKButtonTapped() {
        if isBranchInvalid {
            self.dismiss(animated: true, completion: nil)
        }else{
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: .didTokenExpired, object: nil)
            }
        }

    }
}


