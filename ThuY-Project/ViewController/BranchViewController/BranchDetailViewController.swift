//
//  BranchDetailViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 08/05/2022.
//

import UIKit
protocol BranchDetailViewControllerDelegate: AnyObject {
    func didBranchDeleted()
}
class BranchDetailViewController: ViewControllerCustom {
    // MARK: - Subview
    private let mapView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.translatesAutoresizingMaskIntoConstraints = false
        
        
        return view
    }()
    
    let mapScreen = MapScreenViewController()
    
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    var nameBranchView =  InformationBranchView(title: "Tên chi nhánh", content: "")
    var idBranchView =  InformationBranchView(title: "Mã chi nhánh", content: "")
    var phoneBranchView =  InformationBranchView(title: "Số điện thoại", content: "")
    var addressBranchView =  InformationBranchView(title: "Địa chỉ", content: "")
    var noteBranchView = InformationBranchView(title: "Ghi chú:", content: "Không")
    
    var nameBranchViewHeight: CGFloat!
    var idBranchViewHeight: CGFloat!
    var addressBranchViewHeight: CGFloat!
    var phoneBranchViewHeight: CGFloat!
    var noteBranchViewHeight: CGFloat!
    
    
    private let labelStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 16
        return stackView
    }()
    
    lazy var updateButton = ButtonCustom(title: "Cập nhật", color: .greenTY)
    lazy var deleteButton = ButtonCustom(title: "Xoá", color: .redTY)
    
    private let buttonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 12
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
        
    }()
    // MARK: - Properties
    var isDeleted: Bool = false
    var branch: BranchData?{
        didSet{
            guard let branch = branch else {return}
            configureBranch(branch: branch)
        }
    }
    
    var userInfor: LoginModel!
    
    weak var delegate: BranchDetailViewControllerDelegate?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        layout()
        configBackButtonNavBar()
        
    }
    
    
    // MARK: - Selector
    @objc func didDeleteButtonTapped(_ sender: UIButton){
        guard let branch = branch else {return}
        presentAlertOnMainThread(content: "\(branch.name)", selection: .delete) { [weak self] vc in
            guard let self = self else {return}
            vc.delegate = self
        }
    }
    
    @objc func didUpdateButtonTapped(_ sender: UIButton){
        guard let branch = branch else {return}
        let vc = UpdateBranchViewController(branch: branch)
        vc.delegation = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    // MARK: - API
    func deleteBranch(){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else {return}
//        self.showLoadingView()
        NetworkManager.shared.deleteBranch(token: token, branch: branch!) { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .success(let status):
                switch status {
                case 200:
                    self.isDeleted = true
                    self.dismiss(animated: true, completion: nil)
                    self.navigationController?.popViewController(animated: true)
                    self.delegate?.didBranchDeleted()
                    
                case 400:
                    self.isDeleted = false
                    self.dismiss(animated: true, completion: nil)
                    self.presentAlertNoOptionOnMainThread(content: "Tồn tại khách hàng hoặc nhân viên có liên quan đến chi nhánh. Không thể xoá!") { [weak self] vc in
                        guard let self = self else {return}
                        vc.delegate = self
                    }
                   
                    
                default:
                    break
                }

            case .failure(_):
                self.presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { [weak self] vc in
                    guard let self = self else {return}
                    vc.delegate = self
                }
            }
        }
    }
    
    // MARK: - Helper
    
    private func configureBranch(branch: BranchData){
        
        title = branch.name
        nameBranchView.contentLabel.text = branch.name
        idBranchView.contentLabel.text = branch.id
        addressBranchView.contentLabel.text = branch.address
        phoneBranchView.contentLabel.text = branch.phoneNumber
        noteBranchView.contentLabel.text = branch.note
        calculateHeightForInforBranchView(width: InformationBranchView.contenLabelWidth , branch: branch)
        
    }
    
    private func calculateHeightForInforBranchView(width: CGFloat,branch: BranchData){
        let font = UIFont.boldSystemFont(ofSize: 16)
        nameBranchViewHeight = branch.name.heightWithConstrainedWidth(width: width, font: font)
        idBranchViewHeight = branch.id!.heightWithConstrainedWidth(width: width, font: font)
        addressBranchViewHeight = branch.address.heightWithConstrainedWidth(width: width, font: font)
        phoneBranchViewHeight = branch.phoneNumber.heightWithConstrainedWidth(width: width, font: font)
        noteBranchViewHeight = branch.note.heightWithConstrainedWidth(width: width, font: font)
    }
}
// MARK: - Extension
extension BranchDetailViewController {
    
    func setup(){
        view.backgroundColor = .lightSecondaryGrayTY
        
        deleteButton.addTarget(self, action: #selector(didDeleteButtonTapped(_:)), for: .touchUpInside)
        
        updateButton.addTarget(self, action: #selector(didUpdateButtonTapped(_:)), for: .touchUpInside)
        
        bringVCtoParent(from: mapScreen, to: mapView)
        
        if let data = UserDefaults.standard.object(forKey: "user") as? Data {
            let decoder = JSONDecoder()
            if let infor = try? decoder.decode(LoginModel.self, from: data) {
                self.userInfor = infor
               
            }
        }
        
        if self.userInfor.user.authorities[0].authority == "EMP" {
            buttonStackView.isHidden = false
        }else{
            buttonStackView.isHidden = true
        }
        
        
        
        
    }
    func layout(){
        //mapView
        
        view.addSubview(mapView)
        
        view.addSubview(scrollView)
        
        view.addSubview(buttonStackView)
        
        let heightMapView = view.frame.size.height * 0.3
        
        
        
        //--------------------MapView--------------------
        //mapview
        NSLayoutConstraint.activate([
            mapView.heightAnchor.constraint(equalToConstant: heightMapView),
            mapView.topAnchor.constraint(equalToSystemSpacingBelow: view.safeAreaLayoutGuide.topAnchor, multiplier: 1),
            mapView.leadingAnchor.constraint(equalToSystemSpacingAfter: view.leadingAnchor, multiplier: 2),
            view.trailingAnchor.constraint(equalToSystemSpacingAfter: mapView.trailingAnchor, multiplier: 2)
        ])
        
        //--------------------MapView--------------------
        
        
        
        //--------------------ScrollView--------------------
        
        scrollView.addSubview(stackView)
        stackView.addArrangedSubview(labelStackView)
            //scrollView
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalToSystemSpacingBelow: mapView.bottomAnchor, multiplier: 3),
            scrollView.leadingAnchor.constraint(equalTo: mapView.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: mapView.trailingAnchor),
            scrollView.bottomAnchor.constraint(equalTo: buttonStackView.topAnchor,constant: -16)
        
        ])
            //stackView
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        
        ])
        
        //set height for information branch View
        NSLayoutConstraint.activate([
            nameBranchView.heightAnchor.constraint(equalToConstant: nameBranchViewHeight),
            idBranchView.heightAnchor.constraint(equalToConstant: idBranchViewHeight),
            addressBranchView.heightAnchor.constraint(equalToConstant: addressBranchViewHeight),
            phoneBranchView.heightAnchor.constraint(equalToConstant: phoneBranchViewHeight),
            noteBranchView.heightAnchor.constraint(equalToConstant: noteBranchViewHeight)
        ])
        
            //labelStackView
        labelStackView.addArrangedSubview(nameBranchView)
        labelStackView.addArrangedSubview(idBranchView)
        labelStackView.addArrangedSubview(phoneBranchView)
        labelStackView.addArrangedSubview(addressBranchView)
        labelStackView.addArrangedSubview(noteBranchView)
        

        
        
        
        //--------------------ScrollView--------------------

        
        //button stackview
        buttonStackView.addArrangedSubview(updateButton)
        buttonStackView.addArrangedSubview(deleteButton)
        
        NSLayoutConstraint.activate([
            buttonStackView.leadingAnchor.constraint(equalTo: mapView.leadingAnchor),
            buttonStackView.trailingAnchor.constraint(equalTo: mapView.trailingAnchor),
            buttonStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8)
            
        ])
    }
}

extension BranchDetailViewController: UpdateBranchViewControllerDelegate {
    func didBranchUpdated(branch: BranchData) {
        self.branch = branch
        
        // Branch vc reload view after update branch in backend
        NotificationCenter.default.post(name: .updated, object: nil)

    }
}

extension BranchDetailViewController: AlertViewControllerDelegate {
    func didYesButtonTapped() {
        
        deleteBranch()
    }
    
    func didNoButtonTapped() {
        //stay in this view
        self.dismiss(animated: true, completion: nil)
//        self.navigationController?.popViewController(animated: true)
    }

}

extension BranchDetailViewController: AlertNoOptionViewControllerDelegate {
    func didOKButtonTapped() {
        if isDeleted {
            self.dismiss(animated: true) {
                NotificationCenter.default.post(name: .didTokenExpired, object: nil)
            }
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
}




