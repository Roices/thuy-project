//
//  ProvinceViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 05/05/2022.
//

import UIKit

protocol ProvinceViewControllerDelegate: AnyObject {
    func didProvinceSelected(province: Province)
    func didDistrictSelected(district: District, vc: DistrictViewController)
    func didWardSelected(ward: Ward, vc: WardViewController)
}
class ProvinceViewController: UIViewController, SearchViewDelegate {
    func textFieldDidEndEditing(_ text: String) {}
    
    // MARK: - Subview
    var searchView: SearchView!
    
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    lazy var refreshControl = UIRefreshControl()
    
    
    
    // MARK: - Properties
    var provinces: [Province]?{
        didSet{
            tableView.reloadData()
        }
    }
    
    var filteredProvinces = [Province]()
    
    var keyboardIsShowing: Bool = false
    
    var isSearching: Bool = false
//    var filteredProvinces: [Province]
    weak var delegate: ProvinceViewControllerDelegate?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        searchView = SearchView(placeholder: "Tỉnh/ Thành phố")
        setup()
        layout()
        fetchProvinces()
        setupDismissKeyboard()
        keyboardNotification()
    }
    
    // MARK: - Selector
    @objc func refreshTableView(_ sender: UIRefreshControl){
        //waiting for getBranch successful then end refreshing
        let group = DispatchGroup()
        group.enter()
        fetchProvinces()
        group.leave()
        group.notify(queue: .main) {
            self.refreshControl.endRefreshing()
        }
    }
    
    @objc func keyboardWillShow(_ sender: NSNotification){
        keyboardIsShowing = true
    }
    
    @objc func keyboardWillHide(_ sender: NSNotification){

    }
    
    // MARK: - API
    final func fetchProvinces(){
        NetworkManager.shared.fetchProvinces { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .success(let provinces):
                self.provinces = provinces
            case .failure(_):
                break
            }
        }
    }
    
    // MARK: - Helper

    
    func textFieldDidChangeSelection(_ text: String) {
        guard !text.isEmpty else {
            isSearching = false
            filteredProvinces.removeAll()
            tableView.reloadData()
            return
        }
        isSearching = true
        filteredProvinces = provinces?.filter({$0.name.lowercased().contains(text.lowercased())}) ?? []
        tableView.reloadData()
    }
    
    func keyboardNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
}
// MARK: - Extension
extension ProvinceViewController {
    
    func setup(){
        view.backgroundColor = .lightSecondaryGrayTY
        
        refreshControl.addTarget(self, action: #selector(refreshTableView(_:)), for: .valueChanged)
        
        configTableView()
        
        navigationController?.navigationBar.tintColor = .greenTY
        searchView.delegate = self
    }
    func layout(){
        view.addSubview(searchView)
        
        view.addSubview(tableView)
        
        //searchView
        NSLayoutConstraint.activate([
            searchView.topAnchor.constraint(equalToSystemSpacingBelow: view.safeAreaLayoutGuide.topAnchor, multiplier: 1),
            searchView.leadingAnchor.constraint(equalToSystemSpacingAfter: view.leadingAnchor, multiplier: 1),
            view.trailingAnchor.constraint(equalToSystemSpacingAfter: searchView.trailingAnchor, multiplier: 1)
            
        ])
        
        //tableView
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalToSystemSpacingBelow: searchView.bottomAnchor, multiplier: 1),
            tableView.leadingAnchor.constraint(equalTo: searchView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: searchView.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            
        ])
    }
    

    func configTableView(){
        tableView.register(AddressTableCell.self, forCellReuseIdentifier: AddressTableCell.reuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.rowHeight = AddressTableCell.rowHeight
        tableView.showsVerticalScrollIndicator = false
        tableView.refreshControl = refreshControl
    }
    
}

extension ProvinceViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let province = self.provinces else {
            return
        }
        // if keyboard is showing, must dismiss keyboard before
        if keyboardIsShowing {
            view.endEditing(true)
            keyboardIsShowing = false
            
        }else{
            _ = isSearching ? delegate?.didProvinceSelected(province: filteredProvinces[indexPath.row]) : delegate?.didProvinceSelected(province: province[indexPath.row])

            self.navigationController?.popViewController(animated: true)
        }

    }
}

extension ProvinceViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let provinces = provinces else {return 0 }
        let count = isSearching ? filteredProvinces.count : provinces.count
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AddressTableCell.reuseIdentifier, for: indexPath) as! AddressTableCell
        if isSearching{
            let province = filteredProvinces[indexPath.row]
            cell.bindingProvinceData(province: province)
            return cell
        }
        guard let provinces = self.provinces else {return UITableViewCell()}
        let province = provinces[indexPath.row]
        cell.bindingProvinceData(province: province)
        return cell
    }
}



