//
//  DistrictViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 05/05/2022.
//

import UIKit

class DistrictViewController: ProvinceViewController {
    // MARK: - Subview
    
    // MARK: - Properties
    var districts: [District]?{
        didSet{
            tableView.reloadData()
        }
    }
    var cityCode: Int = 0
    
    var filteredDistricts: [District] = [District]()

    
    // MARK: - Lifecycle
    init(cityCode: Int) {
        super.init(nibName: nil, bundle: nil)
        self.cityCode = cityCode
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchView.searchTextField.placeholder = "Quận/ Huyện"
        fetchDistricts(code: cityCode)
        
        
    }
    
    // MARK: - Selector
    
    // MARK: - API
    final func fetchDistricts(code: Int){
        NetworkManager.shared.fetchDistricts(cityCode: code) { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .success(let districts):
                self.districts = districts
            case .failure(_):
                break
            }
        }
    }
    
    // MARK: - Helper
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let districts = self.districts else {
            return
        }
        
        if keyboardIsShowing {
            view.endEditing(true)
            keyboardIsShowing = false
            
        }else{
            _ = isSearching ? delegate?.didDistrictSelected(district: filteredDistricts[indexPath.row], vc: self) : delegate?.didDistrictSelected(district: districts[indexPath.row], vc: self)

            self.navigationController?.popViewController(animated: true)
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let districts = districts else {return 0 }
        let count = isSearching ? filteredDistricts.count : districts.count
        return count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let districts = self.districts else {return UITableViewCell()}
        let cell = tableView.dequeueReusableCell(withIdentifier: AddressTableCell.reuseIdentifier, for: indexPath) as! AddressTableCell
        
        if isSearching {
            let district = filteredDistricts[indexPath.row]
            cell.bingdingDistrictData(district: district)
            return cell
        }
        let district = districts[indexPath.row]
        cell.bingdingDistrictData(district: district)
        return cell
        
    }
    
    override func textFieldDidChangeSelection(_ text: String) {
        guard !text.isEmpty else {
            isSearching = false
            filteredDistricts.removeAll()
            tableView.reloadData()
            return
        }
        isSearching = true
        filteredDistricts = districts?.filter({$0.name.lowercased().contains(text.lowercased())}) ?? []
        tableView.reloadData()
    }
}
// MARK: - Extension


