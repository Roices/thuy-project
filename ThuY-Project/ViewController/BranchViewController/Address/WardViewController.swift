//
//  CommuneViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 05/05/2022.
//

import UIKit

class WardViewController: ProvinceViewController {
    // MARK: - Subview
    
    
    // MARK: - Properties
    var wards: [Ward]?{
        didSet{
            tableView.reloadData()
        }
    }
    var districtCode: Int = 0
    
    var filteredWard: [Ward] = [Ward]()
    
    // MARK: - Lifecycle
    init(districtCode: Int) {
        super.init(nibName: nil, bundle: nil)
        self.districtCode = districtCode
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchView.searchTextField.placeholder = "Xã/ Phường/ Thị trấn"
        fetchWards(code: districtCode)
    }
    
    // MARK: - Selector
    
    // MARK: - API
    final func fetchWards(code: Int){
        NetworkManager.shared.fetchWards(districtCode: code) { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .success(let wards):
                self.wards = wards
            case .failure(_):
                break
            }
        }
    }
    
    // MARK: - Helper
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let wards = self.wards else {
            return
        }
        if keyboardIsShowing {
            view.endEditing(true)
            keyboardIsShowing = false
            
        }else{
            _ = isSearching ? delegate?.didWardSelected(ward: filteredWard[indexPath.row], vc: self) : delegate?.didWardSelected(ward: wards[indexPath.row], vc: self)

            self.navigationController?.popViewController(animated: true)
        }

    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let wards = wards else {return 0 }
        let count = isSearching ? filteredWard.count : wards.count
        return count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let wards = self.wards else {return UITableViewCell()}
        let cell = tableView.dequeueReusableCell(withIdentifier: AddressTableCell.reuseIdentifier, for: indexPath) as! AddressTableCell
        
        if isSearching {
            let ward = filteredWard[indexPath.row]
            cell.bindingWardData(ward: ward)
            return cell
        }
        let ward = wards[indexPath.row]
        cell.bindingWardData(ward: ward)
        return cell
    }
    
    override func textFieldDidChangeSelection(_ text: String) {
        guard !text.isEmpty else {
            isSearching = false
            filteredWard.removeAll()
            tableView.reloadData()
            return
        }
        isSearching = true
        filteredWard = wards?.filter({$0.name.lowercased().contains(text.lowercased())}) ?? []
        tableView.reloadData()
    }
}
// MARK: - Extension


