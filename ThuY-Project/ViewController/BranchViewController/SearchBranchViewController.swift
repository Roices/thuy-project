//
//  SearchBranchViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 24/05/2022.
//

import UIKit

class SearchBranchViewController: BranchViewController {
    // MARK: - Subview
    
    // MARK: - Properties
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // MARK: - Selector
    override func refreshTableView(_ sender: UIRefreshControl) {
    }
    
    // MARK: - API
    
    // MARK: - Helper
    override func setup() {
        view.backgroundColor = .lightSecondaryGrayTY
        configTableView()
        configBackButtonNavBar()
        title = "Tìm kiếm"
    }

}
// MARK: - Extension

