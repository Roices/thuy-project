//
//  AddNewBranchViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 04/05/2022.
//

import UIKit

protocol AddNewBranchViewControllerDelegate: AnyObject {
    func didSaveBranch()
}

enum BranchTextFieldError: CaseIterable {
    case branchName, branchProvince, branchDistrict, branchWard,  branchPhone, branchStreet
    
    var description: String{
        switch self{
        case .branchName:
            return "Tên không được để trống"
        case .branchProvince:
            return "Tỉnh/ Thành Phố không được để trống"
        case .branchDistrict:
            return "Quận/ Huyện/ Thị xã không được để trống"
        case .branchWard:
            return "Xã/ Phường/ Thị trấn không được để trống"
        case .branchPhone:
            return "Số điện thoại không được để trống"
        case .branchStreet:
            return "Số nhà/ Đường không được để trống"
        }
    }
}

class AddNewBranchViewController: ViewControllerCustom {
    // MARK: - Subview
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 8
        stackView.axis = .vertical
        return stackView
    }()
    
    let footerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 52).isActive = true
        return view
    }()

    
    var branchNameView = BranchTextFieldView(label: BranchTextFields.name.title)
    var branchPhoneView = BranchTextFieldView(label: BranchTextFields.phone.title)
    var branchCityView = BranchTextFieldView(label: BranchTextFields.city.title)
    var branchDistrictView = BranchTextFieldView(label: BranchTextFields.district.title)
    var branchWardView = BranchTextFieldView(label: BranchTextFields.ward.title)
    var branchNoteTextView = BranchTextView(label: "Ghi chú")
    var branchStreetView = BranchTextFieldView(label: BranchTextFields.street.title)
    let textfieldStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.spacing = 16
        stackView.axis = .vertical
        return stackView
    }()
    
    lazy var addButton = ButtonCustom(title: "Thêm Mới", color: .greenTY)
    lazy var cancelButton = ButtonCustom(title: "Huỷ Bỏ", color: .redTY)
    
    private let buttonStackView: UIStackView = {
       let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 12
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    // MARK: - Properties
    var widthNameView: CGFloat = 0
    let padding: CGFloat = 8
    
    var cityCode: Int = 0
    var districtCode: Int = 0
    
    weak var delegate: AddNewBranchViewControllerDelegate?
    
    var isKeyboardShow: Bool = false
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        layout()
        setupDismissKeyboard()
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        keyboardNotification()
    }
    
    override func viewDidDisappear(_ animated: Bool)
    {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    // MARK: - Selector
    @objc func addButtonTapped(_ sender: UIButton) {
        if handleError(){
            return
        }
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        let branch = setBranch()
        NetworkManager.shared.saveBranches(token: token, branch: branch) { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .success():
                self.delegate?.didSaveBranch()
                self.dismiss(animated: true, completion: nil)
            case .failure(_):
                self.presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { [weak self] vc in
                    guard let self = self else {return}
                    vc.delegate = self
                }
            }
        }
       
    }
    
    @objc func cancelButtonTapped(_ sender: UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func didCityTextfieldTapped(_ sender: UITapGestureRecognizer){
        let vc = ProvinceViewController()
        vc.delegate = self
        
        branchCityView.textField.resignFirstResponder()
        view.endEditing(true)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func didDistrictTextfieldTapped(_ sender: UITapGestureRecognizer){
        let vc = DistrictViewController(cityCode: cityCode)
        vc.delegate = self
        view.endEditing(true)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func didWardTextfieldTapped(_ sender: UITapGestureRecognizer){
        let vc = WardViewController(districtCode: districtCode)
        vc.delegate = self
        view.endEditing(true)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    @objc func keyboardWillShow(_ sender: NSNotification){
        if !isKeyboardShow {
            if let currentView = UIResponder.currentFirst() as? UITextView {
                moveView(sender, textView: currentView, textField: nil, isTextField: false)
                
            }else if let currentView = UIResponder.currentFirst() as? UITextField {
                moveView(sender, textView: nil, textField: currentView, isTextField: true)
            }
        }
        isKeyboardShow = true
        
    }
    
    @objc func keyboardWillHide(_ sender: NSNotification){
        let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
        UIView.animate(withDuration: 0.2, delay: 0.05, options: .curveEaseInOut) {
            self.view.frame.origin.y = navigationBarHeight
        } completion: { _ in
            self.isKeyboardShow = false
        }
    }
    
    // MARK: - API
    
    // MARK: - Helper
    
    func moveView(_ sender: NSNotification, textView: UITextView?, textField: UITextField?, isTextField: Bool){
        guard let userinfor = sender.userInfo,
              let keyboardFrame = userinfor[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue
        else {return}
        //Y of keyboard
        let keyboardTopY = keyboardFrame.cgRectValue.origin.y
        
        //bottom of uitextfield
        let currentView = isTextField ? textField! : textView!
        let convertedTextFieldFrame = view.convert(currentView.frame, from: currentView.superview)
        let textViewBottomY = convertedTextFieldFrame.origin.y + currentView.frame.height
        
        let origintextViewBottomY = isTextField ? textViewBottomY + 75 : textViewBottomY
        //75 : make sure bottom always under tf
        if origintextViewBottomY > keyboardTopY {
            UIView.animate(withDuration: 0.3, animations: {
                let newFrameY = keyboardFrame.cgRectValue.size.height
                self.view.frame.origin.y -= newFrameY
            }, completion: nil)
        }
    }
    
    func configAddressTextField(){
        let selectors = [#selector(didCityTextfieldTapped(_:)), #selector(didDistrictTextfieldTapped(_:)), #selector(didWardTextfieldTapped(_:))]
        
        let textfields = [branchCityView.textField, branchDistrictView.textField, branchWardView.textField]
        
        //add gesture to address textfield
        for i in 0..<textfields.count {
            let gesture = UITapGestureRecognizer(target: self, action: selectors[i])
            textfields[i].addGestureRecognizer(gesture)
            textfields[i].isUserInteractionEnabled = true

        }
    }
    func setBranch() -> BranchData{

        let name = getTextFromTf(textField: branchNameView.textField)
        let city = getTextFromTf(textField: branchCityView.textField)
        let district = getTextFromTf(textField: branchDistrictView.textField)
        let ward = getTextFromTf(textField: branchWardView.textField)
        let phone = getTextFromTf(textField: branchPhoneView.textField)
        let street = getTextFromTf(textField: branchStreetView.textField)
        
        let address = "\(street),\(ward),\(district),\(city)"
        
        guard let note = branchNoteTextView.textView.text, !note.isEmpty, note != "Ghi chú" else {
            return BranchData(id: nil, name: name, address: address, phoneNumber: phone, note: "Không có")
        }
        

        return BranchData(id: nil, name: name, address: address, phoneNumber: phone, note: note)
    }

    
    func handleError() -> Bool{
        
        var haveError: Bool = false
        
        let nameCriteria = TextfieldCriteria.emptyTextCriteriaMet(getTextFromTf(textField: branchNameView.textField))
        let cityCriteria = TextfieldCriteria.emptyTextCriteriaMet(getTextFromTf(textField: branchCityView.textField))
        let districtCriteria = TextfieldCriteria.emptyTextCriteriaMet(getTextFromTf(textField: branchDistrictView.textField))
        
        let wardCriteria = TextfieldCriteria.emptyTextCriteriaMet(getTextFromTf(textField: branchWardView.textField))
        
        let phoneCriteria = TextfieldCriteria.emptyTextCriteriaMet(getTextFromTf(textField: branchPhoneView.textField))
        
        let streetCriteria = TextfieldCriteria.emptyTextCriteriaMet(getTextFromTf(textField: branchStreetView.textField))
        
        
        
        // check empty textfield
        let critetias = [nameCriteria, cityCriteria, districtCriteria, wardCriteria, phoneCriteria, streetCriteria]
        let textfieldView = [branchNameView, branchCityView, branchDistrictView, branchWardView, branchPhoneView, branchStreetView]
        
        for index in 0..<critetias.count {
            if critetias[index] {
                textfieldView[index].textField.layer.borderColor = UIColor.redTY.cgColor
                textfieldView[index].showWarningRightView()
                haveError = true
            }
        }
        
        let phoneInvalidCriteria = TextfieldCriteria.lengthPhoneCriteriaMet(getTextFromTf(textField: branchPhoneView.textField))
        
        if !haveError && !phoneInvalidCriteria {
            showToast(message: "Số điện thoại không hợp lệ", font: UIFont.preferredFont(forTextStyle: .body))
            haveError = true
        }
        let includingCommaCriteria = TextfieldCriteria.includingCommaCriteriaMet(getTextFromTf(textField: branchStreetView.textField))
        
        if !haveError && includingCommaCriteria {
            showToast(message: "Số nhà/ Đường không được chứa dấu ',' ", font: UIFont.preferredFont(forTextStyle: .body))
            haveError = true
        }
        
        return haveError
    }
    
    func getTextFromTf(textField: UITextField) -> String{
        guard let text = textField.text, !text.isEmpty else {
            return ""
        }
        return text
    }
    
    func keyboardNotification(){

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
}
// MARK: - Extension
extension AddNewBranchViewController {
    
    func setup(){
        configAddressTextField()
        view.backgroundColor = .lightSecondaryGrayTY
        title = "Thêm Chi Nhánh"
        // (padding * 3) = branchName leading + branchName trailing * 2
        widthNameView = view.frame.width * (2 / 3) - (padding * 2 * 3)
        
        addButton.addTarget(self, action: #selector(addButtonTapped(_:)), for: .touchUpInside)
        cancelButton.addTarget(self, action: #selector(cancelButtonTapped(_:)), for: .touchUpInside)
        
        branchPhoneView.textField.keyboardType = .numberPad
        branchNameView.delegate = self
        branchPhoneView.delegate = self
        branchDistrictView.delegate = self
        branchWardView.delegate = self
        branchCityView.delegate = self
        branchStreetView.delegate = self
        
            }
    func layout(){
        
    //--------------------ScrollView--------------------
        view.addSubview(scrollView)
        view.addSubview(buttonStackView)
        scrollView.addSubview(stackView)
  
        //--------------------buttonStackView--------------------
        buttonStackView.addArrangedSubview(addButton)
        buttonStackView.addArrangedSubview(cancelButton)
        NSLayoutConstraint.activate([
            buttonStackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            buttonStackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            buttonStackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8)
             
        ])
        
        //--------------------buttonStackView--------------------
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalToSystemSpacingBelow: view.safeAreaLayoutGuide.topAnchor, multiplier: 2),
            scrollView.leadingAnchor.constraint(equalToSystemSpacingAfter: view.leadingAnchor, multiplier: 2),
            view.trailingAnchor.constraint(equalToSystemSpacingAfter: scrollView.trailingAnchor, multiplier: 2),
            scrollView.bottomAnchor.constraint(equalTo: addButton.bottomAnchor, constant: -8)
            
        ])
        
    //--------------------ScrollView--------------------
        
        //--------------------StackView--------------------
        
        stackView.addArrangedSubview(branchNameView)
        stackView.addArrangedSubview(branchPhoneView)
        stackView.addArrangedSubview(branchCityView)
        stackView.addArrangedSubview(branchDistrictView)
        stackView.addArrangedSubview(branchWardView)
        stackView.addArrangedSubview(branchStreetView)
        stackView.addArrangedSubview(branchNoteTextView)
        stackView.addArrangedSubview(footerView)
        
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
        
        //--------------------StackView--------------------
    }
}

extension AddNewBranchViewController: ProvinceViewControllerDelegate {
    func didDistrictSelected(district: District, vc: DistrictViewController) {
        branchDistrictView.textField.text = district.name
        
        // select province again -> reset ward
        if districtCode != 0 && district.code != districtCode {
            branchWardView.textField.text = ""
            vc.districts = []
            
        }
        
        districtCode = district.code

        if branchDistrictView.textField.text?.isEmpty != true {
            branchDistrictView.reset()
        }
        
    }
    
    func didWardSelected(ward: Ward, vc: WardViewController) {
        branchWardView.textField.text = ward.name
        vc.wards = []
        if branchWardView.textField.text?.isEmpty != true {
            branchWardView.reset()
        }
    }

    func didProvinceSelected(province: Province) {
        branchCityView.textField.text = province.name
        
        // select province again -> reset district & ward
        if cityCode != 0 && cityCode != province.code {
            branchDistrictView.textField.text = ""
            branchWardView.textField.text = ""
        }
  
        cityCode = province.code
        if branchCityView.textField.text?.isEmpty != true {
            branchCityView.reset()
        }
    }
}


extension AddNewBranchViewController: BranchTextFieldViewDelegate {
    func didWarningButtonTapped(view: BranchTextFieldView) {
        switch view {
        case branchNameView:
            showToast(message: BranchTextFieldError.branchName.description, font: UIFont.preferredFont(forTextStyle: .subheadline))
        case branchPhoneView:
            showToast(message: BranchTextFieldError.branchPhone.description, font: UIFont.preferredFont(forTextStyle: .subheadline))
        case branchDistrictView:
            showToast(message: BranchTextFieldError.branchDistrict.description, font: UIFont.preferredFont(forTextStyle: .subheadline))
        case branchWardView:
            showToast(message: BranchTextFieldError.branchWard.description, font: UIFont.preferredFont(forTextStyle: .subheadline))
        case branchCityView:
            showToast(message: BranchTextFieldError.branchProvince.description, font: UIFont.preferredFont(forTextStyle: .subheadline))
        case branchStreetView:
            showToast(message: BranchTextFieldError.branchStreet.description, font: UIFont.preferredFont(forTextStyle: .subheadline))
        default:
            break
        }
    }
    
    func didTextFieldChanged(view: BranchTextFieldView) {
        switch view {
        case branchNameView:
            branchNameView.reset()
            
        case branchPhoneView:
            branchPhoneView.reset()
            
        case branchStreetView:
            branchStreetView.reset()
        default:
            break
        }
    }
}

extension AddNewBranchViewController: AlertNoOptionViewControllerDelegate {
    func didOKButtonTapped() {
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: .didTokenExpired, object: nil)
        }
    }
}
