//
//  BranchFilterViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 26/05/2022.
//

import Foundation
import UIKit

class BranchFilterViewController: UIViewController {
    // MARK: - Subview
    
    // MARK: - Properties
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        layout()
    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension
extension BranchFilterViewController {
    
    func setup(){
        view.backgroundColor = .blueTY
    }
    func layout(){
        
    }
}
