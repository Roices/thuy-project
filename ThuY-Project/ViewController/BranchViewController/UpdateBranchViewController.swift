//
//  UpdateBranchViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 12/05/2022.
//

import UIKit
protocol UpdateBranchViewControllerDelegate: AnyObject {
    func didBranchUpdated(branch: BranchData)
}
class UpdateBranchViewController: AddNewBranchViewController {
    // MARK: - Subview
    var branch: BranchData!
    // MARK: - Properties
    weak var delegation: UpdateBranchViewControllerDelegate?
    
    // MARK: - Lifecycle
    init(branch: BranchData) {
        super.init(nibName: nil, bundle: nil)
        self.branch = branch
        configTextfield(branch: branch)
        cityCode = -1
        districtCode = -1
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showLoadingView()
        searchAddress()
        addButton.setTitle("Cập nhật", for: .normal)
        configBackButtonNavBar()
        title = "Chỉnh sửa chi nhánh"

        branchDistrictView.delegate = self
        branchWardView.delegate = self
        branchCityView.delegate = self
    
    }
    
    
    
    // MARK: - Selector
    override func keyboardWillHide(_ sender: NSNotification) {
        let navigationBarHeight: CGFloat = self.navigationController!.navigationBar.frame.height
        let statusBarHeight: CGFloat =  view.window?.windowScene?.statusBarManager?.statusBarFrame.height ?? 0
        UIView.animate(withDuration: 0.2, delay: 0.05, options: .curveEaseInOut) {
            self.view.frame.origin.y = navigationBarHeight + statusBarHeight
        } completion: { _ in
            self.isKeyboardShow = false
        }
    }
    //update button
    override func addButtonTapped(_ sender: UIButton) {
        if handleError() {
            return
        }
        presentAlertOnMainThread(content: "Lưu thay đổi", selection: .update) { [weak self] vc in
            guard let self = self else {return}
            vc.delegate = self
        }
    }
    
    override func cancelButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

    // MARK: - API
    
    
    func updateBranch(){
        
        guard let token = UserDefaults.standard.string(forKey: "access_token") else {return}
        let branch = getInforBranch()
        NetworkManager.shared.updateBranch(token: token, branch: branch) { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .success():

                self.delegation?.didBranchUpdated(branch: branch)
                self.navigationController?.popViewController(animated: true)
                
            case .failure(_):
                self.presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { [weak self] vc in
                    guard let self = self else {return}
                    vc.delegate = self
                }
            }
        }
    }
    func getCodeAddress(group: DispatchGroup){
        group.enter()
        guard let province = branchCityView.textField.text, !province.isEmpty else {return}
        guard let district = branchDistrictView.textField.text, !district.isEmpty else {return}
        NetworkManager.shared.searchAddress(isProvince: true,address: province) { [weak self] result in
            guard let self = self else{return}
            switch result{
            case .success(let code):
                self.cityCode = code
                group.leave()
            case .failure(_):
                break
            }
        }
        
        group.enter()
        NetworkManager.shared.searchAddress(isProvince: false, address: district) { [weak self] result in
            guard let self = self else{return}
            switch result{
            case .success(let code):
                self.districtCode = code
                group.leave()
            case .failure(_):
                break
            }
        }
    }
    

    
    // MARK: - Helper
    func searchAddress(){
        let group = DispatchGroup()
        getCodeAddress(group: group)
        group.notify(queue: .main) {
            self.dismissLoadingView()
        }
    }
    
    func getInforBranch() -> BranchData{
        let address = branch.address.splitAddressToAddressArray()
        
        var name = branchNameView.textField.text ?? ""
        name = name.isEmpty ? branch.name : name
        
        var phone = branchPhoneView.textField.text ?? ""
        phone = phone.isEmpty ? branch.phoneNumber : phone
        
        var note = branchNoteTextView.textView.text ?? ""
        note = note.isEmpty ? "Không có" : note
        
        var province = branchCityView.textField.text ?? ""
        province = province.isEmpty ? address.province : province
        
        var district = branchDistrictView.textField.text ?? ""
        district = district.isEmpty ? address.district : district
        
        var street = branchStreetView.textField.text ?? ""
        street = street.isEmpty ? address.detail : street
        
        var ward = branchWardView.textField.text ?? ""
        ward = ward.isEmpty ? address.ward : ward
        
        let add = "\(street),\(ward),\(district),\(province)"
        
        return BranchData(id: branch.id!, name: name, address: add, phoneNumber: phone, note: note)
    }
    
    func configTextfield(branch: BranchData){
        let address = branch.address.splitAddressToAddressArray()
        
        //address
        branchCityView.textField.text = address.province
        branchDistrictView.textField.text = address.district
        branchWardView.textField.text = address.ward
        branchStreetView.textField.text = address.detail
        
        //infor
        branchNameView.textField.text = branch.name
        branchPhoneView.textField.text = branch.phoneNumber
        branchNoteTextView.textView.text = branch.note
    }
    
}
// MARK: - Extension
extension UpdateBranchViewController: AlertViewControllerDelegate {
    func didYesButtonTapped() {
        self.dismiss(animated: true, completion: nil)
        updateBranch()
        
    }
    
    func didNoButtonTapped() {
        //stay in this view
        self.dismiss(animated: true, completion: nil)
    }
}
