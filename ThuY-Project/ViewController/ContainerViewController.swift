//
//  ContainerHomeViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 29/04/2022.
//

import UIKit

class ContainerViewController: UIViewController {
    // MARK: - Subview
    let tabBarVC = MainTabViewController()
    let menuVC = SideMenuViewController()
    let branchFilterVC = BranchFilterViewController()
    var homeVC: HomeViewController!
    
    // MARK: - Properties
    // side menu status
    enum SideMenuStatus {
        case opened, closed
    }
    
    private var sideMenuStatus: SideMenuStatus = .closed
    
    static let slideMenuPadding: CGFloat = UIScreen.main.bounds.width * 0.7
    
    var panGesture: UIPanGestureRecognizer!
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        layout()
        addChildVCs()
        registerSideMenuNotification()
        reigsterTokenExpiredNotification()

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    
    // MARK: - Selector
    
    @objc func didSideMenuTappedFromHome(){
//        homeView = tabBarVC.viewControllers?.first as? ViewControllerCustom

        switch sideMenuStatus {
    
        case .opened:
            //close it
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut) {
                self.tabBarVC.view.frame.origin.x = 0
                self.tabBarVC.dismissWaitingView()
            } completion: { [weak self] done in
                guard let self = self else {return}
                if done{
                    self.sideMenuStatus = .closed
                }
            }
            
            
        case .closed:
            
            //open it
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut) {
                self.tabBarVC.view.frame.origin.x = ContainerViewController.slideMenuPadding
                self.tabBarVC.showWaitingView()
            } completion: { [weak self] done in
                guard let self = self else {return}
                if done{
                    self.sideMenuStatus = .opened
                }
            }
        }
    }
    
    @objc func tokenExpired(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    
    // MARK: - API
    
    // MARK: - Helper
    private func registerSideMenuNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(didSideMenuTappedFromHome), name: .sideMenu, object: nil)
    }
    
    private func reigsterTokenExpiredNotification(){
        NotificationCenter.default.addObserver(self, selector: #selector(tokenExpired), name: .didTokenExpired, object: nil)
    }
    

}
// MARK: - Extension
extension ContainerViewController {
    
    func setup(){
        menuVC.delegate = self
    }
    
    func layout(){
        
    }
    
    private func addChildVCs(){
        // menuVC
        addChild(menuVC)
        view.addSubview(menuVC.view)
        menuVC.didMove(toParent: self)
        
        //tabbar
        addChild(tabBarVC)
        view.addSubview(tabBarVC.view)
        tabBarVC.didMove(toParent: self)
        
    }
    
    func pushVC(vc: UIViewController){
        self.navigationController?.pushViewController(vc, animated: true)
        NotificationCenter.default.post(name: .sideMenu, object: nil)
    }
}

extension ContainerViewController: SideMenuViewControllerDelegate {
    func didUserInfor(model: LoginModel) {
        let vc = UserProfileViewController(user: model)
        self.navigationController?.pushViewController(vc, animated: true)
        NotificationCenter.default.post(name: .sideMenu, object: nil)
    }
    
    func didLogout() {
        NotificationCenter.default.post(name: .sideMenu, object: nil)
        self.navigationController?.popToRootViewController(animated: true)
        

    }
    
    func didSideRowTapped(_ selection: Overview) {
        switch selection {
        case .branch:
            pushVC(vc: BranchViewController())
        case .pet:
            pushVC(vc: SpeciesViewController())
        case .staff:
//            pushVC(vc: StaffViewController())
            tabBarVC.selectedIndex = 1
            NotificationCenter.default.post(name: .sideMenu, object: nil)
        case .booking:
            tabBarVC.selectedIndex = 2
            NotificationCenter.default.post(name: .sideMenu, object: nil)
        case .owner:
            pushVC(vc: CustomerViewController())
        default:
            break
        }
    }
}


