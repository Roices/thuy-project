//
//  AlertViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 08/05/2022.
//

import UIKit

protocol AlertViewControllerDelegate: AnyObject {
    func didYesButtonTapped()
    func didNoButtonTapped()
}

class AlertViewController: UIViewController {
    // MARK: - Subview
    private let containerView = AlertContainerView()
    private let titleLabel = LabelCustom(color: .black, fontFamily: UIFont.boldSystemFont(ofSize: 22), alignment: .center)
    
    lazy var yesButton = ButtonCustom(title: "Có", color: .greenTY)
    lazy var noButton = ButtonCustom(title: "Không", color: .redTY)
    
    private let buttonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 12
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 32
        return stackView
        
    }()
    
    var containerHeightConstant: CGFloat!
    
    // MARK: - Properties
    init(content: String, selection: AlertSelection) {
        super.init(nibName: nil, bundle: nil)
        
        switch selection {
        case .delete:
            titleLabel.text = "\(selection.title) \(content)"
        case .update:
            titleLabel.text = selection.title
        case .tokenExprired:
            titleLabel.text = selection.title
        case .normal:
            break
        }
    }
    
    weak var delegate: AlertViewControllerDelegate?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var widthContainerConstant: CGFloat = 0
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        layout()
    }
    
    // MARK: - Selector
    @objc func didYesButtonTapped(_ sender: UIButton){
        delegate?.didYesButtonTapped()
    }
    
    @objc func didNoButtonTapped(_ sender: UIButton){
        delegate?.didNoButtonTapped()
    }
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension
extension AlertViewController {
    
    func setup(){
        widthContainerConstant = view.frame.width - (32 * 2)
        
        containerHeightConstant = 150
        
        yesButton.addTarget(self, action: #selector(didYesButtonTapped(_:)), for: .touchUpInside)
        noButton.addTarget(self, action: #selector(didNoButtonTapped(_:)), for: .touchUpInside)
        
    }
    
    func layout(){
        
        view.addSubview(containerView)
        
        containerView.addSubview(stackView)
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(buttonStackView)
        
        buttonStackView.addArrangedSubview(yesButton)
        buttonStackView.addArrangedSubview(noButton)
        
        //containerView
        NSLayoutConstraint.activate([
            containerView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            containerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            containerView.widthAnchor.constraint(equalToConstant: widthContainerConstant),
            containerView.heightAnchor.constraint(equalToConstant: containerHeightConstant)
        ])
        
        //StackView
        NSLayoutConstraint.activate([
            stackView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            stackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            stackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -8),
            
        ])
        
        buttonStackView.setContentHuggingPriority(.defaultHigh, for: .vertical)
        titleLabel.setContentHuggingPriority(.defaultLow, for: .vertical)
    }
}
