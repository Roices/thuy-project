//
//  AlertNoOptionViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 24/05/2022.
//

import UIKit
protocol AlertNoOptionViewControllerDelegate: AnyObject {
    func didOKButtonTapped()
}

class AlertNoOptionViewController: UIViewController {
    private let containerView = AlertContainerView()
    private var titleLabel = LabelCustom(color: .black, fontFamily: UIFont.boldSystemFont(ofSize: 22), alignment: .center)
    
    lazy var yesButton = ButtonCustom(title: "OK", color: .greenTY)
    
    private let buttonStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .horizontal
        stackView.spacing = 12
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = 32
        return stackView
        
    }()
    
    var containerHeightConstant: CGFloat!
    
    // MARK: - Properties
    init(content: String) {
        super.init(nibName: nil, bundle: nil)
        titleLabel.text = content
        
        if titleLabel.text?.findNumberWordInSentence() ?? 0 > 10 {
            titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
        }
        titleLabel.numberOfLines = 0
        titleLabel.lineBreakMode = .byWordWrapping
    }
    
    weak var delegate: AlertNoOptionViewControllerDelegate?
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var widthContainerConstant: CGFloat = 0
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.75)
        layout()
    }
    
    // MARK: - Selector
    @objc func didYesButtonTapped(_ sender: UIButton){
        delegate?.didOKButtonTapped()
//        self.dismiss(animated: true, completion: nil)
    }
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension
extension AlertNoOptionViewController {
    
    func setup(){
        widthContainerConstant = view.frame.width - (32 * 2)
        
        containerHeightConstant = 150
        
        yesButton.addTarget(self, action: #selector(didYesButtonTapped(_:)), for: .touchUpInside)

        
        
    }
    
    func layout(){
        
        view.addSubview(containerView)
        
        containerView.addSubview(stackView)
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(buttonStackView)
        
        buttonStackView.addArrangedSubview(yesButton)
        
        //containerView
        NSLayoutConstraint.activate([
            containerView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            containerView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            containerView.widthAnchor.constraint(equalToConstant: widthContainerConstant),
            containerView.heightAnchor.constraint(equalToConstant: containerHeightConstant)
        ])
        
        //StackView
        NSLayoutConstraint.activate([

            stackView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            stackView.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 32),
            stackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -32),
            
        ])
        
        buttonStackView.setContentHuggingPriority(.defaultHigh, for: .vertical)
        titleLabel.setContentHuggingPriority(.defaultLow, for: .vertical)
    }
}
