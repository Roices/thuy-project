//
//  UpdatePetsViewController.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 18/05/2022.
//

import UIKit

class UpdatePetsViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    var obj: PetDataModel!
    let viewUpdatePet = UINib(nibName: "UpdatePets", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UpdatePets
    override func viewDidLoad() {
        super.viewDidLoad()
        viewUpdatePet.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        viewUpdatePet.id = self.obj.id
        viewUpdatePet.root = self
        viewUpdatePet.obj = obj
        if CustomerViewController.roleCustomer == "DOC"{
            viewUpdatePet.btnCancel.isHidden = true
            viewUpdatePet.btnUpdate.isHidden = true
            viewUpdatePet.btnAddImg.isHidden = true
        }else{
            viewUpdatePet.btnCancel.isHidden = false
            viewUpdatePet.btnUpdate.isHidden = false
            viewUpdatePet.btnAddImg.isHidden = false
        }
        configBackButtonNavBar()
        title = "Cập nhật thú nuôi"
        self.view.addSubview(viewUpdatePet)
        viewUpdatePet.btnAddImg.addTarget(self, action:#selector(clickShowMoreImg(_:)), for: .touchUpInside)
        viewUpdatePet.btnAddImg.tag = 0
        viewUpdatePet.setData(objData: obj)
        viewUpdatePet.btnUpdate.addTarget(self, action: #selector(clickShowMore(_:)), for: .touchUpInside)
    }
    
    @objc func clickShowMore(_ sender:UIButton) {
        let imgData = viewUpdatePet.imgBirthCerti.image?.pngData()
        let arr = viewUpdatePet.txtBreed.text?.components(separatedBy: " ")
        if viewUpdatePet.txtNamePets.text?.count != 0 && viewUpdatePet.txtSex.text?.count != 0 && viewUpdatePet.txtBreed.text?.count != 0 && imgData != nil {
            var idSex : Int
            if viewUpdatePet.txtSex.text! == "Đực"{
                idSex = 1
            }else{
                idSex = 2
            }
            commomSevice().putApiPetsFormData(idPet: self.obj.id, name: viewUpdatePet.txtNamePets.text!, sex: idSex, birthCerti: imgData!, note: viewUpdatePet.txtNote.text ?? "", customerId: self.obj.customerId, idBreed: arr![arr!.count - 1])
            self.navigationController?.popViewController(animated: true)
        }else{
            var err : String = ""
            if viewUpdatePet.txtNamePets?.text?.count == 0{
                viewUpdatePet.txtNamePets.layer.borderWidth = 1
                viewUpdatePet.txtNamePets.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền tên thú nuôi, "
            }else{
                viewUpdatePet.txtNamePets.layer.borderWidth = 0
            }
            if viewUpdatePet.txtSex.text?.count == 0{
                viewUpdatePet.txtSex.layer.borderWidth = 1
                viewUpdatePet.txtSex.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền giới tính, "
            }else{
                viewUpdatePet.txtSex.layer.borderWidth = 0
            }
            if imgData == nil {
                err += "chưa điền ảnh giấy khai sinh, "
            }
            if viewUpdatePet.txtBreed.text?.count == 0{
                viewUpdatePet.txtBreed.layer.borderWidth = 1
                viewUpdatePet.txtBreed.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền giống thú nuôi. "
            }else{
                viewUpdatePet.txtBreed.layer.borderWidth = 0
            }
            showAlert(title: "Thông báo", message: err)
        }
    }
    
    @objc func clickShowMoreImg(_ sender:UIButton) {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else {return}
        viewUpdatePet.imgBirthCerti.image = image
        dismiss(animated: true)
    }
}
