//
//  AddPetsViewController.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 18/05/2022.
//

import UIKit

class AddPetsViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    let viewAddPet = UINib(nibName: "AddPets", bundle: nil).instantiate(withOwner: AddPetsViewController.self, options: nil)[0] as! AddPets
    var id: String!
    var documentsUrl: URL {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        viewAddPet.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        viewAddPet.root = self
        viewAddPet.idCustomer = id
        self.view.addSubview(viewAddPet)
        viewAddPet.btnAddImg.addTarget(self, action:#selector(clickShowMoreImg(_:)), for: .touchUpInside)
        configBackButtonNavBar()
        title = "Thêm thú nuôi"
        viewAddPet.btnAddImg.tag = 0
        viewAddPet.btnAdd.addTarget(self, action: #selector(clickShowMore(_:)), for: .touchUpInside)
    }
    @objc func clickShowMore(_ sender:UIButton) {
        let imgData = viewAddPet.imgBirthCerti.image?.pngData()
        let arr = viewAddPet.txtBreed.text?.components(separatedBy: " ")
        if viewAddPet.txtNamePets.text?.count != 0 && viewAddPet.txtSex.text?.count != 0 && viewAddPet.txtBreed.text?.count != 0 && imgData != nil  {
            var idSex : Int
            if viewAddPet.txtSex.text! == "Đực"{
                idSex = 1
            }else{
                idSex = 2
            }
            commomSevice().postApiPetFormData(name: viewAddPet.txtNamePets.text!, Idsex: idSex, birthCerti: imgData!, note: viewAddPet.txtNote.text ?? "", breed: arr![arr!.count - 1], customerId: self.id)
            self.navigationController?.popViewController(animated: true)
        }else{
            var err : String = ""
            if viewAddPet.txtNamePets?.text?.count == 0{
                viewAddPet.txtNamePets.layer.borderWidth = 1
                viewAddPet.txtNamePets.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền tên thú nuôi, "
            }else{
                viewAddPet.txtNamePets.layer.borderWidth = 0
            }
            if viewAddPet.txtSex.text?.count == 0{
                viewAddPet.txtSex.layer.borderWidth = 1
                viewAddPet.txtSex.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền giới tính, "
            }else{
                viewAddPet.txtSex.layer.borderWidth = 0
            }
            if imgData == nil {
                err += "chưa điền ảnh giấy khai sinh, "
            }
            if viewAddPet.txtBreed.text?.count == 0{
                viewAddPet.txtBreed.layer.borderWidth = 1
                viewAddPet.txtBreed.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền giống thú nuôi. "
            }else{
                viewAddPet.txtBreed.layer.borderWidth = 0
            }
            showAlert(title: "Thông báo", message: err)
        }
    }
    
    @objc func clickShowMoreImg(_ sender:UIButton) {
        let picker = UIImagePickerController()
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.editedImage] as? UIImage else {return}
        viewAddPet.imgBirthCerti.image = image
        dismiss(animated: true)
    }
}
