//
//  AddCustomerViewController.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 04/05/2022.
//
//
import UIKit

class AddCustomerViewController: UIViewController {
    let viewAddCustomer = UINib(nibName: "AddCustomer", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! AddCustomer
    lazy var customerController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomerViewController") as! CustomerViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        viewAddCustomer.root = self
        viewAddCustomer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        configBackButtonNavBar()
        title = "Thêm khách hàng"
        self.view.addSubview(viewAddCustomer)
        viewAddCustomer.btnAdd.addTarget(self, action: #selector(clickShowMore(_:)), for: .touchUpInside)
    }
    
    @objc func clickShowMore(_ sender:UIButton) {
        let textName = viewAddCustomer.txtNameCustomer.text
        let textAddress = viewAddCustomer.txtAddress.text
        let textPhoneNumber = viewAddCustomer.txtNumberPhone.text
        let textEmail = viewAddCustomer.txtEmail.text
        let textType = viewAddCustomer.txtType.text
        let textBranch = viewAddCustomer.txtBranch.text
        if textName?.count != 0 && textAddress?.count != 0 && textPhoneNumber?.count != 0 && textEmail?.count != 0 && textType?.count != 0 && textBranch?.count != 0 && (textPhoneNumber!.count >= 9 && textPhoneNumber?.count ?? 0 <= 11)  && (validation().isValidEmail(emailID: textEmail ?? "") == true){
            var idType : Int
            let arr = viewAddCustomer.txtBranch.text?.components(separatedBy: " ")
            if textType == "normal" {
                idType = 1
            }else {
                idType = 2
            }
            commomSevice().postApiCustomer(name: textName!, address: textAddress!, phoneNumber: textPhoneNumber!, email: textEmail!, type: idType, idBranch: arr![(arr?.count ?? 0) - 1])
            self.navigationController?.popViewController(animated: true)
        }else{
            var err : String = ""
            if textName?.count == 0{
                viewAddCustomer.txtNameCustomer.layer.borderWidth = 1
                viewAddCustomer.txtNameCustomer.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền tên khách hàng, "
            }else{
                viewAddCustomer.txtNameCustomer.layer.borderWidth = 0
            }
            if textAddress?.count == 0{
                viewAddCustomer.txtAddress.layer.borderWidth = 1
                viewAddCustomer.txtAddress.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền địa chỉ, "
            }else{
                viewAddCustomer.txtAddress.layer.borderWidth = 0
            }
            if textPhoneNumber?.count == 0{
                viewAddCustomer.txtNumberPhone.layer.borderWidth = 1
                viewAddCustomer.txtNumberPhone.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền số điện thoại, "
            }else if !(textPhoneNumber!.count >= 9 && textPhoneNumber?.count ?? 0 <= 11){
                viewAddCustomer.txtNumberPhone.layer.borderWidth = 1
                viewAddCustomer.txtNumberPhone.layer.borderColor = UIColor.red.cgColor
                err += "số điện thoại phải đủ 9 - 11 số , "
            }else{
                viewAddCustomer.txtNumberPhone.layer.borderWidth = 0
            }
            if textEmail?.count == 0{
                viewAddCustomer.txtEmail.layer.borderWidth = 1
                viewAddCustomer.txtEmail.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền email, "
            } else if validation().isValidEmail(emailID: textEmail ?? "") == false{
                viewAddCustomer.txtEmail.layer.borderWidth = 1
                viewAddCustomer.txtEmail.layer.borderColor = UIColor.red.cgColor
                err += "email của bạn chưa đúng, "
            }else{
                viewAddCustomer.txtEmail.layer.borderWidth = 0
            }
            if textType?.count == 0{
                viewAddCustomer.txtType.layer.borderWidth = 1
                viewAddCustomer.txtType.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền loại khách hàng, "
            }else{
                viewAddCustomer.txtType.layer.borderWidth = 0
            }
            if textBranch?.count == 0{
                viewAddCustomer.txtBranch.layer.borderWidth = 1
                viewAddCustomer.txtBranch.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền chi nhánh khách hàng. "
            }else{
                viewAddCustomer.txtBranch.layer.borderWidth = 0
            }
            showAlert(title: "Thông báo", message: err)
        }
    }
}
