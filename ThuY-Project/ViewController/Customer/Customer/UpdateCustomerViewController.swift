//
//  UpdateCustomerViewController.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 03/05/2022.
//

import UIKit
//
class UpdateCustomerViewController: UIViewController {
    var obj: CustomerDataModel?
    let viewUpdateCustomer = UINib(nibName: "UpdateCustomer", bundle: nil).instantiate(withOwner: UpdateCustomerViewController.self, options: nil)[0] as! UpdateCustomer
    static var numberPet :Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewUpdateCustomer.root = self
        if CustomerViewController.roleCustomer == "DOC"{
            viewUpdateCustomer.btnCancel.isHidden = true
            viewUpdateCustomer.btnCancel.isHidden = true
        }else{
            viewUpdateCustomer.btnCancel.isHidden = false
            viewUpdateCustomer.btnCancel.isHidden = false
        }
        UpdateCustomerViewController.numberPet = obj?.pets.count
        configBackButtonNavBar()
        title = "Cập nhập khách hàng"
        viewUpdateCustomer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        self.view.addSubview(viewUpdateCustomer)
        viewUpdateCustomer.setData(obj: obj!)
        viewUpdateCustomer.btnUpdate.addTarget(self, action: #selector(clickShowMore(_:)), for: .touchUpInside)
        viewUpdateCustomer.btnCancel.addTarget(self, action: #selector(clickDelete(_:)), for: .touchUpInside)
    }
    
    @objc func clickDelete(_ sender:UIButton){
        if UpdateCustomerViewController.numberPet ?? 0 == 0{
            commomSevice().deleteApiCustomer(id: self.obj?.id ?? "")
            self.navigationController?.popViewController(animated: true)
            self.navigationController?.popViewController(animated: true)
        }else{
            showAlert(title: "Thông báo", message: "Khách hàng vẫn còn thú nuôi không thể xoá!")
        }
            
    }
    
    @objc func clickShowMore(_ sender:UIButton) {
        if viewUpdateCustomer.txtNameCustomer?.text?.count != 0 && viewUpdateCustomer.txtAddress.text?.count != 0 && viewUpdateCustomer.txtNumberPhone.text?.count != 0 && viewUpdateCustomer.txtEmail.text?.count != 0 && viewUpdateCustomer.txtType.text?.count != 0 && (viewUpdateCustomer.txtNumberPhone!.text?.count ?? 0 >= 9 && viewUpdateCustomer.txtNumberPhone?.text?.count ?? 0 <= 11) &&  (validation().isValidEmail(emailID: viewUpdateCustomer.txtEmail.text ?? "") == true){
            var idType : Int
            let arr = viewUpdateCustomer.txtBranch.text?.components(separatedBy: " ")
            if viewUpdateCustomer.txtType!.text == "normal" {
                idType = 1
            }else {
                idType = 2
            }
            commomSevice().putApiCustomer(idCustomer: self.obj?.id ?? "", name: viewUpdateCustomer.txtNameCustomer.text!, address: viewUpdateCustomer.txtAddress.text!, phoneNumber: viewUpdateCustomer.txtNumberPhone.text!, email: viewUpdateCustomer.txtEmail.text!, type: idType, idBranch: arr![arr!.count - 1])
            self.navigationController?.popViewController(animated: true)
        }else{
            var err : String = ""
            if viewUpdateCustomer.txtNameCustomer?.text?.count == 0{
                viewUpdateCustomer.txtNameCustomer.layer.borderWidth = 1
                viewUpdateCustomer.txtNameCustomer.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền tên khách hàng, "
            }else{
                viewUpdateCustomer.txtNameCustomer.layer.borderWidth = 0
            }
            if viewUpdateCustomer.txtAddress.text?.count == 0{
                viewUpdateCustomer.txtAddress.layer.borderWidth = 1
                viewUpdateCustomer.txtAddress.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền địa chỉ, "
            }else{
                viewUpdateCustomer.txtAddress.layer.borderWidth = 0
            }
            if viewUpdateCustomer.txtNumberPhone.text?.count == 0{
                viewUpdateCustomer.txtNumberPhone.layer.borderWidth = 1
                viewUpdateCustomer.txtNumberPhone.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền số điện thoại, "
            }else if !(viewUpdateCustomer.txtNumberPhone.text?.count ?? 0 >= 9 && viewUpdateCustomer.txtNumberPhone.text?.count ?? 0 <= 11){
                viewUpdateCustomer.txtNumberPhone.layer.borderWidth = 1
                viewUpdateCustomer.txtNumberPhone.layer.borderColor = UIColor.red.cgColor
                err += "số điện thoại phải đủ 9 - 11 số , "
            }else{
                viewUpdateCustomer.txtNumberPhone.layer.borderWidth = 0
            }
            if viewUpdateCustomer.txtEmail.text?.count == 0{
                viewUpdateCustomer.txtEmail.layer.borderWidth = 1
                viewUpdateCustomer.txtEmail.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền email, "
            } else if validation().isValidEmail(emailID: viewUpdateCustomer.txtEmail.text ?? "") == false{
                viewUpdateCustomer.txtEmail.layer.borderWidth = 1
                viewUpdateCustomer.txtEmail.layer.borderColor = UIColor.red.cgColor
                err += "email của bạn chưa đúng, "
            }else{
                viewUpdateCustomer.txtEmail.layer.borderWidth = 0
            }
            if viewUpdateCustomer.txtType.text?.count == 0{
                viewUpdateCustomer.txtType.layer.borderWidth = 1
                viewUpdateCustomer.txtType.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền loại khách hàng, "
            }else{
                viewUpdateCustomer.txtType.layer.borderWidth = 0
            }
            if viewUpdateCustomer.txtBranch.text?.count == 0{
                viewUpdateCustomer.txtBranch.layer.borderWidth = 1
                viewUpdateCustomer.txtBranch.layer.borderColor = UIColor.red.cgColor
                err += "chưa điền chi nhánh khách hàng. "
            }else{
                viewUpdateCustomer.txtBranch.layer.borderWidth = 0
            }
            showAlert(title: "Thông báo", message: err)
        }
    }

}
