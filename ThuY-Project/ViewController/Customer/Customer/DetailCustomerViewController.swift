//
//  DetailCustomerViewController.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 18/05/2022.
//

import UIKit
//
class DetailCustomerViewController: ViewControllerCustom, UITableViewDelegate, UITableViewDataSource{
    var idCustomer: String?
    var obj: CustomerDataModel!
    let refreshControl = UIRefreshControl()
    let viewDetailCustomer = UINib(nibName: "DetailCustomer", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! DetailCustomer
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDetailCustomer.tbView.delegate = self
        viewDetailCustomer.tbView.dataSource = self
        viewDetailCustomer.tbView.register(UINib(nibName: "PetCustomerTableViewCell", bundle: nil), forCellReuseIdentifier: "PetCustomerTableViewCell")
        viewDetailCustomer.root = self
        viewDetailCustomer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        configBackButtonNavBar()
        title = "Thông tin khách hàng"
        if CustomerViewController.roleCustomer == "DOC"{
            viewDetailCustomer.btnUpdate.isHidden = true
            viewDetailCustomer.btnAddPet.isHidden = true
            viewDetailCustomer.btnUpdateCustomer.isHidden = true
        }else{
            viewDetailCustomer.btnAddPet.isHidden = false
            viewDetailCustomer.btnUpdate.isHidden = false
            viewDetailCustomer.btnUpdateCustomer.isHidden = false
        }
        self.view.addSubview(viewDetailCustomer)
        refreshControl.addTarget(self, action: #selector(updateData), for: .valueChanged)
        viewDetailCustomer.tbView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showLoadingView()
        getApiCustomerDataModel(id: self.idCustomer ?? "")
        viewDetailCustomer.obj = self.obj
        viewDetailCustomer.idCustomer = self.idCustomer
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getApiCustomerDataModel(id: self.idCustomer ?? "")
        viewDetailCustomer.obj = self.obj
        viewDetailCustomer.idCustomer = self.idCustomer
    }
    
    @objc func updateData(_ sender: AnyObject) {
        getApiCustomerDataModel(id: self.idCustomer ?? "")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return obj?.pets.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PetCustomerTableViewCell", for: indexPath) as! PetCustomerTableViewCell
        if let objData = self.obj?.pets[indexPath.row]{
            cell.setData(obj: objData)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let obj = obj?.pets[indexPath.row]{
            HandlePush().gotoUpdatePetsViewController(root: self, obj: obj)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func getApiCustomerDataModel(id: String){
        commomSevice().getApiCustomerDetail(id:id) { response in
            if let obj = CustomerModel.init(JSON: response){
                if obj.data.count == 1 {
                    self.obj = obj.data[0]
                    if let objData = self.obj {
                        self.viewDetailCustomer.setData(obj: objData)
                    }
                }
                self.viewDetailCustomer.tbView.reloadData()
            }
            self.refreshControl.endRefreshing()
            self.dismissLoadingView()
        } failure: { err in
            
        }

    }
    
}
