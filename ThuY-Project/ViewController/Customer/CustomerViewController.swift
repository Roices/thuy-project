//
//  CustomerViewController.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 03/05/2022.
//
//
import UIKit
//
class CustomerViewController: ViewControllerCustom, UITableViewDataSource, UITableViewDelegate {
    static var type: customer = .customers
    var objCustomerData: CustomerModel?
    var objPetData: PetModel?
    var objBreed: BreedModel?
    var objBranch: BranchesModel?
    static var roleCustomer: String?
    static var arrBranch: [String] = []
    let refreshControl = UIRefreshControl()
    let viewCustomer = UINib(nibName: "Customer", bundle: nil).instantiate(withOwner: CustomerViewController.self, options: nil)[0] as! CustomerView
    override func viewDidLoad() {
        super.viewDidLoad()
        viewCustomer.tbView.dataSource = self
        viewCustomer.tbView.delegate = self
        guard let role = UserDefaults.standard.string(forKey: "role") else { return }
        CustomerViewController.roleCustomer = role
        viewCustomer.tbView.register(UINib(nibName: "CustomerTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomerTableViewCell")
        viewCustomer.tbView.register(UINib(nibName: "PetsTableViewCell", bundle: nil), forCellReuseIdentifier: "PetsTableViewCell")
        viewCustomer.root = self
        viewCustomer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        configBackButtonNavBar()
        title = "Chủ nuôi & Vật nuôi"
        
        self.view.addSubview(viewCustomer)
        viewCustomer.btnSeach.addTarget(self, action:#selector(clickShowMore(_:)), for: .touchUpInside)
        viewCustomer.btnSeach.tag = 0
        getBreedDataModel(page: 1, size: 20)
        getApiBranchDataModel(page: 1, size: 30)
        refreshControl.addTarget(self, action: #selector(updateData), for: .valueChanged)
        viewCustomer.tbView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showLoadingView()
        getPetDataModel(page: 1, size: 100)
        getCustomerDataModel(page: 1, size: 100)
//        viewCustomer.tbView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getPetDataModel(page: 1, size: 100)
        getCustomerDataModel(page: 1, size: 100)
    }
    
    @objc func updateData(_ sender: AnyObject) {
        getPetDataModel(page: 1, size: 100)
        getCustomerDataModel(page: 1, size: 100)
    }
    
    @objc func clickShowMore(_ sender:UIButton) {
        let textSearch = viewCustomer.txtSearch.text
        
        if sender.tag == 0{
            if CustomerViewController.type == .customers{
                getCustomerSearchDataModel(page: 1, size: 10, name: textSearch ?? "")
            }else{
                getPetsSearchDataModel(page: 1, size: 10, name: textSearch ?? "")
            }

        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if CustomerViewController.type == .customers {
            return self.objCustomerData?.data.count ?? 0
        }else{
            return self.objPetData?.data.count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if CustomerViewController.type == .customers {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerTableViewCell", for: indexPath) as! CustomerTableViewCell
            if let obj = self.objCustomerData?.data[indexPath.row]{
                cell.setData(obj: obj)
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PetsTableViewCell", for: indexPath) as! PetsTableViewCell
            if let obj = self.objPetData?.data[indexPath.row]{
                cell.setData(obj: obj)
            }
            return cell
        }
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if CustomerViewController.type == .customers {
            if let obj = objCustomerData {
                HandlePush().gotoDetailCustomerViewController(root: self,id: obj.data[indexPath.row].id, obj: obj.data[indexPath.row])
            }
        }else{
            if let obj = objPetData?.data[indexPath.row]{
                HandlePush().gotoUpdatePetsViewController(root: self, obj: obj)
            }
            
        }
       
    }
    
    func getPetDataModel(page:Int, size: Int){
        let commonService = commomSevice()
        commonService.getApiPets(page: "\(page)", size: "\(size)"){ response in
            if let obj = PetModel.init(JSON: response){
                self.objPetData = obj
                self.viewCustomer.tbView.reloadData()
            }
            self.refreshControl.endRefreshing()
            self.dismissLoadingView()
        } failure: { err in

        }
    }
    
    func arrStringBreed(objData: BreedModel){
        for i in objData.data {
            SpeciesViewController.arrBreed.append("\(i.name) - \(i.id)")
        }
    }
    
    func getCustomerDataModel(page:Int, size: Int){
//        showLoadingView()
        let commonService = commomSevice()
        commonService.getApiCustome(page: "\(page)", size: "\(size)"){ response in
            if let obj = CustomerModel.init(JSON: response){
                self.objCustomerData = obj
                self.viewCustomer.tbView.reloadData()
            }
            self.dismissLoadingView()
        } failure: { err in

        }
    }
    
    func getCustomerSearchDataModel(page:Int, size: Int, name: String){
//        showLoadingView()
        let commonService = commomSevice()
        commonService.getApiCustomerSearch(page: "\(page)", size: "\(size)", name: name) {response in
                if let obj = CustomerModel.init(JSON: response){
                    self.objCustomerData = obj
                    self.viewCustomer.tbView.reloadData()
                }
//            self.dismissLoadingView()
        }failure: { err in
            
        }
    }
    
    func getPetsSearchDataModel(page:Int, size: Int, name: String){
        let commonService = commomSevice()
        commonService.getApiPetSearch(page: "\(page)", size: "\(size)", name: name) {response in
                if let obj = PetModel.init(JSON: response){
                    self.objPetData = obj
                    self.viewCustomer.tbView.reloadData()
                }
        }failure: { err in
            
        }
    }
    
    
    func arrStringBranch(objData: BranchesModel){
        CustomerViewController.arrBranch = []
        for i in objData.data {
            CustomerViewController.arrBranch.append("\(i.name) - \(i.id)")
        }
    
    }

    func getApiBranchDataModel(page: Int, size: Int){
        commomSevice().getApiBranches(page: "\(page)", size: "\(size)") { response in
            if let obj = BranchesModel.init(JSON: response){
                self.objBranch = obj
                self.arrStringBranch(objData: obj)
            }
        } failure: { err in
        }
    }
    
    func getBreedDataModel(page:Int, size: Int){
        let commonService = commomSevice()
        commonService.getApiBreed(page: "\(page)", size: "\(size)"){ response in
            if let obj = BreedModel.init(JSON: response){
                self.objBreed = obj
                self.arrStringBreed(objData: self.objBreed!)
            }
        } failure: { err in

        }
    }
}
