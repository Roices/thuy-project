//
//  MainTabViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 27/04/2022.
//

import UIKit

class MainTabViewController: UITabBarController {

    var containerView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setup()
        layout()
        setupViews()
    }
    
    @objc func didWaitingViewTapped(_ sender: UITapGestureRecognizer){
        NotificationCenter.default.post(name: .sideMenu, object: nil)
    }
}

extension MainTabViewController {
    func setup(){
        view.backgroundColor = .white
        tabBar.tintColor = UIColor.greenTY
        tabBar.isTranslucent = false
    }
    
    func layout(){
        
    }
    
    func setupViews(){
        //initialize tab bar elements
        let homeVC = HomeViewController()
        homeVC.delegate = self
        
        let staffVC = StaffViewController()
        let bookingVC = BookingOverViewController()
        let notificationVC = NotificationViewController()
        
        //setupVCs
        homeVC.setTabBarImage(image: Image.home, title: "Trang chủ", tag: 0)
        staffVC.setTabBarImage(image: Image.people, title: "Nhân viên", tag: 1)
        bookingVC.setTabBarImage(image: Image.calendar, title: "Lịch khám", tag: 2)
        notificationVC.setTabBarImage(image: Image.bell, title: "Thông báo", tag: 3)
        
        //initialize navigation for each VC
        let homeNC = UINavigationController(rootViewController: homeVC)
        let staffNC = UINavigationController(rootViewController: staffVC)
        let bookingNC = UINavigationController(rootViewController: bookingVC)
        let notificationNC = UINavigationController(rootViewController: notificationVC)
        
        //add NCs to tabbar
        let tabBarLists = [homeNC, staffNC, bookingNC, notificationNC]
        viewControllers = tabBarLists
        
    }
    
    func showWaitingView(){
        containerView = UIView(frame: view.bounds)
        view.addSubview(containerView)
        
        addGestureWaitingView()
        
        containerView.backgroundColor = .black
        containerView.alpha = 0
        
        UIView.animate(withDuration: 0.25) {
            self.containerView.alpha = 0.8
        }
    }
    
    func dismissWaitingView(){
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.25) {
                self.containerView.alpha = 0
            } completion: { done in
                if done{
                    self.containerView.removeFromSuperview()
                    self.containerView = nil
                }
            }
        }
    }
    func addGestureWaitingView(){
        let gesture = UITapGestureRecognizer(target: self, action: #selector(didWaitingViewTapped(_:)))
        containerView.isUserInteractionEnabled = true
        containerView.addGestureRecognizer(gesture)
    }
}

extension MainTabViewController: HomeViewControllerDelegate {
    func didStaffVCTapped() {
        selectedIndex = 1
    }
    
}


