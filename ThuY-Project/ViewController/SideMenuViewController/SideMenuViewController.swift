//
//  SideMenuViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 29/04/2022.
//

import UIKit
protocol SideMenuViewControllerDelegate: AnyObject {
    func didSideRowTapped(_ selection: Overview)
    func didUserInfor(model: LoginModel)
    func didLogout()
}
class SideMenuViewController: UIViewController {
    // MARK: - Subview
    private let exitButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        let configuration = UIImage.SymbolConfiguration(pointSize: 32, weight: .medium)
        let image = Image.multiply.withConfiguration(configuration)
        button.setImage(image, for: .normal)
        button.tintColor = .white
        button.addTarget(self, action: #selector(didExitButtonTapped(_:)), for: .touchUpInside)
        return button
    }()
    
    private let titleLabel = LabelCustom(color: .white, fontFamily: UIFont.boldSystemFont(ofSize: 24), alignment: .left)
    
    
    let footerView = FooterSideMenuView()
    
    private let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    // MARK: - Properties
    lazy var menuPadding = view.frame.size.width - ContainerViewController.slideMenuPadding
    weak var delegate: SideMenuViewControllerDelegate?
    
    // Information to display
    let rows: [Overview] = [Overview.branch, Overview.owner, Overview.staff, Overview.booking]

    
    var trailingPadding: CGFloat! // calculate trailing padding for side menu
    
    var userInformation: LoginModel!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        layout()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let data = UserDefaults.standard.object(forKey: "user") as? Data {
            let decoder = JSONDecoder()
            if let infor = try? decoder.decode(LoginModel.self, from: data) {
                userInformation = infor
                footerView.bindingData(model: userInformation)
            }
        }
    }
    
    // MARK: - Selector
    @objc func didExitButtonTapped(_ sender: UIButton){
        // post to exit from side menu
        NotificationCenter.default.post(name: .sideMenu, object: nil)
    }
    

    
    // MARK: - API
    
    // MARK: - Helper

}
// MARK: - Extension
extension SideMenuViewController {
    
    func setup(){
        view.backgroundColor = .greenTY
        
        titleLabel.text = "Phòng khám thú y"
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.8
        
        footerView.delegate = self
        configTableView()
       
        
        
        
    }
    func layout(){
        view.addSubview(exitButton)
        view.addSubview(titleLabel)
        view.addSubview(footerView)
        view.addSubview(tableView)
        
        trailingPadding = menuPadding + 8
        
        //exitButton
        NSLayoutConstraint.activate([
            exitButton.topAnchor.constraint(equalToSystemSpacingBelow: view.safeAreaLayoutGuide.topAnchor, multiplier: 1),
            exitButton.leadingAnchor.constraint(equalToSystemSpacingAfter: view.leadingAnchor, multiplier: 2)
        ])
        
        //titleLabel
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalToSystemSpacingBelow: exitButton.bottomAnchor, multiplier: 2),
            titleLabel.leadingAnchor.constraint(equalTo: exitButton.leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -trailingPadding)
            
        ])
        
        //footerview
        NSLayoutConstraint.activate([
            footerView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -8),
            footerView.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            footerView.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor)
            
        ])
        
        //tableView
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalToSystemSpacingBelow: titleLabel.bottomAnchor, multiplier: 2),
            tableView.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: titleLabel.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: footerView.topAnchor, constant: -8)
        ])
    }
    
    func configTableView(){
        tableView.register(SideMenuTableCell.self, forCellReuseIdentifier: SideMenuTableCell.reuseIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = SideMenuTableCell.rowHeight
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        
        // dismiss empty rows
        let footerView = UIView()
        footerView.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        tableView.tableFooterView = footerView
    }
}

extension SideMenuViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // detext which is selected
        let selection = rows[indexPath.row]
        delegate?.didSideRowTapped(selection)
        
    }
}

extension SideMenuViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SideMenuTableCell.reuseIdentifier, for: indexPath) as! SideMenuTableCell
        
        //populate cell
        cell.imgView.image = rows[indexPath.row].image
        cell.titleLabel.text = rows[indexPath.row].title
        
        return cell
    }
    
    
}

extension SideMenuViewController: FooterSideMenuViewDelegate{
    func didLogoutButtonTapped() {
        delegate?.didLogout()
    }
    
    func didUserClicked() {
        delegate?.didUserInfor(model: userInformation)
    }
    
    
}
