//
//  LoginController.swift
//  ThuY-Project
//
//  Created by Tuan on 04/05/2022.
//

import Foundation
import UIKit
import Alamofire

class LoginController: ViewControllerCustom, AlertNoOptionViewControllerDelegate {

    let loginContentView = UINib(nibName: "LoginView", bundle: nil).instantiate(withOwner: LoginController.self, options: nil)[0] as! LoginView
    
    lazy var homeView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ContainerViewController") as! ContainerViewController
    
    lazy var bookingView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookingOverViewController") as! BookingOverViewController
    
    lazy var forgotPasswordController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ForgotPasswordController") as! ForgotPasswordController
    
    lazy var url = "\(BaseURL.loginURL)"
    
    var login: LoginModel? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

    }
    
    func loginAPI(staff: String, pass: String) {
        
        let param = ["username": staff, "password": pass]
        
        showLoadingView()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { 
       
        }
        
        APIService.shared.requestWith(url: url,
                                      method: .post,
                                      parameters: param,
                                      objectTyple: LoginModel.self,
                                      headers: nil,
                                      encoding: URLEncoding.default) { [self] success, data, status in
            
            guard let status = status, let data = data else { return }
           
            dismissLoadingView()
            
            if status != 200 {
                
                presentAlertNoOptionOnMainThread(content: "Sai mã nhân viên hoặc mật khẩu") { vc in
                    vc.delegate = self
                }
                
            } else {
                
            guard let model = data as? LoginModel else { return }

            UserDefaults.standard.set(model.access_token, forKey:"access_token")
            UserDefaults.standard.set(model.user.name, forKey:"name")
            UserDefaults.standard.set(model.refresh_token, forKey: "refresh_token")
            UserDefaults.standard.set(model.user.id, forKey: "userID")
            UserDefaults.standard.set(model.user.authorities[0].authority, forKey: "role")
            UserDefaults.standard.set(model.user.branch.id, forKey: "branchID")
            UserDefaults.standard.set(model.user.branch.name, forKey: "branchName")
            
            let encoder = JSONEncoder()
            if let encoded = try? encoder.encode(model) {
                    let defaults = UserDefaults.standard
                    defaults.set(encoded, forKey: "user")
                }
            
                pushNotiTokenToServer()
                navigationController?.pushViewController(homeView, animated: true)
            
            
            }
            
            
        }
    }
    
    
    func pushNotiTokenToServer() {
        
        let url = "\(BaseURL.pushTokenToServerURL)"
        
        let header = APIService.shared.getHeader()
        
        guard let fcmToken = UserDefaults.standard.string(forKey: "FCM_token"), let userID = UserDefaults.standard.string(forKey: "userID") else { return }
        
        let param = ["userId": userID,
                     "token": fcmToken]
     
        
        APIService.shared.requestWith(url: url,
                                      method: .post,
                                      parameters: param,
                                      objectTyple: validateError.self,
                                      headers: header,
                                      encoding: JSONEncoding.default) { isSuccess, json, statusCode in
            
            guard let data = json as? validateError else { return }
            print(data)
            
        }
    }
    
}


// MARK: - Extension
extension LoginController: LoginViewDelegate {

    func didOKButtonTapped() {
        self.dismiss(animated: true)
    }

    func forgotPassword() {
        
        forgotPasswordController.setTfEmpty()
        
        present(forgotPasswordController, animated: true)
        
    }
    
    func loginToApp(username: String, password: String) {
        
        loginContentView.staffID.text = ""
        loginContentView.password.text = ""
        
        loginAPI(staff: username, pass: password)

    }
    
    func setup() {

        loginContentView.frame = CGRect(x: 0,
                                        y: 0,
                                        width: UIScreen.main.bounds.width,
                                        height: UIScreen.main.bounds.height)
        
        self.navigationController?.isNavigationBarHidden = true
        
        loginContentView.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        
        self.view.addSubview(loginContentView)
    }
    
}


