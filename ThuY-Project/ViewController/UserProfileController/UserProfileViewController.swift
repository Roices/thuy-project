//
//  UserProfileViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 25/05/2022.
//

import UIKit

class UserProfileViewController: UIViewController {
    // MARK: - Subview
    private let avatarView = UserAvatarView()
    
    
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        return scrollView
    }()
    
    private let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
        
    }()
    
    private let usernameFieldView = UserInformationView(title: "Username", content: "thoapro")
    private let phoneNumberFieldView = UserInformationView(title: "Số điện thoại", content: "0976870299")
    private let identityCardFieldView = UserInformationView(title: "CMT/CCCD", content: "122376531")
    private let birthdayFieldView = UserInformationView(title: "Ngày/Tháng/Năm sinh", content: "03/07/2000")
    private let addressFieldView = UserInformationView(title: "Địa chỉ", content: "Xã Tân Thới Nhì, Huyện Hóc Môn, Hồ Chí Minh (tphcm)")
    
    private let idFieldView = UserInformationView(title: "ID", content: "12132112")
    private let authorityFieldView = UserInformationView(title: "Vị trí", content: "EMP")
    
    private let changePassword: UIButton = {
        let button = UIButton(type: .custom)
        button.setTitle("Đặt lại mật khẩu", for: .normal)
        button.backgroundColor = .greenTY
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 15
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return button
    }()
    
    private let footerView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.heightAnchor.constraint(equalToConstant: 15).isActive = true
        return view
    }()
    
    // MARK: - Properties
    private let padding: CGFloat = 16
//    private let radius: CGFloat = 68
    
    var user: LoginModel!
    
    // MARK: - Lifecycle
    init(user: LoginModel){
        super.init(nibName: nil, bundle: nil)
        self.user = user
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        layout()
    }
    
    // MARK: - Selector
    @objc func resetPasswordButtonTapped(_ sender: UIButton){
        let vc = ResetPasswordViewController(user: user)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension
extension UserProfileViewController {
    
    func setup(){
        view.backgroundColor = .lightSecondaryGrayTY
        scrollView.showsVerticalScrollIndicator = false
        
        configNavBar()

        changePassword.addTarget(self, action: #selector(resetPasswordButtonTapped(_:)), for: .touchUpInside)
        idFieldView.contentLabel.text = user.user.id
        usernameFieldView.contentLabel.text = user.user.username
        phoneNumberFieldView.contentLabel.text = user.user.phoneNumber
        identityCardFieldView.contentLabel.text = user.user.identityCard
        birthdayFieldView.contentLabel.text = user.user.birth
        addressFieldView.contentLabel.text = user.user.address
        authorityFieldView.contentLabel.text = user.user.authorities[0].authority
        
        avatarView.nameLabel.text = user.user.name
        avatarView.characterLabel.text = String(user.user.name.prefix(1)).uppercased()
        
    }
    func layout(){
        view.addSubview(avatarView)
        view.addSubview(scrollView)
        
        scrollView.addSubview(stackView)
        stackView.addArrangedSubview(idFieldView)
        stackView.addArrangedSubview(usernameFieldView)
        stackView.addArrangedSubview(phoneNumberFieldView)
        stackView.addArrangedSubview(identityCardFieldView)
        stackView.addArrangedSubview(birthdayFieldView)
        stackView.addArrangedSubview(addressFieldView)
        stackView.addArrangedSubview(authorityFieldView)
        
        stackView.addArrangedSubview(changePassword)
        stackView.addArrangedSubview(footerView)
        
        
        NSLayoutConstraint.activate([
            avatarView.topAnchor.constraint(equalToSystemSpacingBelow: view.safeAreaLayoutGuide.topAnchor, multiplier: 1),
            avatarView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding),
            avatarView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding)

        ])
    
//
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalToSystemSpacingBelow: avatarView.bottomAnchor, multiplier: 2),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)

        ])
        
        //stackView
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
        
        
    }
    
    func configNavBar(){
        title = "Hồ sơ"
        configBackButtonNavBar()
    }
}
