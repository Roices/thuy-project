//
//  RestPasswordViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 25/05/2022.
//

import UIKit

class ResetPasswordViewController: UIViewController {
    // MARK: - Subview
    private let currentPasswordView = BranchTextFieldView(label: "Mật khẩu hiện tại")
    
    private let newPasswordView = BranchTextFieldView(label: "Mật khẩu mới")
    
    private let confirmPasswordView = BranchTextFieldView(label: "Xác nhận mật khẩu")
    
    private let stackView: UIStackView = {
       let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 12
        return stackView
    }()
    
    private let confirmButton: UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = 10
        button.backgroundColor = .greenTY
        button.setTitle("Xác nhận", for: .normal)
        button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        return button
        
    }()
    
    
    
    // MARK: - Properties
    var newPasswordText: String = ""
    var confirmPasswordText: String = ""
    var oldPasswordText: String = ""
    
    var user: LoginModel!
    
    var isSuccess: Bool = false
    // MARK: - Lifecycle
    
    
    init(user: LoginModel){
        super.init(nibName: nil, bundle: nil)
        self.user = user
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        layout()
        setupDismissKeyboard()
    }
    
    // MARK: - Selector
    @objc func didConfirmButtonTapped(_ sender: UIButton){
        
        guard let token = UserDefaults.standard.string(forKey: "access_token") else {return}
                
        if newPasswordText != confirmPasswordText {

            self.presentAlertNoOptionOnMainThread(content: "Mật khẩu nhập lại không đúng!") { [weak self] vc in
                vc.delegate = self
            }
        }else{
            NetworkManager.shared.resetPassword(token: token, oldPassword: oldPasswordText, newPassword: newPasswordText, userName: self.user.user.username) { [weak self] result in
                guard let self = self else {return}
                switch result {
                
                case .success(let message):
                    switch message.status {
                    case 403:
                        //do st
                        self.presentAlertNoOptionOnMainThread(content: message.message) { [weak self] vc in
                            vc.delegate = self
                        }
                    case 400:
                        
                        self.presentAlertNoOptionOnMainThread(content: message.message) { [weak self] vc in
                            vc.delegate = self
                        }
                    case 200:
                        //success
                        self.presentAlertNoOptionOnMainThread(content: "Thành công") { [weak self] vc in
                            guard let self = self else {return}
                            self.isSuccess = true
                            vc.delegate = self
                        }
                        
                        
                    case 304:
                        self.presentAlertNoOptionOnMainThread(content: message.message) { [weak self] vc in
                            vc.delegate = self
                        }
                    default:
                        break
                    }
                case .failure(let error):
                    print("DEBUG: \(error)")
                }
            }
        }
    }
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension
extension ResetPasswordViewController {
    
    func setup(){
//        currentPasswordView.textView
        view.backgroundColor = .lightSecondaryGrayTY
        
        configBackButtonNavBar()
        title = "Đặt lại mật khẩu"
        
        currentPasswordView.delegate = self
        newPasswordView.delegate = self
        confirmPasswordView.delegate = self
        
        confirmButton.addTarget(self, action: #selector(didConfirmButtonTapped(_:)), for: .touchUpInside)
    }
    func layout(){
        view.addSubview(stackView)
        stackView.addArrangedSubview(currentPasswordView)
        stackView.addArrangedSubview(newPasswordView)
        stackView.addArrangedSubview(confirmPasswordView)
        stackView.addArrangedSubview(confirmButton)
        
        //stackView
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalToSystemSpacingBelow: view.safeAreaLayoutGuide.topAnchor, multiplier: 1),
            stackView.leadingAnchor.constraint(equalToSystemSpacingAfter: view.leadingAnchor, multiplier: 2),
            view.trailingAnchor.constraint(equalToSystemSpacingAfter: stackView.trailingAnchor, multiplier: 2)
        
        ])
        
        currentPasswordView.setContentHuggingPriority(.defaultHigh, for: .vertical)
        newPasswordView.setContentHuggingPriority(.defaultHigh, for: .vertical)
        confirmPasswordView.setContentHuggingPriority(.defaultHigh, for: .vertical)
    }
}

extension ResetPasswordViewController: BranchTextFieldViewDelegate {
    func didTextFieldChanged(view: BranchTextFieldView) {
        if view === currentPasswordView {
//            newPasswordText = ""
            guard let text = view.textField.text else {return}
            oldPasswordText = text
        }else if view === newPasswordView{
            guard let text = view.textField.text else {return}
            newPasswordText = text
        }else if view === confirmPasswordView {
            guard let text = view.textField.text else {return}
            confirmPasswordText = text
        }
    }
    
    func didWarningButtonTapped(view: BranchTextFieldView) {}
}

extension ResetPasswordViewController: AlertNoOptionViewControllerDelegate {
    func didOKButtonTapped() {
        if isSuccess {
            self.dismiss(animated: true, completion: nil)
            self.navigationController?.popToRootViewController(animated: true)
            
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
}
