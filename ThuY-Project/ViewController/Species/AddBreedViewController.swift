//
//  AddBreedViewController.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 29/04/2022.
//

import UIKit

class AddBreedViewController: UIViewController {

    var arrDropdown : [String]?
    let viewAddBreed = UINib(nibName: "AddGiong", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! AddGiong
    override func viewDidLoad() {
        super.viewDidLoad()
        viewAddBreed.root = self
        viewAddBreed.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        configBackButtonNavBar()
        title = "Thêm giống"
        viewAddBreed.txtNameSpecies.optionArray = arrDropdown!
        self.view.addSubview(viewAddBreed)
        viewAddBreed.btnAdd.addTarget(self, action:#selector(clickShowMore(_:)), for: .touchUpInside)
        viewAddBreed.btnAdd.tag = 0
    }
    
    @objc func clickShowMore(_ sender:UIButton) {
        let textNameBreed = viewAddBreed.txtNameBreed.text
        let textNote = viewAddBreed.txtNote.text
        let textNameSpecies = viewAddBreed.txtNameSpecies.text
        if textNameSpecies?.count != 0 && textNameBreed?.count != 0 {
            let arrId = textNameSpecies?.components(separatedBy: " ")
            if let arr = arrId {
                commomSevice().postApiBreed(name: textNameBreed!, note: textNote ?? "", id: arr[arr.count - 1])
            }
            self.navigationController?.popViewController(animated:true)
        }else{
            if textNameSpecies?.count == 0 {
                viewAddBreed.txtNameSpecies.layer.borderWidth = 1
                viewAddBreed.txtNameSpecies.layer.borderColor = UIColor.red.cgColor
            }else{
                viewAddBreed.txtNameSpecies.layer.borderWidth = 0
            }
            if textNameBreed?.count == 0{
                viewAddBreed.txtNameBreed.layer.borderWidth = 1
                viewAddBreed.txtNameBreed.layer.borderColor = UIColor.red.cgColor
            }else{
                viewAddBreed.txtNameBreed.layer.borderWidth = 0
            }
            showAlert(title: "Thông báo", message: "Điền thiếu thông tin!")
        }
    }


}
