//
//  UpdateBreedViewController.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 29/04/2022.
//

import UIKit

class UpdateBreedViewController: UIViewController {
    var obj : BreedDataModel?
    var id : String?
    var arrDropdown : [String]?
    let viewUpdateBreed = UINib(nibName: "UpdateBreed", bundle: nil).instantiate(withOwner: UpdateBreedViewController.self, options: nil)[0] as! UpdateBreed
    override func viewDidLoad() {
        super.viewDidLoad()
        viewUpdateBreed.root = self
        viewUpdateBreed.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        viewUpdateBreed.setData(obj: obj!)
        configBackButtonNavBar()
        title = "cập nhập giống"
        if SpeciesViewController.roleSpecies == "DOC"{
            viewUpdateBreed.btnUpdate.isHidden = true
            viewUpdateBreed.btnCancel.isHidden = true
        }else{
            viewUpdateBreed.btnUpdate.isHidden = false
            viewUpdateBreed.btnCancel.isHidden = false
        }
        self.view.addSubview(viewUpdateBreed)
        viewUpdateBreed.btnUpdate.addTarget(self, action:#selector(clickShowMore(_:)), for: .touchUpInside)
        viewUpdateBreed.id = self.id!
        viewUpdateBreed.btnUpdate.tag = 0
        viewUpdateBreed.txtNameSpecies.optionArray = arrDropdown!
    }
    
    @objc func clickShowMore(_ sender:UIButton) {
        let textNameBreed = viewUpdateBreed.txtNameBreed.text
        let textNote = viewUpdateBreed.txtNote.text
        let textNameSpecies = viewUpdateBreed.txtNameSpecies.text
        if textNameSpecies?.count != 0 && textNameBreed?.count != 0 {
            let arrId = textNameSpecies?.components(separatedBy: " ")
            if let arr = arrId {
                commomSevice().putApiBreed(id: self.id!, name: textNameBreed!, note: textNote ?? "", idSpecies: arr[arr.count - 1])
            }
            self.navigationController?.popViewController(animated:true)
        }else{
            if textNameSpecies?.count == 0 {
                viewUpdateBreed.txtNameSpecies.layer.borderWidth = 1
                viewUpdateBreed.txtNameSpecies.layer.borderColor = UIColor.red.cgColor
            }else{
                viewUpdateBreed.txtNameSpecies.layer.borderWidth = 1
                viewUpdateBreed.txtNameSpecies.layer.borderColor = UIColor.black.cgColor
            }
            if textNameBreed?.count == 0 {
                viewUpdateBreed.txtNameBreed.layer.borderWidth = 1
                viewUpdateBreed.txtNameBreed.layer.borderColor = UIColor.red.cgColor
            }else{
                viewUpdateBreed.txtNameBreed.layer.borderWidth = 1
                viewUpdateBreed.txtNameBreed.layer.borderColor = UIColor.black.cgColor
            }
            
            showAlert(title: "Thông báo", message: "Điền thiếu thông tin!")
            
        }
    }
    
}
