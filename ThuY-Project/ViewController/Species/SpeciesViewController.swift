//
//  SpeciesViewController.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 28/04/2022.
//
//
import UIKit

class SpeciesViewController: ViewControllerCustom, UITableViewDelegate, UITableViewDataSource {
    static var arrBreed: [String] = []
    static var type : species = .giong
    static var roleSpecies: String?
    var objSpecies: SpeciesModel?
    var objBreed: BreedModel?
    let viewSpecies = UINib(nibName: "Species", bundle: nil).instantiate(withOwner: SpeciesViewController.self, options: nil)[0] as! Species
    var arrSpecies: [String] = []
    let refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let role = UserDefaults.standard.string(forKey: "role") else { return }
        SpeciesViewController.roleSpecies = role
        viewSpecies.tbView.delegate = self
        viewSpecies.tbView.dataSource = self
        viewSpecies.tbView.register(UINib(nibName: "GiongTableViewCell", bundle: nil), forCellReuseIdentifier: "GiongTableViewCell")
        viewSpecies.tbView.register(UINib(nibName: "LoaiTableViewCell", bundle: nil), forCellReuseIdentifier: "LoaiTableViewCell")
        viewSpecies.root = self
        viewSpecies.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        if SpeciesViewController.roleSpecies == "DOC"{
            viewSpecies.btnAdd.isHidden = true
        }else{
            viewSpecies.btnAdd.isHidden = false
        }
        self.view.addSubview(viewSpecies)
        title = "Giống & Loài   "
        configBackButtonNavBar()
        viewSpecies.btnSearch.addTarget(self, action:#selector(clickShowMore(_:)), for: .touchUpInside)
        viewSpecies.btnSearch.tag = 0
        refreshControl.addTarget(self, action: #selector(updateData), for: .valueChanged)
        viewSpecies.tbView.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showLoadingView()
        getBreedDataModel(page: 1, size: 50)
        getSpeciesDataModel(page: 1, size: 50)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getBreedDataModel(page: 1, size: 50)
        getSpeciesDataModel(page: 1, size: 50)
    }
    
    @objc func updateData(_ sender: AnyObject) {
        getBreedDataModel(page: 1, size: 50)
        getSpeciesDataModel(page: 1, size: 50)
    }
    
    func testArr(objData: SpeciesModel) {
        arrSpecies = []
        for i in objData.data {
            arrSpecies.append("\(i.name) - \(i.id)")
        }
    }
    
    func arrStringBreed(objData: BreedModel){
        SpeciesViewController.arrBreed = []
        for i in objData.data {
            SpeciesViewController.arrBreed.append("\(i.name) - \(i.id)")
        }
    }
    
    @objc func clickShowMore(_ sender:UIButton) {
        let textSearch = viewSpecies.txtSearch.text
        if sender.tag == 0{
            if SpeciesViewController.type == .loai{
                getSpeciesSearchDataModel(page: 1, size: 10, name: textSearch ?? "")
            }else{
                getBreedSearchDataModel(page: 1, size: 10, name: textSearch ?? "")
            }

        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if SpeciesViewController.type == .giong{
            return self.objBreed?.data.count ?? 0
        }else {
            return self.objSpecies?.data.count ?? 0
            
        }
            
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if SpeciesViewController.type == .giong {
            let cell = tableView.dequeueReusableCell(withIdentifier: "GiongTableViewCell", for: indexPath) as! GiongTableViewCell
            if let obj = self.objBreed?.data[indexPath.row]{
                cell.setData(obj: obj)
            }
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoaiTableViewCell", for: indexPath) as! LoaiTableViewCell
            if let obj = self.objSpecies?.data[indexPath.row]{
                cell.setData(obj: obj)
            }
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if SpeciesViewController.type == .giong {
            if let obj = self.objBreed?.data[indexPath.row]{
                HandlePush().gotoUpdateBreedViewController(root: self, obj: obj,id: obj.id, arr: arrSpecies)
            }
        }else {
            if let obj = self.objSpecies?.data[indexPath.row]{
                HandlePush().gotoUpdateSpeciesViewController(root: self, obj: obj, id: obj.id)
            }
        }
  
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    
     func getSpeciesDataModel(page:Int, size: Int){
        let commonService = commomSevice()
        commonService.getApiSpecies(page: "\(page)", size: "\(size)"){ response in
            if let obj = SpeciesModel.init(JSON: response){
                self.objSpecies = obj
                self.viewSpecies.tbView.reloadData()
                self.testArr(objData: self.objSpecies!)
                self.viewSpecies.arr = self.arrSpecies
               
            }
            self.refreshControl.endRefreshing()
            self.dismissLoadingView()
        } failure: { err in

        }
    }
    
    func getBreedDataModel(page:Int, size: Int){
        let commonService = commomSevice()
        commonService.getApiBreed(page: "\(page)", size: "\(size)"){ response in
            if let obj = BreedModel.init(JSON: response){
                self.objBreed = obj
                self.viewSpecies.tbView.reloadData()
                if SpeciesViewController.arrBreed.count <= obj.data.count ?? 0 {
                    self.arrStringBreed(objData: self.objBreed!)
                }
                
            }
            self.dismissLoadingView()
        } failure: { err in

        }
    }
    
    func getBreedSearchDataModel(page:Int, size: Int, name: String){
        let commonService = commomSevice()
        commonService.getApiBreedSearch(page: "\(page)", size: "\(size)",name: name){ response in
            if let obj = BreedModel.init(JSON: response){
                self.objBreed = obj
                self.viewSpecies.tbView.reloadData()
            }
        } failure: { err in

        }
    }
    
    func getSpeciesSearchDataModel(page:Int, size: Int, name: String){
        let commonService = commomSevice()
        commonService.getApiSpeciesSearch(page: "\(page)", size: "\(size)",name: name){ response in
            if let obj = SpeciesModel.init(JSON: response){
                
                self.objSpecies = obj
                self.viewSpecies.tbView.reloadData()
            }
        } failure: { err in

        }
    }
}
