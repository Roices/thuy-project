//
//  UpdateSpeciesViewController.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 29/04/2022.
//

import UIKit

class UpdateSpeciesViewController: UIViewController {
    var obj : SpeciesDataModel?
    var id : String?
    let viewUpdateSpecies = UINib(nibName: "UpdateSpecies", bundle: nil).instantiate(withOwner: UpdateSpeciesViewController.self, options: nil)[0] as! UpdateSpecies
    override func viewDidLoad() {
        super.viewDidLoad()
        viewUpdateSpecies.root = self
        viewUpdateSpecies.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        viewUpdateSpecies.id = id
        if SpeciesViewController.roleSpecies == "DOC"{
            viewUpdateSpecies.btnUpdate.isHidden = true
            viewUpdateSpecies.btnCancel.isHidden = true
        }else{
            viewUpdateSpecies.btnUpdate.isHidden = false
            viewUpdateSpecies.btnCancel.isHidden = false
        }
        configBackButtonNavBar()
        title = "Cập nhập loài"
        self.view.addSubview(viewUpdateSpecies)
        viewUpdateSpecies.setData(obj: obj!)
        viewUpdateSpecies.btnUpdate.addTarget(self, action:#selector(clickShowMore(_:)), for: .touchUpInside)
        viewUpdateSpecies.btnCancel.addTarget(self, action:#selector(clickDelete(_:)), for: .touchUpInside)
    }
    
    
    @objc func clickDelete(_ sender:UIButton) {
            commomSevice().deleteApiSpecies(id: id!)
            self.navigationController?.popViewController(animated: true)
    }
    
    @objc func clickShowMore(_ sender:UIButton) {
        let textName = viewUpdateSpecies.txtNameSpecies.text
        let textNote = viewUpdateSpecies.txtNote.text
        if textName?.count != 0 {
            commomSevice().putApiSpecies(id: id!, name: textName!, note: textNote!)
            self.navigationController?.popViewController(animated:true)
        }else{
            viewUpdateSpecies.txtNameSpecies.layer.borderWidth = 1
            viewUpdateSpecies.txtNameSpecies.layer.borderColor = UIColor.red.cgColor
            showAlert(title: "Thông báo", message: "Điền thiếu thông tin!")
        }
    }
    
}
