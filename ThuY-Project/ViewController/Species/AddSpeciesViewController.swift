//
//  AddSpeciesViewController.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 29/04/2022.
//

import UIKit

class AddSpeciesViewController: UIViewController {
    
    let viewAddSpecies = UINib(nibName: "AddSpecies", bundle: nil).instantiate(withOwner: AddSpeciesViewController.self, options: nil)[0] as! AddSpecies
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewAddSpecies.root = self
        viewAddSpecies.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        configBackButtonNavBar()
        title = "Thêm Loài"
        self.view.addSubview(viewAddSpecies)
        viewAddSpecies.btnAdd.addTarget(self, action:#selector(clickShowMore(_:)), for: .touchUpInside)
        viewAddSpecies.btnAdd.tag = 0
    }
    
    @objc func clickShowMore(_ sender:UIButton) {
        let textName = viewAddSpecies.txtNameSpecies.text
        let textNote = viewAddSpecies.txtNote.text
        if textName?.count != 0 {
            commomSevice().postApiSpecies(name: textName ?? "", note: textNote ?? "")
            self.navigationController?.popViewController(animated:true)
        }else{
            viewAddSpecies.txtNameSpecies.layer.borderWidth = 1
            viewAddSpecies.txtNameSpecies.layer.borderColor = UIColor.red.cgColor
            showAlert(title: "Thông báo", message: "Điền thiếu thông tin!")
        }
        
        
    }

}
