//
//  ForgotPasswordController.swift
//  ThuY-Project
//
//  Created by Tuan on 20/05/2022.
//

import Foundation
import UIKit
import Alamofire

class ForgotPasswordController: ViewControllerCustom, ForgotPasswordDelegate, AlertNoOptionViewControllerDelegate {
    
    let forgotPasswordView = UINib(nibName: "ForgotPassword", bundle: nil).instantiate(withOwner: ForgotPasswordController.self, options: nil)[0] as! ForgotPassword
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    
    func setup() {

        forgotPasswordView.frame = CGRect(x: 0,
                                        y: 0,
                                        width: UIScreen.main.bounds.width,
                                        height: UIScreen.main.bounds.height)
        
        self.navigationController?.isNavigationBarHidden = true
        
        forgotPasswordView.delegate = self
        
        self.hideKeyboardWhenTappedAround()
        
        self.view.addSubview(forgotPasswordView)
    }
    
    
    func setTfEmpty() {
        forgotPasswordView.staffID.text = ""
        forgotPasswordView.newPassword.text = ""
        forgotPasswordView.email.text = ""
    }
    
    func getPasswordAction(staffID: String, newPassword: String, email: String) {
        
        let param: Parameters = ["username": staffID, "newPassword": newPassword, "email": email]
        
        self.showLoadingView()
        
        APIService.shared.requestWith(url: "\(BaseURL.forgotPasswordURL)",
                                      method: .put,
                                      parameters: param,
                                      objectTyple: validateError.self,
                                      headers: nil,
                                      encoding: URLEncoding.default) { isSuccess, json, statusCode in
            guard let status = statusCode, let message = json else { return }
            self.dismissLoadingView()
            if status == 200 {
                self.presentAlertNoOptionOnMainThread(content: "Thành công") { vc in
                    vc.delegate = self
                }
            } else {
                self.showAlert(title: "⚠️", message: "\(message)")
            }
            
        }
    }
    
    func didOKButtonTapped() {
        self.dismiss(animated: true)
    }
    
}

