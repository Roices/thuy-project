//
//  HomeViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 27/04/2022.
//

import UIKit
protocol HomeViewControllerDelegate: AnyObject {
    func didStaffVCTapped()
    
}

class HomeViewController: ViewControllerCustom {
    // MARK: - Subview
    let overviewView: UIView = UIView()
    
    var overviewVC: OverviewViewController!
    
    var petChartView: ChartPieView = ChartPieView()
    
    var staffChartView: ChartPieView = ChartPieView()
    
    let scrollView = UIScrollView()
    
    let stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = 8
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    let refreshControl: UIRefreshControl = {
       let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshControlDidPull(_:)), for: .valueChanged)
        return refreshControl
    }()
    
    let filterMenuSelectionView = FilterMenuView()
    
    
    // MARK: - Properties
    weak var delegate: HomeViewControllerDelegate?
    var overviewHeight: NSLayoutConstraint!
    private let padding: CGFloat = 8
    static let overviewHeight: CGFloat = 210
        
    var scrollViewTopAnchor: NSLayoutConstraint!
    
    var filterMenuHeightAnchor: NSLayoutConstraint!
    
    var qttRole = [String: Int]()
    var petSpecies = [String: Int]()
    

    var petsLabel = [String]()

    // pets quatity
    var petsQtt = [Double]()
    
    var petsTotal = 0
    
    var staffLabel = [String]()
    //staff quatity
    var staffsQtt = [Double]()
    
    var staffTotal = 0
    
    var showFilter: Bool = false{
        didSet{
            showFilterMenu()
        }
    }
    
    var height: CGFloat = 0

    
    //-------------------- Call Pet API--------------------
//    var pet: Pet?{
//        didSet{
//
//        }
//    }
    //-------------------- Call Pet API--------------------
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupScrollView()
        setup()
        layout()
        setupNavBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        showLoadingView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if showFilter {
            showFilter.toggle()
        }
    }
    
    // MARK: - Selector
    @objc func refreshControlDidPull(_ sender: UIRefreshControl){
        // reload all data
        let group = DispatchGroup()
        group.enter()
        overviewVC.updateData()
        group.leave()
        group.notify(queue: .main) {
            self.refreshControl.endRefreshing()
        }
        // refresh end
    }
    @objc func didSideMenuTapped(_ sender: UIButton){
        NotificationCenter.default.post(name: .sideMenu, object: nil)
    }
    
    @objc func didFilterMenuTapped(_ sender: UIButton){
        showFilter.toggle()
    }

    
    // MARK: - API
    
    // MARK: - Helper
    func showFilterMenu(){
        if showFilter {
            filterMenuSelectionView.isHidden = false
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                self.scrollViewTopAnchor.constant = self.height + 16
                self.view.layoutIfNeeded()
            }, completion: nil)
        }else{
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseInOut, animations: {
                self.scrollViewTopAnchor.constant = 8
                self.view.layoutIfNeeded()
            }, completion: {_ in
                self.filterMenuSelectionView.isHidden = true
            })
        }
    }
    
    func sortArray(labels: [String], values: [Double]) -> (label: [String], value: [Double]){
        var label = labels
        var value = values
        for i in 0..<value.count - 1 {
            for j in 0..<value.count - i - 1 {
                if value[j] > value[j+1] {
                    value.swapAt(j, j+1)
                    label.swapAt(j, j+1)
                }
                
            }
        }
        return (label, value)
    }
}
// MARK: - Extension
extension HomeViewController {
    //hello world
    func setup(){
        view.backgroundColor = .lightSecondaryGrayTY
        overviewVC = OverviewViewController()
        overviewVC.delegate = self
        bringVCtoParent(from: overviewVC, to: overviewView)
        
        configView(with: overviewView, height: 210)
        configView(with: petChartView, height: 330)
        configView(with: staffChartView, height: 330)
        
        filterMenuSelectionView.isHidden = true
        
        petChartView.delegate = self
        staffChartView.delegate = self
        
        // api change
//        petChartView.customizeChart(dataPoints: pets, values: petsQtt)
//        petChartView.titleLabel.text = "Thú nuôi"
        
        
        staffChartView.titleLabel.text = "Nhân sự"
        
        filterMenuSelectionView.delegate = self
        
    }
    func layout(){
        view.addSubview(filterMenuSelectionView)
        view.addSubview(scrollView)
        scrollView.addSubview(stackView)
        
        //filterMenuSelectionView
        filterMenuHeightAnchor = filterMenuSelectionView.heightAnchor.constraint(equalToConstant: 0)
        NSLayoutConstraint.activate([
            filterMenuSelectionView.topAnchor.constraint(equalToSystemSpacingBelow: view.safeAreaLayoutGuide.topAnchor, multiplier: 1),
            filterMenuSelectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -8),
            filterMenuSelectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 8),
            filterMenuHeightAnchor

        
        ])
        
        //--------------------ScrollView--------------------
        stackView.addArrangedSubview(overviewView)
        stackView.addArrangedSubview(petChartView)
        stackView.addArrangedSubview(staffChartView)
        
        scrollViewTopAnchor = scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: padding)
        
        NSLayoutConstraint.activate([
            scrollViewTopAnchor,
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: padding),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -padding),
            scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
            
        ])
        
        //stackView
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor)
        ])
        
        //--------------------ScrollView--------------------
    }
    
    func setupNavBar(){
        title = "Trang chủ"
        let button = NavCustomButton(image: Image.threeLine)
        button.addTarget(self, action: #selector(didSideMenuTapped(_:)), for: .touchUpInside)
        let leftItemButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = leftItemButton
        
        let filterButton = NavCustomButton(image: Image.filter)
        filterButton.addTarget(self, action: #selector(didFilterMenuTapped(_:)), for: .touchUpInside)
        
        let rightItemButton = UIBarButtonItem(customView: filterButton)
        self.navigationItem.rightBarButtonItem = rightItemButton
    }
    
    func setupScrollView(){
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.refreshControl = refreshControl
        scrollView.showsVerticalScrollIndicator = false
        
    }
    
    func configView(with view: UIView, height: CGFloat){
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        view.heightAnchor.constraint(equalToConstant: height).isActive = true
    }
}

// MARK: - ChartPieViewDelegate
extension HomeViewController: ChartPieViewDelegate {
    func didDetailButtonTapped(view: UIView) {
        if view === petChartView{
            //move to petVC
            let vc = SpeciesViewController()
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }else if view === staffChartView{
            //move to staffVCV
            delegate?.didStaffVCTapped()
            
        }
    }

}

extension HomeViewController: OverviewViewControllerDelegate {
    
    func didLoadedBranch(branches: [BranchData]) {
        filterMenuSelectionView.branches = branches
                
        height = CGFloat(branches.count * FilterMenuCell.rowHeight + 16 + 30)
        height = height <= 200 ? height : 200
        filterMenuHeightAnchor.constant = CGFloat(height)
        view.layoutIfNeeded()
        
    }

    
    func loadPetData(type: [SpeciesData]) {
        petsLabel.removeAll()
        petSpecies.removeAll()
        petsTotal = 0
        petsQtt.removeAll()
        
        for type in type {
            if petSpecies[type.name] == nil {
                petSpecies[type.name] = 0
                petSpecies[type.name]! += 1
            }else{
                petSpecies[type.name]! += 1
            }
        }
        
        //get total species
        _ = petSpecies.values.map({petsTotal += $0})
        //add label
        petsLabel.append(contentsOf: petSpecies.keys)
        //get %
        _ = petSpecies.map({petsQtt.append(Double(petSpecies[$0.key]!) / Double(petsTotal))})
        
                
        if petsLabel.count > 0 && petsQtt.count > 0 {
            let sortedTuple = sortArray(labels: petsLabel, values: petsQtt)
            petChartView.customizeChart(dataPoints: sortedTuple.label, values: sortedTuple.value)
        }else{
            petChartView.customizeChart(dataPoints: petsLabel, values: petsQtt)
        }
    }

    
    func loadedStaffData(roles: [[RoleModel]]) {
        staffLabel.removeAll()
        qttRole.removeAll()
        staffTotal = 0
        staffsQtt.removeAll()
        // statistic number staff in each roles
        for role in roles {
            for i in role {
                if qttRole[i.name] == nil {
                    qttRole[i.name] = 0
                    qttRole[i.name]! += 1
                }else{
                    qttRole[i.name]! += 1
                }
            }
        }
        _ = qttRole.values.map({staffTotal += $0})
        staffLabel.append(contentsOf: qttRole.keys)
        _ = qttRole.map({staffsQtt.append(Double(qttRole[$0.key]!) / Double(staffTotal))})
        
        if staffLabel.count > 0 && staffsQtt.count > 0 {
            let sortedTuple = sortArray(labels: staffLabel, values: staffsQtt)
            staffChartView.customizeChart(dataPoints: sortedTuple.label, values: sortedTuple.value)
        }else{
            staffChartView.customizeChart(dataPoints: staffLabel, values: staffsQtt)
        }

        
    }
    

    
    func failedToLoadAPI() {
        self.presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { [weak self] vc in
            guard let self = self else {return}
            vc.delegate = self
        }
    }
    
    func didOverviewCellSelected(selection: Overview) {
        switch selection {
        case .branch:
            let vc = BranchViewController()
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
            
        case .pet:
            let vc = CustomerViewController()
            CustomerViewController.type = .pets
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case .staff:
            delegate?.didStaffVCTapped()
            break
        case .treat:
            let vc = CustomerViewController()
            CustomerViewController.type = .customers
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        case .owner:
            break
        case .booking:
            break
        }
    }
    func didLoadedData() {
        dismissLoadingView()
    }
}

extension HomeViewController: AlertNoOptionViewControllerDelegate {
    func didOKButtonTapped() {
        self.dismiss(animated: true) {
            if self.containerView != nil {
                self.dismissLoadingView()
            }
            NotificationCenter.default.post(name: .didTokenExpired, object: nil)
        }
    }
}

extension HomeViewController: FilterMenuViewDelegate{
    func didFilterCellSelected(branch: BranchData?, isAll: Bool) {
        DispatchQueue.main.async {
            self.showLoadingView()
        }
        overviewVC.branch = branch
        overviewVC.isAllSelected = isAll
        showFilter = false
        
    }
}
