//
//  OverviewViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 27/04/2022.
//

import UIKit


protocol OverviewViewControllerDelegate: AnyObject {
//    func didLayouSubview(_ availableHeight: CGFloat)
    func didLoadedData()
    func loadedStaffData(roles: [[RoleModel]])
    func didOverviewCellSelected(selection: Overview)
    func loadPetData(type: [SpeciesData])
    func failedToLoadAPI()
    func didLoadedBranch(branches: [BranchData])
}

class OverviewViewController: UIViewController {
    // MARK: - Subview
    private let titleLabel = LabelCustom(color: .greenTY, fontFamily: UIFont.preferredFont(forTextStyle: .headline), alignment: .left)
    
    var collectionView: UICollectionView!
    
    
    // MARK: - Properties
    var titleHeight: CGFloat = 0
    var collectionViewHeight: NSLayoutConstraint!
    private var padding: CGFloat = 8
    
    weak var delegate: OverviewViewControllerDelegate?
    var quatities = [String: Int]()
        
    var group: DispatchGroup!
    
    var roles =  [[RoleModel]]()
    
    var species = [SpeciesData]()
    
    var isAllSelected: Bool = true{
        didSet{
            updateData()
        }
    }
    
    var branch: BranchData?
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        layout()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateData()
    }
    // MARK: - Selector
    
    // MARK: - API
    
    func getTotalBranch(page: Int, size: Int, group: DispatchGroup){
        
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        NetworkManager.shared.fetchBranches(token: token, page: page, size: size) { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .success(let branches):
                self.quatities[Overview.branch.title] = branches.count
//                self.collectionView.reloadData()
                self.delegate?.didLoadedBranch(branches: branches)
                
                group.leave()
            case .failure(_):
                self.delegate?.failedToLoadAPI()
            }
        }
    }
    
    func getTotalStaff(group: DispatchGroup){
        
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        
        self.roles.removeAll()
        NetworkManager.shared.fetchStaffs(token: token, page: 1, size: 999) { [weak self] result in
            guard let self = self else {return}
            switch result {
            case .success(let staffs):
                self.roles.removeAll()
                _ = staffs.map({self.roles.append($0.roles)})
                self.quatities[Overview.staff.title] = staffs.count
                self.delegate?.loadedStaffData(roles: self.roles)
                group.leave()
            case .failure(_):
                self.delegate?.failedToLoadAPI()
            }
        }
    }
    
    

    
    func getTotalPets(group: DispatchGroup){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        self.species.removeAll()
        NetworkManager.shared.fetchPets(token: token, page: 1, size: 999) { [weak self] result in
            guard let self = self else {return}
            switch result{
            
            case .success(let pets):
                self.quatities[Overview.pet.title] = pets.count
                if pets.count > 0 {
                    _ = pets.map({self.species.append($0.species[0].type!)})
                }

                self.delegate?.loadPetData(type: self.species)
                group.leave()
            case .failure(_):
                self.delegate?.failedToLoadAPI()
            }
        }
    }
    
    
    func getTotalCustomers(group: DispatchGroup){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        NetworkManager.shared.fetchCustomers(token: token, page: 1, size: 999) { [weak self] result in
            guard let self = self else {return}
            switch result {
            
            case .success(let res):
                self.quatities[Overview.treat.title] = res
                group.leave()
            case .failure(_):
                self.delegate?.failedToLoadAPI()
            }
            
        }
    }
    
    func getFilterBranch(group: DispatchGroup){
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        guard let id = branch?.id else{return}
        NetworkManager.shared.filterBranch(token: token, id: id) { [weak self] result in
            guard let self = self else {return}
            switch result{
            case .success(let branch):
                self.quatities[Overview.treat.title] = branch.customerNumber
                self.quatities[Overview.pet.title] = branch.petNumber
                self.quatities[Overview.staff.title] = branch.userNumber
                group.leave()
            case .failure(_):
                self.delegate?.failedToLoadAPI()
            }
        }
    }
    
    func getUserFollowNameBranch(group: DispatchGroup){
        guard let branch = branch else {return}
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        NetworkManager.shared.searchUserInBranch(token: token, page: 1, size: 999, branch: branch.name) { [weak self] result in
            guard let self = self else {return}
            switch result {
            
            case .success(let staffs):
                self.roles.removeAll()
                _ = staffs.map({self.roles.append($0.roles)})
                self.delegate?.loadedStaffData(roles: self.roles)
                
                group.leave()
            case .failure(let error):
                self.delegate?.failedToLoadAPI()
                
            }
        }
    }
    
    func getPetFollowNameBranch(group: DispatchGroup){
        guard let branch = branch else {return}
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
        NetworkManager.shared.searchPetsInBranch(token: token, page: 1, size: 999, branchID: branch.id!) { [weak self] result in
            guard let self = self else {return}
            switch result {
            
            case .success(let pets):
                self.species.removeAll()
                
                _ = pets.map({self.species.append($0.species[0].type!)})
                self.delegate?.loadPetData(type: self.species)
                group.leave()
            case .failure(_):
                self.delegate?.failedToLoadAPI()
            }
        }
    }
    
    // MARK: - Helper
    func updateData(){
        group = DispatchGroup()
        if isAllSelected {
            group.enter()
            getTotalBranch(page: 1, size: 1000, group: group)
            group.enter()
            getTotalStaff(group: group)
            group.enter()
            getTotalPets(group: group)
            group.enter()
            getTotalCustomers(group: group)
            
        }else{
            group.enter()
            getTotalBranch(page: 1, size: 1000, group: group)
            group.enter()
            getFilterBranch(group: group)
            group.enter()
            getUserFollowNameBranch(group: group)
            group.enter()
            getPetFollowNameBranch(group: group)
        }
    
        group.notify(queue: .main) {
            self.collectionView.reloadData()
            self.delegate?.didLoadedData()
        }
        //check all api load successful before dislay homeview

    }
    
    func requiredHeight(labelText:String) -> CGFloat {

        let font = UIFont.preferredFont(forTextStyle: .headline)
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: .max))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.text = labelText
        label.sizeToFit()
        return label.frame.height
    }
}
// MARK: - Extension
extension OverviewViewController {
    
    func setup(){
        
        configCollectionView()
        
        //calculate height of label
        titleLabel.text = "Tổng quan"
        titleLabel.numberOfLines = 0
        titleHeight = requiredHeight(labelText: titleLabel.text!)
        quatities[Overview.branch.title] = 0
        quatities[Overview.pet.title] = 0
        quatities[Overview.treat.title] = 0
        quatities[Overview.staff.title] = 0
        
    }
    func layout(){
        view.addSubview(titleLabel)
        view.addSubview(collectionView)
        
        // layout title
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 8),
            titleLabel.leadingAnchor.constraint(equalToSystemSpacingAfter: view.leadingAnchor, multiplier: 1),

        ])
        
        //layout Collectionview
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 0),
            collectionView.leadingAnchor.constraint(equalToSystemSpacingAfter: view.leadingAnchor, multiplier: 0),
            view.trailingAnchor.constraint(equalToSystemSpacingAfter: collectionView.trailingAnchor, multiplier: 0),
            collectionView.heightAnchor.constraint(equalToConstant: HomeViewController.overviewHeight - titleHeight - padding * 2),

        ])
    }
    
    func configCollectionView(){
        collectionView = UICollectionView(frame: .zero, collectionViewLayout: createTwoColumnLayout())
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(OverviewCollectionCell.self, forCellWithReuseIdentifier: OverviewCollectionCell.reuseIdentifier)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .white
        collectionView.isScrollEnabled = false
        
    }
    
    //set layout for collectionView
    func createTwoColumnLayout() -> UICollectionViewFlowLayout{
        let layout = UICollectionViewFlowLayout()
        let width = view.bounds.width
        let height = HomeViewController.overviewHeight - titleHeight - padding * 2
        let minimumSpacing: CGFloat = 8
        let availableWidth = width - padding * 4 - minimumSpacing * 2
        let availableHeight = height - (padding * 6)
    
        let itemWidth = availableWidth / 2
        let itemHeight = availableHeight / 2
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        
        layout.sectionInset = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        
        return layout
    }
}

extension OverviewViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selection = Overview.allCases[indexPath.row]
        delegate?.didOverviewCellSelected(selection: selection)
    }
}

extension OverviewViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OverviewCollectionCell.reuseIdentifier, for: indexPath) as! OverviewCollectionCell
        cell.backgroundColor = Overview.allCases[indexPath.row].color
        cell.titleLabel.text = Overview.allCases[indexPath.row].title
        cell.quatityLabel.text = String(quatities[Overview.allCases[indexPath.row].title]!)
        cell.translucentImage.imageView.image = Overview.allCases[indexPath.row].image
        return cell
    }
    
    
}

