//
//  AddStaffViewController.swift
//  ThuY-Project
//
//  Created by Viettelimex on 04/05/2022.
//

import UIKit
import Alamofire

struct Teesst: Codable {
    let status: Int
    let messsage: String
}

class AddStaffViewController: ViewControllerCustom {
        
    
    lazy var addStaffView = UINib(nibName: "AddStaff", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! AddStaff
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        addStaffView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        addStaffView.root = self
       addStaffView.delegate = self
  

        self.view.addSubview(addStaffView)
        
        setupAddMenu()
        
        }
 
}

extension AddStaffViewController{
    func setupAddMenu(){
        view.backgroundColor = .white
        title = "Thêm mới nhân viên"
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.greenTY]
        
    }
    
}


extension AddStaffViewController: AddStaffDelegate{

    func AddStaff(userNameAdd: String, sexAdd: String, birthAdd: String, identifyAdd: String, emailAdd: String, noteAdd: String, experienceAdd: String, specializationAdd: String, addressAdd: String, phoneNumberAdd: String) {



        guard let token = UserDefaults.standard.string(forKey: "access_token") else {return}
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(token)"]


        let param = [   "name": userNameAdd,
                        "sex": sexAdd,
                        "birth": birthAdd,
                        "identityCard": identifyAdd,
                        "phoneNumber": phoneNumberAdd,
                        "address": addressAdd,
                        "specialization": specializationAdd,
                        "experience": experienceAdd,
                        "username": userNameAdd,
                        "password": "1",
                        "roles": [1],
                        "branchId": "CN00001",
                        "email": emailAdd ] as [String: Any?]

        let parameters = param.compactMapValues { $0 }


        APIService.shared.requestWith(url: "\(StaffBaseURL.addURL)",
                                          method: .post,
                                          parameters: parameters,
                                          objectTyple: Teesst.self,
                                          headers: headers,
                                          encoding: JSONEncoding.default ) {isSuccess, json, statusCode in
            guard let status = statusCode, let data = json else {return}

                DispatchQueue.main.async {
                    self.dismissLoadingView()
                }

                if status == 200{
                    self.showAlerttest(title: "Success", message: "Thêm thành công")
                    self.setEmpty()
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    self.showAlert(title: "Error", message: "\(data)")

                }
            }
    }
    func setEmpty(){

        addStaffView.userNameAdd.text = ""
        addStaffView.sexAdd.text = ""
        addStaffView.birthAdd.text = ""
        addStaffView.identifyAdd.text = ""
        addStaffView.emailAdd.text = ""
        addStaffView.noteAdd.text = ""
        addStaffView.experienceAdd.text = ""
        addStaffView.specializationAdd.text = ""
        addStaffView.addressAdd.text = ""
        addStaffView.phoneNumberAdd.text = ""

    }
    func showAlerttest(title: String, message: String) {
       let alertController = UIAlertController(title: title, message:
         message, preferredStyle: .alert)
       alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
           self.navigationController?.popViewController(animated: true)
       }))
       self.present(alertController, animated: true, completion: nil)
     }


}
