//
//  InfoStaffViewController.swift
//  ThuY-Project
//
//  Created by Viettelimex on 04/05/2022.
//
import Foundation
import UIKit
import Alamofire

class InfoStaffController: UIViewController {

    
    // MARK: - Subview
    lazy var infoStaffView = UINib(nibName: "InfoStaff", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! InfoStaff
    
    lazy var staffView = UINib(nibName: "StaffViewController", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! StaffViewController

    
    // MARK: - Properties
    lazy var infoStaff: StaffModel? = nil

    lazy var url = "\(StaffBaseURL.staffURL)"
        
    
    override func viewDidLoad() {
        super.viewDidLoad()

        infoStaffView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        infoStaffView.root = self
    
        setupInfo()
        
        configData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configData()
    }
 
}
extension InfoStaffController {
    
    func configData() {
        infoStaffView.addressStaff.text = infoStaff?.address
        infoStaffView.userName.text = infoStaff?.username
        infoStaffView.idStaff.text = infoStaff?.id
        infoStaffView.sexStaff.text = infoStaff?.sex
        infoStaffView.birthStaff.text = infoStaff?.birth
        infoStaffView.identifyCardStaff.text = infoStaff?.identityCard
        infoStaffView.addressStaff.text = infoStaff?.address
        infoStaffView.phoneNumberStaff.text = infoStaff?.phoneNumber
        infoStaffView.specializationStaff.text = infoStaff?.specialization
        infoStaffView.emailStaff.text = infoStaff?.email
        infoStaffView.experienceStaff.text = infoStaff?.experience
        
    }
    
    func setupInfo(){
        
        view.backgroundColor = .white
        title = "Thông Tin Nhân Viên"
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.greenTY]
        
        infoStaffView.delegate = self
        
    }
}

extension InfoStaffController: InfoStaffDelegate {
    func deleteStaff(staffID: String) {
        guard let token = UserDefaults.standard.string(forKey: "access_token") else { return }
    
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]


        APIService.shared.requestWith(url: "\(url)/\(staffID)",
                                  method: .delete,
                                  parameters: nil,
                                  objectTyple:EditStaffModel.self,
                                  headers: headers,
                                    encoding: JSONEncoding.default) {isSuccess, json , statusCode in
        
            guard let status = statusCode else { return }
        
            if status == 200 {
            
                let delete = UIAlertController(title: "Success", message: "Xoá thành công", preferredStyle: .alert)
                delete.addAction(UIAlertAction(title: "OK",style: .default,handler: { action in
                self.navigationController?.popViewController(animated: true)

                }))
                
              self.present(delete, animated: true, completion: nil)

            }else {
                self.showAlert(title: "Failed", message: "Đã có lỗi xảy ra vui lòng thử lại!")

    }
            
    }
    }
    
        
    
}
