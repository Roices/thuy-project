//
//  EditStaffViewController.swift
//  ThuY-Project
//
//  Created by Viettelimex on 05/05/2022.
//

import UIKit
import Alamofire

struct Info {
    var id: String
    var sex: String
    var date: String
    var identify: String
    var phoneNumber: String
    var address: String
    var email: String
    var experience: String
    var specialization: String
}


class EditStaffViewController: ViewControllerCustom {
    
    lazy var editStaffView = UINib(nibName: "EditStaff", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! EditStaff
    
    lazy var staffView = UINib(nibName: "StaffViewController", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! StaffViewController
    
    lazy var infostaffView = UINib(nibName:  "InfoStaffController", bundle: nil).instantiate(withOwner: self, options: nil) [0] as! InfoStaffController
    
    lazy var url = "\(StaffBaseURL.staffURL)"
    
    var pushData: Info?
    
    
    var infomation: EditStaffModel? = nil
    
    var idStaff: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        editStaffView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        editStaffView.root = self
        editStaffView.delegate = self
        
        self.view.addSubview(editStaffView)
        setupEditMenu()
        
        editStaffView.pushData = pushData
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
            
    }


}

extension EditStaffViewController{
    func setupEditMenu(){
        view.backgroundColor = .white
        title = "Chỉnh sửa nhân viên"
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.greenTY]
        
    }
}
extension EditStaffViewController: EditDelegate{

    func editStaff(userNameEdit : String, sexEdit : String, birthEdit : String, identifyEdit : String, emailEdit : String, noteEdit : String, experienceEdit : String, specializationEdit : String, addressEdit : String, phoneNumberEdit : String) {
        
        
        guard let token = UserDefaults.standard.string(forKey: "access_token") else {return}
        
        guard let id = self.idStaff else {return}
        
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(token)"]
        
        let param = [   "name": userNameEdit,
                        "sex": sexEdit,
                        "birth": birthEdit,
                        "identityCard": identifyEdit,
                        "phoneNumber": phoneNumberEdit,
                        "address": addressEdit,
                        "specialization": specializationEdit,
                        "experience": experienceEdit,
                        "username": userNameEdit,
                        "password": "1",
                        "roles": [1],
                        "branchId": "CN00001",
                        "email": emailEdit ] as [String: Any?]
        
        let parameters = param.compactMapValues { $0 }
        
    
        
            APIService.shared.requestWith(url: "\(StaffBaseURL.editStaffURL)/\(id)",
                                          method: .put,
                                          parameters: parameters,
                                          objectTyple: Teesst.self,
                                          headers: headers,
                                          encoding: JSONEncoding.default ) {isSuccess, json, statusCode in
                guard let status = statusCode else {return}
                
                DispatchQueue.main.async {
                    self.dismissLoadingView()
                    
                }
                if status == 200{
                 
                        self.showAlerttest(title: "Success", message: "Sửa thành công")
                        self.setEmpty()
                        self.navigationController?.popViewController(animated: true)
                    
                }
                else{
                    self.showAlert(title: "Failed", message: "Đã có lỗi xảy ra vui lòng thử lại!")
                }
            }
        }

    

    func setEmpty(){
        editStaffView.userNameEdit.text = ""
        editStaffView.sexEdit.text = ""
        editStaffView.birthEdit.text = ""
        editStaffView.identifyEdit.text = ""
        editStaffView.emailEdit.text = ""
        editStaffView.noteEdit.text = ""
        editStaffView.experienceEdit.text = ""
        editStaffView.specializationEdit.text = ""
        editStaffView.addressEdit.text = ""
        editStaffView.phoneNumberEdit.text = ""
    }
    func showAlerttest(title: String, message: String) {
       let alertController = UIAlertController(title: title, message:
         message, preferredStyle: .alert)
       alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
           self.navigationController?.popViewController(animated: true)
       }))
       self.present(alertController, animated: true, completion: nil)
     }


}

