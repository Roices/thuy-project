//
//  StaffViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 27/04/2022.
//

import UIKit
import Alamofire
import SwiftUI


class StaffViewController: UIViewController{
 
    
    lazy var staffView = UINib(nibName: "Staff", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! Staff
    
    
    
    @IBOutlet weak var btnDoc: UIButton!
    @IBOutlet weak var btnStaff: UIButton!
    @IBOutlet weak var StaffTableView: UITableView!
    @IBOutlet weak var backGround: UIView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK: - Subview
    
    
    // MARK: - Properties
    
    var getInfoStaff: [StaffModel] = [] {
        didSet {
            if isViewLoaded {
                staffView.StaffTableView.reloadData()
            }
        }
    }
    
    lazy var url = "\(StaffBaseURL.staffURL)"
    
    lazy var searchurl = "\(StaffBaseURL.searchStaffURL)"

    lazy var staffEdit = "\(StaffBaseURL.editStaffURL)"
    static var type: staffButton = .staffView

    lazy var param = [String: String]()
    var id:Int? = nil
    
    var currentTableView: Int!
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        currentTableView = 0
        
        staffView.frame = CGRect(x: 0,
                                 y: 0,
                                 width: UIScreen.main.bounds.width,
                                 height: UIScreen.main.bounds.height)
        staffView.StaffTableView.delegate = self
        staffView.StaffTableView.dataSource = self
        staffView.root = self
        staffView.StaffTableView.sectionFooterHeight = 0
        staffView.searchBar.delegate = self
        
        let staffCellNib = UINib(nibName: "StaffCell", bundle: nil)
        staffView.StaffTableView.register(staffCellNib, forCellReuseIdentifier: "StaffCell")
        let staffDocNib = UINib(nibName: "StaffCellDoc", bundle: nil)
        staffView.StaffTableView.register(staffDocNib, forCellReuseIdentifier: "StaffCellDoc")
        
        staffView.searchBar.delegate = self

        
        self.view.addSubview(staffView)
        

       // getDataStaff()
        setupMenu()
    }
        

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getDataStaff()

    }
    

    
    @IBAction func btnAddStaff(_ sender: UIButton){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "AddStaffViewController") as! AddStaffViewController
                self.navigationController?.pushViewController(vc, animated: true)
        navigationItem.backButtonTitle = ""

    }
    

}

 // MARK: - API
extension StaffViewController: StaffDelegate{

 
    
    func getDataStaff() {
        

        guard let token = UserDefaults.standard.string(forKey: "access_token") else {return}

        let headers: HTTPHeaders = ["Authorization" : "Bearer \(token)"]

        param = ["page" : "1", "size" : "10"]

        APIService.shared.requestWith(url: url,
                                      method: .get,
                                      parameters: param,
                                      objectTyple: BaseResponse<StaffModel>.self,
                                      headers: headers,
                                      encoding: URLEncoding.default) {[self] success, data, statusCode in
            guard let status = statusCode, let _data = data as? BaseResponse<StaffModel> else {return}
            
            if status != 200{
                showAlert(title: "error", message: "\(_data)")
            }
            
     
            getInfoStaff = _data.data
          
            
        
        DispatchQueue.main.async {
            self.staffView.StaffTableView.reloadData()
        }
    }

    }
    
}
// MARK: - Extension
extension StaffViewController {
    
    func setupMenu(){
        
        
        title = "Nhân Viên"
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.greenTY]
        self.navigationController?.navigationBar.backgroundColor = .clear
    }
    

}
    


extension StaffViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getInfoStaff.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 74
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if StaffViewController.type == .staffView{
        let cell = tableView.dequeueReusableCell(withIdentifier: "StaffCell", for: indexPath) as! StaffCell
            cell.textLabel?.textColor = UIColor.white
            cell.layer.cornerRadius = 10
            cell.layer.borderWidth = 0.2
            cell.layer.borderColor = UIColor.gray.cgColor

            let modelStaff = getInfoStaff[indexPath.row]
            cell.IDLabel.text = modelStaff.id
            cell.UserName.text = modelStaff.name
//            cell.Role.text = modelStaff.roles.name
            
            
        return cell
        }else
        {
        let cellDoc = tableView.dequeueReusableCell(withIdentifier: "StaffCellDoc", for: indexPath) as! StaffCellDoc
            cellDoc.textLabel?.textColor = UIColor.white
            cellDoc.layer.cornerRadius = 10
            cellDoc.layer.borderWidth = 0.2
            cellDoc.layer.borderColor = UIColor.gray.cgColor
            let modelStaff = getInfoStaff[indexPath.row]
            cellDoc.IDLabel.text = modelStaff.id
            cellDoc.UserName.text = modelStaff.name
//            cellDoc.Role.text = modelStaff.roles.name
        return cellDoc
        }
        

        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

       return 50

    }
    


    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if StaffViewController.type == .staffView{
    let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let vc  = storyboard.instantiateViewController(withIdentifier: "InfoStaffController") as? InfoStaffController
            
            vc?.infoStaff = getInfoStaff[indexPath.row]
        
            self.navigationController?.pushViewController(vc!, animated: true)
        navigationItem.backButtonTitle = ""
            
    
        }
        if StaffViewController.type == .docView {
    let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    let vc  = storyboard.instantiateViewController(withIdentifier: "InfoDocViewController") as! InfoDocViewController
            vc.infoStaff = getInfoStaff[indexPath.row]
            
                self.navigationController?.pushViewController(vc, animated: true)
            navigationItem.backButtonTitle = ""
        }
    
    }
    
 

    

}
extension StaffViewController: UISearchBarDelegate{
    

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        guard let token = UserDefaults.standard.string(forKey: "access_token") else{return}
        
        guard let data = searchBar.text else { return }
        
        let headers: HTTPHeaders = ["Authorization" : "Bearer \(token)"]
        
        param = ["page" : "1", "size" : "10", "name" : data ,"phone" : "", "role" : "", "branch" : "" ]
        
        let parameters = param.compactMapValues { $0 }

     
        APIService.shared.requestWith(url: searchurl,
                                      method: .get,
                                      parameters: parameters,
                                      objectTyple: BaseResponse<StaffModel>.self,
                                      headers: headers,
                                      encoding: URLEncoding.default){ isSuccess, json , statusCode in
            
            guard let status = statusCode, let _data = json as? BaseResponse<StaffModel> else {return}
            
            if status != 200{
                self.showAlert(title: "error", message: "\(_data)")
            }
            
      
            self.getInfoStaff = _data.data
       
            
        
        DispatchQueue.main.async {
            self.staffView.StaffTableView.reloadData()
        }

        }
}
    
    
}
