//
//  NotificationViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 27/04/2022.
//
import UIKit
import Alamofire


class NotificationViewController: ViewControllerCustom, AlertNoOptionViewControllerDelegate {

    
    // MARK: - Subview
    let notificationView = UINib(nibName: "Notification", bundle: nil).instantiate(withOwner: NotificationViewController.self, options: nil)[0] as! NotificationView
    
    let bookingDetailView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailNotificationViewController") as! DetailNotificationViewController
    
    lazy var refreshControl = UIRefreshControl()
    
        var listNotiUser: [Noti] = [] {
        didSet {
            DispatchQueue.main.async {
                self.notificationView.notiTableView.reloadData()
            }
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        
        NotificationCenter.default.addObserver(self, selector: #selector(becomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }
    
    // MARK: - Selector
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.handleNotifData()

    }
    

    func handleNotifData() {
        
        listNotiUser.removeAll()
        
        self.showLoadingView()
        
        guard let userID = UserDefaults.standard.value(forKey: "userID") else { return }
        
        let url = "\(BaseURL.sendingNotiURL)/?page=1&size=30&userId=\(userID)"
        
        APIService.shared.requestWith(url: url,
                                      method: .get,
                                      parameters: nil,
                                      objectTyple: BaseResponse<Noti>.self,
                                      headers: nil,
                                      encoding: JSONEncoding.default) { [self] isSuccess, json, statusCode in
            
            guard let status = statusCode, let data = json as? BaseResponse<Noti> else { return }
            
          
            
            dismissLoadingView()
            
            if status == 200 {
                self.listNotiUser = data.data
            
                listNotiUser = listNotiUser.reversed()
         
                
                DispatchQueue.main.async {
                    self.notificationView.notiTableView.reloadData()
                }

            } else {
                self.presentAlertNoOptionOnMainThread(content: "Đã có lỗi xảy ra bạn vui lòng thử lại") { vc in
                    vc.delegate = self
                }
            }
        }
    }
    
    func didOKButtonTapped() {
        self.dismiss(animated: true)
    }
    
    @objc func becomeActive() {
        self.handleNotifData()
    }


    @objc func refreshTableView(_ sender: UIRefreshControl){
        //waiting for getSchedule successful then end refreshing
        let group = DispatchGroup()
        group.enter()
        handleNotifData()
        group.leave()
        group.notify(queue: .main) {
            self.refreshControl.endRefreshing()
        }
    }
    
}
// MARK: - Extension
extension NotificationViewController {
    
    func setup(){
        title = "Thông báo"
        view.backgroundColor = .clear
        notificationView.frame = CGRect(x: 0, y: 0,
                                        width: UIScreen.main.bounds.width,
                                        height: UIScreen.main.bounds.height)
        refreshControl.addTarget(self, action: #selector(refreshTableView(_:)), for: .valueChanged)
        setupTableView()
        self.view.addSubview(notificationView)
        
    }

}

extension NotificationViewController: UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return listNotiUser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotiForUserCell", for: indexPath) as! NotiForUserCell
        let data = listNotiUser[indexPath.row]
        cell.doctorName.text = data.message
        cell.time.text = data.date
        return cell
        
    }
    // View and Action of Table ---------------------------------------------
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let data = listNotiUser[indexPath.row]
        bookingDetailView.id = data.topic
        self.navigationController?.pushViewController(bookingDetailView, animated: true)
    }
    
    func setupTableView() {
        let nib = UINib(nibName: "NotiForUserCell", bundle: nil)
        self.notificationView.notiTableView.register(nib, forCellReuseIdentifier: "NotiForUserCell")
        notificationView.notiTableView.backgroundColor = .clear
        notificationView.notiTableView.dataSource = self
        notificationView.notiTableView.delegate = self
        notificationView.notiTableView.showsVerticalScrollIndicator = false
        notificationView.notiTableView.refreshControl = refreshControl
        
        //
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 91
        
    }
}


