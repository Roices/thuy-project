//
//  DetailNotificationViewController.swift
//  ThuY-Project
//
//  Created by Tuan on 05/06/2022.
//

import UIKit
import Alamofire

class DetailNotificationViewController: ViewControllerCustom{
        
    let bookingDetailView = UINib(nibName: "BookingDetail", bundle: nil).instantiate(withOwner: DetailNotificationViewController.self, options: nil)[0] as! BookingDetail
    
    var id:String? = nil
    
    var infomation:Booking? = nil
    
    var isRole: String = "EMP" {
        didSet {
            if isRole == "EMP"{
                bookingDetailView.editButton.isHidden = true
                bookingDetailView.cancelButton.isHidden = true
            } else {
                bookingDetailView.editButton.isHidden = false
                bookingDetailView.cancelButton.isHidden = false
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupNavBar()
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        guard let role = UserDefaults.standard.string(forKey: "role") else { return }
        
        isRole = role
        
        getBookingWithId()
    }
    
    func setupNavBar() {
            title = "Chi tiết lịch khám"
        
            let button = NavCustomButton(image: Image.backButton)
        
            button.addTarget(self, action: #selector(popView), for: .touchUpInside)
        
            let leftItemButton = UIBarButtonItem(customView: button)
        
            self.navigationItem.leftBarButtonItem = leftItemButton
    }


    func setup(){
        // Set frame & height for customerInfoView
        bookingDetailView.frame = CGRect(x: 0,
                            y: 0,
                            width: UIScreen.main.bounds.width,
                            height: UIScreen.main.bounds.height)
        
        bookingDetailView.delegate = self
        
        guard let role = UserDefaults.standard.string(forKey: "role") else { return }
        switch role {
        case "EMP":
            bookingDetailView.editButton.isHidden = true
            bookingDetailView.cancelButton.isHidden = true
        default:
            print("Default")
        }
        
        self.view.addSubview(bookingDetailView)
        
        self.hideKeyboardWhenTappedAround()
        
    }
    
    func configDataForUI() {
        guard let info = infomation else { return }
        
        var status = ""
        
        switch info.status{
        case "cancel":
            status = "Đã hủy"
        case "process":
            status = "Đang tiến hành"
        case "success":
            status = "Đã nhận"
        default:
            status = ""
        }
        
        if info.additionalDetail?.planDocId == nil {
            bookingDetailView.doctorName.text = info.additionalDetail?.realDocName
            bookingDetailView.doctorSpecialist.text = info.additionalDetail?.realDocSpec

            bookingDetailView.doctorExperience.text = info.additionalDetail?.realDocPhone
        } else {
            bookingDetailView.doctorName.text = info.additionalDetail?.planDocName
            bookingDetailView.doctorSpecialist.text = info.additionalDetail?.planDocSpec

            bookingDetailView.doctorExperience.text = info.additionalDetail?.planDocphone
        }
        


        bookingDetailView.address.text = info.user.branch.address

        bookingDetailView.date.text = info.date

        bookingDetailView.time.text = info.time

        bookingDetailView.customer.text = info.additionalDetail?.customerName

        bookingDetailView.petName.text = info.pet.name

        bookingDetailView.type.text = info.pet.species[0].name
        
        bookingDetailView.status.text = status

        bookingDetailView.symptom.text = info.symptom

        bookingDetailView.note.text = info.note
        
        
    }
    
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
    }
}


extension DetailNotificationViewController: BookingDetailDelegate, AlertNoOptionViewControllerDelegate {
    
    
    func getBookingWithId() {
        showLoadingView()
        
        guard let id = self.id else { return }
        
        let headers = APIService.shared.getHeader()

        APIService.shared.requestWith(url: "\(BaseURL.scheduleURL)/\(id)",
                                      method: .get,
                                      parameters: nil,
                                      objectTyple: BaseResponse<Booking>.self,
                                      headers: headers,
                                      encoding: URLEncoding.default) { isSuccess, json, statusCode in
            
            guard let status = statusCode, let data = json as? BaseResponse<Booking> else { return}
            
            self.dismissLoadingView()
            
            if status != 200 {
                self.showAlert(title: "Error", message: "Đã có lỗi xảy ra bạn vui lòng thử lại!")
            }else if status == 200 && isSuccess == false {
                self.presentAlertNoOptionOnMainThread(content: "Bạn đã nhận lịch khám này hoặc lịch khám này đã bị hủy") { vc in
                    vc.delegate = self
                }
            }else {
                self.infomation = data.data[0]
                self.configDataForUI()
            }
            
        }
        
        dismissLoadingView()
    }
    
    func cancelSchedule() {
        //
    }
    
    func editSchedule() {
        
        guard let userIdBooking = infomation?.user.id else {return}
        
        sendingNotiToUser(userID: "\(userIdBooking)")
    }
    
    
    func didOKButtonTapped() {
        
        self.dismiss(animated: true)
        
        self.navigationController?.popViewController(animated: true)
    }
    
        
    func sendingNotiToUser(userID: String) {
        
        guard let userName = UserDefaults.standard.string(forKey: "name"), let id = id else { return }
        
        let header = APIService.shared.getHeader()
        
        let url = "\(BaseURL.sendingNotiURL)?userId=\(userID)"
        
        let currentTime = Date()
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-dd-mm HH:mm"
        let currentTimeFormat = dateformatter.string(from: currentTime)
        
        let param = ["title": "Lịch khám",
                     "message": "Bác sĩ \(userName) đã nhận lịch khám của bạn.",
                     "topic": "\(id)",
                     "dateString": "\(currentTimeFormat)"] as [String : Any?]
        
        let parameters = param.compactMapValues { $0 }
        
        APIService.shared.requestWith(url: url,
                                      method: .post,
                                      parameters: parameters,
                                      objectTyple: validateError.self,
                                      headers: header,
                                      encoding: JSONEncoding.default) { [self] isSuccess, json, statusCode in
            guard let status = statusCode else { return }
            
            if status == 200 {
                presentAlertNoOptionOnMainThread(content: "Bạn đã nhận lịch khám thành công") { vc in
                    vc.delegate = self
                }
                getBookingWithId()
                
            } else {
                showAlert(title: "", message: "Đã có lỗi xảy ra bạn vui lòng thử lại")
            }
        
        }
        
        
    }
   
}
