//
//  AddingPetIDController.swift
//  ThuY-Project
//
//  Created by Tuan on 23/05/2022.
//

import UIKit
import Alamofire


protocol AddingPetIdProtocol: NSObject {
    func passingPetID(data: String)
}

class AddingPetIDController: ViewControllerCustom, AlertNoOptionViewControllerDelegate {


    let addingPetIDView = UINib(nibName: "AddingInformation", bundle: nil).instantiate(withOwner: AddingPetIDController.self, options: nil)[0] as! AddingInformation
     lazy var page = 1
     let size = 10
 
    weak var delegate: AddingPetIdProtocol?
    
    private let notFoundLabel = LabelCustom(color: .redTY, fontFamily: UIFont.preferredFont(forTextStyle: .headline), alignment: .center)
    
    // MARK: - Lifecycle
    
    var isFoundData: Bool = true{
        didSet{
            displayNotFoundLabelData()
        }
    }
    
    
    var listIdPet: [Pet] = [] {
        didSet {
            if isViewLoaded {
                addingPetIDView.tableData.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavBar()
        setup()
        // Do any additional setup after loading the view.
    }
    
    func setupNavBar(){
            title = "Mã vật nuôi"
        
            let button = NavCustomButton(image: Image.backButton)
        
            button.addTarget(self, action: #selector(popView), for: .touchUpInside)
        
            let leftItemButton = UIBarButtonItem(customView: button)
        
            self.navigationItem.leftBarButtonItem = leftItemButton
    }
    
    
    func setup(){
        // Set frame & height for customerInfoView
        addingPetIDView.frame = CGRect(x: 0,
                            y: 0,
                            width: UIScreen.main.bounds.width,
                            height: UIScreen.main.bounds.height)
        
        addingPetIDView.delegate = self
        
        addingPetIDView.tableData.delegate = self
        
        addingPetIDView.tableData.dataSource = self
        
        addingPetIDView.tableData.register(UITableViewCell.self, forCellReuseIdentifier: "petCell")
        
        self.view.addSubview(addingPetIDView)
        
        self.hideKeyboardWhenTappedAround()
        
        
        notFoundLabel.text = "Không có dữ liệu nào!"
        notFoundLabel.alpha = 0
        notFoundLabel.isHidden = true
        
        self.view.addSubview(notFoundLabel)
        
        NSLayoutConstraint.activate([
            notFoundLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            notFoundLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }

    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func displayNotFoundLabelData(){

        if isFoundData {
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseInOut) {
                self.notFoundLabel.alpha = 0
            } completion: { _ in
                self.notFoundLabel.isHidden = true
            }
        }else{
            self.notFoundLabel.isHidden = false
            self.notFoundLabel.alpha = 1
        }


    }

}


extension AddingPetIDController: AddingInfomationDelegate{
    
    func searchBarEnter(data: String) {
        
        showLoadingView()
        
        let param = ["page": page, "size": size, "name": data] as [String : Any?]

        let parameters = param.compactMapValues { $0 }

        let headers = APIService.shared.getHeader()

        APIService.shared.requestWith(url: "\(BaseURL.petURL)/search",
                                      method: .get,
                                      parameters: parameters,
                                      objectTyple: BaseResponse<Pet>.self,
                                      headers: headers,
                                      encoding: URLEncoding.default) { [self] isSuccess, json, statusCode in

            guard let data = json as? BaseResponse<Pet> , let status = statusCode else {
                presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { vc in
                    vc.delegate = self
                }
                return
            }
            
            
            self.dismissLoadingView()
            
            if status == 200 {
                listIdPet = data.data
                isFoundData = listIdPet.isEmpty ? false : true
                DispatchQueue.main.async { 
                    self.addingPetIDView.tableData.reloadData()
                }
            }
        }
    }
    
    func didOKButtonTapped() {
        self.dismiss(animated: true)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
}

extension AddingPetIDController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listIdPet.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "petCell")
        let data = listIdPet[indexPath.row]
        cell?.textLabel?.text = "\(data.name) - Mã Thú nuôi: \(data.id)"
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = listIdPet[indexPath.row]
        self.delegate?.passingPetID(data: data.id)
        
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
      
}
