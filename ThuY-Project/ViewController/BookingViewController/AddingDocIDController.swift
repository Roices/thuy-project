//
//  AddingDocIDController.swift
//  ThuY-Project
//
//  Created by Tuan on 23/05/2022.
//

import UIKit
import Alamofire

protocol AddingDocIdProtocol: NSObject {
    func passingDocID(data: String)
}

class AddingDocIDController: ViewControllerCustom, AlertNoOptionViewControllerDelegate {
    

    let addingDocIDView = UINib(nibName: "AddingInformation", bundle: nil).instantiate(withOwner: AddingDocIDController.self, options: nil)[0] as! AddingInformation
    
    weak var delegate: AddingDocIdProtocol?
    
    lazy var page = 1
    
    let size = 10
    
    private let notFoundLabel = LabelCustom(color: .redTY, fontFamily: UIFont.preferredFont(forTextStyle: .headline), alignment: .center)
    
    // MARK: - Lifecycle
    
    var isFoundData: Bool = true{
        didSet{
            displayNotFoundLabelData()
        }
    }
    
    var listIdDoc: [StaffModel] = [] {
        didSet {
            if isViewLoaded {
                addingDocIDView.tableData.reloadData()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavBar()
        setup()
        // Do any additional setup after loading the view.
    }
    
    func setupNavBar(){
            title = "Mã bác sĩ"
        
            let button = NavCustomButton(image: Image.backButton)
        
            button.addTarget(self, action: #selector(popView), for: .touchUpInside)
        
            let leftItemButton = UIBarButtonItem(customView: button)
        
            self.navigationItem.leftBarButtonItem = leftItemButton
    }
    
    
    func setup(){
        // Set frame & height for customerInfoView
        addingDocIDView.frame = CGRect(x: 0,
                            y: 0,
                            width: UIScreen.main.bounds.width,
                            height: UIScreen.main.bounds.height)
        
        addingDocIDView.delegate = self
        
        addingDocIDView.tableData.register(UITableViewCell.self, forCellReuseIdentifier: "docCell")
        
        addingDocIDView.tableData.delegate = self
        
        addingDocIDView.tableData.dataSource = self
        
        self.view.addSubview(addingDocIDView)
        
        self.hideKeyboardWhenTappedAround()
        
        notFoundLabel.text = "Không có dữ liệu nào!"
        notFoundLabel.alpha = 0
        notFoundLabel.isHidden = true
        
        self.view.addSubview(notFoundLabel)
        
        NSLayoutConstraint.activate([
            notFoundLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            notFoundLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }

    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func displayNotFoundLabelData(){

        if isFoundData {
            UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseInOut) {
                self.notFoundLabel.alpha = 0
            } completion: { _ in
                self.notFoundLabel.isHidden = true
            }
        }else{
            self.notFoundLabel.isHidden = false
            self.notFoundLabel.alpha = 1
        }


    }

}


extension AddingDocIDController: AddingInfomationDelegate{
    
    func searchBarEnter(data: String) {
        
            showLoadingView()
        
            guard let branhName = UserDefaults.standard.string(forKey: "branchName") else { return }
        
            let param = ["page": page, "size": size, "name": data, "phone": "", "role": "Doc", "branch": branhName] as [String : Any?]
        
            let parameters = param.compactMapValues { $0 }
        
            let headers = APIService.shared.getHeader()
        
        
        APIService.shared.requestWith(url: "\(BaseURL.userURL)/search",
                                      method: .get,
                                      parameters: parameters,
                                      objectTyple: BaseResponse<StaffModel>.self,
                                      headers: headers,
                                      encoding: URLEncoding.default) { [self] isSuccess, json, statusCode in
        
            guard let data = json as? BaseResponse<StaffModel>, let status = statusCode else {
                presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { vc in
                    vc.delegate = self
                }
                return
                
            }
                
            dismissLoadingView()
            
                if status == 200 {
              
                   listIdDoc = data.data
                   isFoundData = listIdDoc.isEmpty ? false : true
                    
                    DispatchQueue.main.async {
                        self.addingDocIDView.tableData.reloadData()
                    }
                }
        
            }
    }
    
    
    func didOKButtonTapped() {
        self.dismiss(animated: true)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
}


extension AddingDocIDController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listIdDoc.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "docCell")
        let data = listIdDoc[indexPath.row]
        cell?.textLabel?.text = "\(data.name) - Mã nhân viên: \(data.id)"
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = listIdDoc[indexPath.row]
        self.delegate?.passingDocID(data: data.id)
        
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
      
}
