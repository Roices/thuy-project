//
//  EditBookingController.swift
//  ThuY-Project
//
//  Created by Tuan on 19/05/2022.
//

import Foundation
import UIKit
import Alamofire

class EditBookingController: ViewControllerCustom, AlertNoOptionViewControllerDelegate {

    
    
    // MARK: - View
    let editBookingView = UINib(nibName: "CustomerInfomation", bundle: nil).instantiate(withOwner: EditBookingController.self, options: nil)[0] as! CustomerInfomation
    
    lazy var url = "\(BaseURL.scheduleURL)"
    
    var id:Int? = nil
    
    var infomation:Booking? = nil
    
    var statusToken = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        
        setup()
        
    }

    
    
    func setupNavBar(){
            title = "Chỉnh sửa lịch khám"
        
            let button = NavCustomButton(image: Image.backButton)
        
            button.addTarget(self, action: #selector(popView), for: .touchUpInside)
        
            let leftItemButton = UIBarButtonItem(customView: button)
        
            self.navigationItem.leftBarButtonItem = leftItemButton
    }
    
    
    func setup(){
        // Set frame & height for customerInfoView
        editBookingView.frame = CGRect(x: 0,
                            y: 0,
                            width: UIScreen.main.bounds.width,
                            height: UIScreen.main.bounds.height)
        
        editBookingView.delegate = self
        
        self.view.addSubview(editBookingView)
        
        self.hideKeyboardWhenTappedAround()
    }
    
    
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
    }
}


// MARK: - ACTION --
extension EditBookingController: CustomerInfomationDelegate,AddingDocIdProtocol, AddingPetIdProtocol {
    
    func didDocIDtfTapped() {
        let docIDController = AddingDocIDController()
        
        docIDController.delegate = self
 
        self.navigationController?.pushViewController(docIDController, animated: true)
    }
    
    func didPetIDtfTapped() {
        let petIDController = AddingPetIDController()
        
        petIDController.delegate = self
        
        self.navigationController?.pushViewController(petIDController, animated: true)
    }
    
    //get DoctorID from another controller
    func passingDocID(data: String) {
        editBookingView.doctorID.text = data
    }
    
    //get petID from another controller
    func passingPetID(data: String) {
        editBookingView.petID.text = data
    }
        
    //update schedule booking
    func register(doctorID: String, petID: String, date: String, time: String, status: String, symptom: String, note: String) {
        
        showLoadingView()
        
        guard let id = id else { return }
        
        guard let userID =  UserDefaults.standard.string(forKey: "userID") else { return }
                
        let headers = APIService.shared.getHeader()
        
        let param = ["date": date,
                     "time": time,
                     "symptom": symptom,
                     "note": note,
                     "status": status,
                     "planDocId":nil,
                     "realDocId":doctorID,
                     "petId": petID,
                     "userId":userID] as [String : Any?]
        
        let parameters = param.compactMapValues { $0 }

        APIService.shared.requestWith(url: "\(url)/\(id)",
                                      method: .put,
                                      parameters: parameters,
                                      objectTyple: AddingBooking.self,
                                      headers: headers,
                                      encoding: JSONEncoding.default) { [self] isSuccess, json, statusCode in
            
            guard let status = statusCode else { return }
            
            self.dismissLoadingView()
            
            if status == 200 {
                self.presentAlertNoOptionOnMainThread(content: "Sửa thành công") { [self] vc in
                    statusToken = "Success"
                    vc.delegate = self
                }
                
            }else {
                self.presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { [self] vc in
                    statusToken = "AccessTokenDied"
                    vc.delegate = self
                }
            }
            
        }
        
    }
    
    // set Textfield is empty
    func setTfEmptyWhenSuccess() {
        editBookingView.doctorID.text = ""
        editBookingView.petID.text = ""
        editBookingView.status.text = ""
        editBookingView.symptom.text = ""
        editBookingView.date.text = ""
        editBookingView.time.text = ""
        editBookingView.note.text = ""
        editBookingView.note.textColor = .lightGray
    }
    
    //fecth data to label
    func configDataForUI() {
        guard let infomation = infomation else { return }
        var status = ""
        
        switch infomation.status{
        case "cancel":
            status = "Đã hủy"
        case "process":
            status = "Đang tiến hành"
        case "success":
            status = "Đã nhận"
        default:
            status = ""
        }
        
        if infomation.additionalDetail?.planDocId == nil {
            editBookingView.doctorID.text = infomation.additionalDetail?.realDocId
        } else {
            editBookingView.doctorID.text = infomation.additionalDetail?.planDocId
        }
        editBookingView.petID.text = infomation.pet.id
        editBookingView.date.text = infomation.date
        editBookingView.time.text = infomation.time
        editBookingView.symptom.text = infomation.symptom
        editBookingView.status.text = status
        
        if infomation.note == "Không có" {
            editBookingView.note.text = "Ghi chú"
            
        } else {
            editBookingView.note.text = infomation.note
            editBookingView.note.textColor = .black
        }
    }

    func didOKButtonTapped() {
        if statusToken == "Success" {
        self.dismiss(animated: true)
        self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
