//
//  BookingDetailController.swift
//  ThuY-Project
//
//  Created by Tuan on 04/05/2022.
//

import UIKit
import Alamofire

class BookingDetailController: ViewControllerCustom, AlertNoOptionViewControllerDelegate {
    
    // MARK: - View
    let bookingDetailView = UINib(nibName: "BookingDetail", bundle: nil).instantiate(withOwner: BookingDetailController.self, options: nil)[0] as! BookingDetail
    
    lazy var editBookingController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "EditBookingController") as! EditBookingController
    
    // MARK: - Properties
    var infomation:Booking? = nil
    
    lazy var url = "\(BaseURL.scheduleURL)"
    
    var id:Int? = nil
    
    var userReceveiNotiId: String? = nil
    
    lazy var statusBtn = ""
    
    var isRole: String = "EMP" {
        didSet {
            if isRole == "EMP"{
                bookingDetailView.editButton.setTitle("Chỉnh sửa", for: .normal)
                bookingDetailView.cancelButton.isEnabled = true
                bookingDetailView.cancelButton.backgroundColor = UIColor(hexString: "DE6161")
            } else {
                bookingDetailView.editButton.setTitle("Nhận lịch", for: .normal)
                bookingDetailView.cancelButton.isEnabled = false
                bookingDetailView.cancelButton.backgroundColor = .gray
            }
        }
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavBar()
        
        setup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        guard let role = UserDefaults.standard.string(forKey: "role") else { return }
        
        isRole = role
        
        getScheduleDetail()
    
    }
    
    func configDataForUI() {
        guard let info = infomation else { return }
        
        var status = ""
        
        switch info.status{
        case "cancel":
            status = "Đã hủy"
        case "process":
            status = "Đang tiến hành"
        case "success":
            status = "Đã hoàn thành"
        default:
            status = ""
        }
        
        if info.additionalDetail?.planDocId == nil {
            bookingDetailView.doctorName.text = info.additionalDetail?.realDocName
            bookingDetailView.doctorSpecialist.text = info.additionalDetail?.realDocSpec
            bookingDetailView.doctorExperience.text = info.additionalDetail?.realDocPhone
            
        } else {
            bookingDetailView.doctorName.text = info.additionalDetail?.planDocName
            bookingDetailView.doctorSpecialist.text = info.additionalDetail?.planDocSpec
            bookingDetailView.doctorExperience.text = info.additionalDetail?.planDocphone
        }

        bookingDetailView.address.text = info.user.branch.address

        bookingDetailView.date.text = info.date

        bookingDetailView.time.text = info.time

        bookingDetailView.customer.text = info.additionalDetail?.customerName

        bookingDetailView.petName.text = info.pet.name

        bookingDetailView.type.text = info.pet.species[0].name
        
        bookingDetailView.status.text = status

        bookingDetailView.symptom.text = info.symptom

        bookingDetailView.note.text = info.note
        
        
    }

    
}


// MARK: - Setup View ---------------
extension BookingDetailController {
    
    // get method -------------------
    func getScheduleDetail() {

        showLoadingView()
        
        guard let id = self.id else { return }
        
        let headers = APIService.shared.getHeader()
        
        APIService.shared.requestWith(url: "\(BaseURL.scheduleURL)/\(id)",
                                      method: .get,
                                      parameters: nil,
                                      objectTyple: BaseResponse<Booking>.self,
                                      headers: headers,
                                      encoding: URLEncoding.default) { [self] isSuccess, json, statusCode in
            
            guard let status = statusCode, let data = json as? BaseResponse<Booking> else { return }
            self.dismissLoadingView()
            
            if status != 200 {
                self.presentAlertNoOptionOnMainThread(content: "Đã hết phiên đăng nhập") { vc in
                    vc.delegate = self
                }
            }else{
                statusBtn = "Success"
                self.infomation = data.data[0]
                self.configDataForUI()
            }
            
        }

    }
    
    func setup() {

        bookingDetailView.frame = CGRect(x: 0,
                                         y: 0,
                                         width: UIScreen.main.bounds.width,
                                         height: UIScreen.main.bounds.height)
        
        bookingDetailView.delegate = self
        
        title = "Chi tiết lịch khám"
        
        self.view.addSubview(bookingDetailView)
    }
    
    func setupNavBar() {
            title = "Chi tiết lịch khám"
        
            let button = NavCustomButton(image: Image.backButton)
        
            button.addTarget(self, action: #selector(popView), for: .touchUpInside)
        
            let leftItemButton = UIBarButtonItem(customView: button)
        
            self.navigationItem.leftBarButtonItem = leftItemButton
    }
    
    @objc func popView() {
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - API Action

extension BookingDetailController: BookingDetailDelegate {
    
    func cancelSchedule() {
        
        guard let id = id else { return }
        
        let headers = APIService.shared.getHeader()
        
        APIService.shared.requestWith(url: "\(url)/\(id)",
                                      method: .delete,
                                      parameters: nil,
                                      objectTyple: Booking.self,
                                      headers: headers,
                                      encoding: URLEncoding.default) { isSuccess, json, statusCode in
            
            guard let status = statusCode else { return }
            
            if status == 200 {
                self.statusBtn = "Success"
                self.presentAlertNoOptionOnMainThread(content: "Xóa thành công") { vc in
                    vc.delegate = self
                }
            }else {
                self.presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { vc in
                    vc.delegate = self
                }
                
            }
            
        }
    }
    
    func didOKButtonTapped() {
        if statusBtn == "Success" || statusBtn == "Received Success"{
        self.dismiss(animated: true)
        self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true)
            self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    func editSchedule() {
        guard let role = UserDefaults.standard.string(forKey: "role") else { return }
        switch role {
        case "DOC":
            guard let userIdBooking = infomation?.user.id else {return}
            sendingNotiToUser(userID: "\(userIdBooking)")
        case "EMP":
            guard let id = id else { return }
            editBookingController.infomation = self.infomation
            editBookingController.id = id
            editBookingController.configDataForUI()
            navigationController?.pushViewController(editBookingController, animated: true)
        default:
            print("Default")
        }

    }
    
    func sendingNotiToUser(userID: String) {
        
        guard let userName = UserDefaults.standard.string(forKey: "name") else { return }
        
        let header = APIService.shared.getHeader()
        
        let url = "\(BaseURL.sendingNotiURL)?userId=\(userID)"
        
        let currentTime = Date()
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-dd-mm HH:mm"
        let currentTimeFormat = dateformatter.string(from: currentTime)
        
        let param = ["title": "Lịch khám",
                     "message": "Bác sĩ \(userName) đã nhận lịch khám của bạn.",
                     "topic": "Nhận lịch",
                     "dateString": "\(currentTimeFormat)"] as [String : Any?]
        
        let parameters = param.compactMapValues { $0 }
        
        APIService.shared.requestWith(url: url,
                                      method: .post,
                                      parameters: parameters,
                                      objectTyple: validateError.self,
                                      headers: header,
                                      encoding: JSONEncoding.default) { [self] isSuccess, json, statusCode in
            guard let status = statusCode else { return }
            
            if status == 200 {
                statusBtn = "Success"
                presentAlertNoOptionOnMainThread(content: "Nhận lịch khám thành công!") { vc in
                    vc.delegate = self
                }
                
            } else {
                statusBtn = "AccessTokenDied"
                presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { vc in
                    vc.delegate = self
                }
            }

        }
        
        
    }
    
    
    
}
