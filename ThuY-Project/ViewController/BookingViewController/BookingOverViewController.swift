//
//  BookingViewController.swift
//  ThuY-Project
//
//  Created by AnhLe on 27/04/2022.
//

import UIKit
import Alamofire

class BookingOverViewController: ViewControllerCustom, AlertNoOptionViewControllerDelegate {
    

    // MARK: - Subview
    let bookingOverview = UINib(nibName: "BookingOverview", bundle: nil).instantiate(withOwner: BookingOverViewController.self, options: nil)[0] as! BookingOverview
    
    lazy var bookingDetailController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "BookingDetailController") as! BookingDetailController
    
    lazy var customerBookingController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomerBookingController") as! CustomerBookingController
    
    lazy var page = 1
    
    lazy var size = 100
    // MARK: - Properties
    let spacingOfCell = 7.0
    
    lazy var scheduleURl = "\(BaseURL.scheduleURL)"
    
    lazy var refreshControl = UIRefreshControl()
    
    lazy var param = [String: String]()
    
    private let notFoundLabel = LabelCustom(color: .redTY, fontFamily: UIFont.preferredFont(forTextStyle: .headline), alignment: .center)
    
    // MARK: - Lifecycle
    
    var isFoundData: Bool = true{
        didSet{
            displayNotFoundLabelData()
        }
    }
    
    var isRole: String = "EMP" {
        didSet {
            if isRole == "EMP"{
            
            bookingOverview.addingButton.isHidden = false
            } else {
        
            bookingOverview.addingButton.isHidden = true
            }
        }
    }
    
    
    var listSchedule: [Booking] = [] {
        didSet {
            if isViewLoaded {
                bookingOverview.tableSchedule.reloadData()
            }
        }
    }
    
    var status = ""
        // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
//
        setup()
        
        setupTableView()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        guard let role = UserDefaults.standard.string(forKey: "role") else { return }
        
        isRole = role
        
        showLoadingView()
    
        let group = DispatchGroup()
        group.enter()
        getDataScheduleBooking()
        group.leave()
        group.notify(queue: .main) {
            self.refreshControl.endRefreshing()
        }
        
    }
    

}
// MARK: - Set up for view
extension BookingOverViewController: BookingOverviewDelegate {

    func addingSchedule() {
        customerBookingController.customerInfoView.status.text = "Đang thực hiện"
        customerBookingController.customerInfoView.status.isEnabled = false
        self.navigationController?.pushViewController(customerBookingController, animated: true)
    }

    // setup for Controller ----------------
    func setup() {
    
        title = "Lịch khám"
    
        bookingOverview.frame = CGRect(x: 0,
                                       y: 0,
                                       width: UIScreen.main.bounds.width,
                                       height: UIScreen.main.bounds.height)
        
        bookingOverview.delegate = self
        
        refreshControl.addTarget(self, action: #selector(refreshTableView(_:)), for: .valueChanged)
        
        notFoundLabel.text = "Không có dữ liệu nào!"
        notFoundLabel.alpha = 0
        notFoundLabel.isHidden = true
        
        self.view.addSubview(bookingOverview)
        
        self.view.addSubview(notFoundLabel)
        
        NSLayoutConstraint.activate([
            notFoundLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            notFoundLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
        

    }
    
    
    @objc func refreshTableView(_ sender: UIRefreshControl){
        //waiting for getSchedule successful then end refreshing
        isFoundData = true
        let group = DispatchGroup()
        group.enter()
        getDataScheduleBooking()
        group.leave()
        group.notify(queue: .main) {
            self.refreshControl.endRefreshing()
        }
    }
    
    func displayNotFoundLabelData(){
    
            if isFoundData {
                UIView.animate(withDuration: 0.1, delay: 0, options: .curveEaseInOut) {
                    self.notFoundLabel.alpha = 0
                } completion: { _ in
                    self.notFoundLabel.isHidden = true
                }
            }else{
                self.notFoundLabel.isHidden = false
                self.notFoundLabel.alpha = 1
            }
    
    
        }
    
}

// MARK: - Config Booking Table View

extension BookingOverViewController: UITableViewDelegate, UITableViewDataSource {
    // data for table -----------------------------------------------
    func numberOfSections(in tableView: UITableView) -> Int {
        return listSchedule.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleCell", for: indexPath) as! ScheduleCell
        let data = listSchedule[indexPath.section]
        
        cell.customerName.text = data.additionalDetail?.customerName
        cell.timeBooking.text = data.time
        cell.titleSchedule.text = data.symptom
        
        guard let planDoc = data.additionalDetail?.planDocName else { return cell }
        
        if planDoc.isEmpty {
            cell.doctorName.text = data.additionalDetail?.realDocName
        } else {
            cell.doctorName.text = data.additionalDetail?.planDocName
        }
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        cell.selectedBackgroundView = backgroundView
        
        return cell
    }
    // View and Action of Table ---------------------------------------------
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        bookingDetailController.infomation = listSchedule[indexPath.section]
        bookingDetailController.id = listSchedule[indexPath.section].id
        self.navigationController?.pushViewController(bookingDetailController, animated: true)
    }
    
    func setupTableView() {
        
        let nib = UINib(nibName: "ScheduleCell", bundle: nil)
        self.bookingOverview.tableSchedule.register(nib, forCellReuseIdentifier: "ScheduleCell")
        bookingOverview.tableSchedule.backgroundColor = .clear
        bookingOverview.tableSchedule.dataSource = self
        bookingOverview.tableSchedule.delegate = self
        bookingOverview.tableSchedule.showsVerticalScrollIndicator = false
        bookingOverview.tableSchedule.refreshControl = refreshControl
        
        //
    }
    
    // get data if scroll to the last 
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if indexPath.section == listSchedule.count - 1 && listSchedule.count == 10 {
//            page = page + 1
//            getDataScheduleBooking()
//            
//        }
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(spacingOfCell)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat(spacingOfCell)
    }
}


// MARK: - API Action

extension BookingOverViewController {
    
    func didOKButtonTapped() {
        self.dismiss(animated: true)
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func searchData(data: String) {

        let url = "\(scheduleURl)/search"
    
        param = ["page": "\(page)", "size": "10", "name": data]

        listSchedule.removeAll()
        
        showLoadingView()
        
        let headers = APIService.shared.getHeader()
        
        APIService.shared.requestWith(url: url,
                                      method: .get,
                                      parameters: param,
                                      objectTyple: BaseResponse<Booking>.self,
                                      headers: headers,
                                      encoding: URLEncoding.default) { [self] isSuccess, data, statusCode in
            
            guard let status = statusCode, let _data = data as? BaseResponse<Booking> else {
                presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { vc in
                    vc.delegate = self
                }
                return
                
            }
            
            self.dismissLoadingView()
            
            switch status {
            case 200:
                listSchedule = _data.data
                self.isFoundData = listSchedule.isEmpty ? false : true
              
                DispatchQueue.main.async {
                    self.bookingOverview.tableSchedule.reloadData()
                }
                
            default:
                print()

            }
        }
    }
        
        
    
    func getDataScheduleBooking() {
        
        listSchedule.removeAll()
                
        guard let branchID = UserDefaults.standard.string(forKey: "branchID") else { return }
        
        let headers = APIService.shared.getHeader()
        
        param = ["page": "\(page)", "size": "\(size)", "branchId": branchID]
        
        APIService.shared.requestWith(url: scheduleURl,
                                      method: .get,
                                      parameters: param,
                                      objectTyple: BaseResponse<Booking>.self,
                                      headers: headers,
                                      encoding: URLEncoding.default) { [self] isSuccess, data, statusCode in
            
            guard let status = statusCode, let _data = data as? BaseResponse<Booking> else {
                presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { vc in
                    vc.delegate = self
                }
                return
                
            }
            
            DispatchQueue.main.async {
                self.dismissLoadingView()
            }
            
            switch status {
            case 200:
                listSchedule = _data.data
                listSchedule = listSchedule.reversed()
                self.isFoundData = listSchedule.isEmpty ? false : true
                DispatchQueue.main.async {
                    self.bookingOverview.tableSchedule.reloadData()
                }
            default:
                print()
            }

        }
    }
    
    
    
    func dateDidChange(date: String, time: String) {
        
        showLoadingView()
        
        listSchedule.removeAll()
        
        let param = ["page": page,
                     "size": size,
                     "date": date,
                     "time": "\(time)"] as [String : Any]
        
        let parameters = param.compactMapValues { $0 }
        
        let headers = APIService.shared.getHeader()
        
        APIService.shared.requestWith(url: "\(BaseURL.scheduleURL)/searchDateTime",
                                      method: .get,
                                      parameters: parameters,
                                      objectTyple: BaseResponse<Booking>.self,
                                      headers: headers,
                                      encoding: URLEncoding.default) { [self] isSuccess, json, statusCode in
            
            guard let data = json as? BaseResponse<Booking>,
                  let status = statusCode else {
                presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { vc in
                    vc.delegate = self
                }
                return
            }
            
            self.dismissLoadingView()
            
            if status == 200 {
                listSchedule = data.data
                isFoundData = listSchedule.isEmpty ? false : true
                
                DispatchQueue.main.async {
                    self.bookingOverview.tableSchedule.reloadData()
                }
            }
            
        }
        
    }
}


