//
//  BookingDoctorController.swift
//  ThuY-Project
//
//  Created by Tuan on 30/04/2022.
//

import UIKit

class BookingDoctorController: UIViewController {

    let bookingDoctor = UINib(nibName: "BookingDoctor", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! BookingDoctor
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavBar()
        self.hideKeyboardWhenTappedAround()
        setup()
    }
    
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}


// MARK: - Extension
extension BookingDoctorController: BookingDoctorDelegate {
    func bookingSchedule() {
       
    }
    
    
    func setup(){
        // Title for controller -------
        title = "Đăng ký lịch khám"
        
        // Set frame for BookingDoctorView -----
        bookingDoctor.frame = CGRect(x: 0,
                            y: 0,
                            width: UIScreen.main.bounds.width,
                            height: UIScreen.main.bounds.height)
        bookingDoctor.delegate = self
        self.view.addSubview(bookingDoctor)
    }
    
    func setupNavBar(){
            title = "Đăng ký lịch khám"
            let button = NavCustomButton(image: Image.backButton)
            button.addTarget(self, action: #selector(popView), for: .touchUpInside)
            let leftItemButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = leftItemButton
    }
    
    @objc func popView() {
        bookingDoctor.doctorName.withImage(direction: .Nil, image: nil)
        bookingDoctor.dateBooking.withImage(direction: .Nil, image: nil)
        bookingDoctor.startTime.withImage(direction: .Nil, image: nil)
        bookingDoctor.endTime.withImage(direction: .Nil, image: nil)
        bookingDoctor.specialist.withImage(direction: .Nil, image: nil)
        self.navigationController?.popViewController(animated: true)
    }
}
