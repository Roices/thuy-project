//
//  CustomerBookingController.swift
//  ThuY-Project
//
//  Created by Tuan on 29/04/2022.
//

import Foundation
import UIKit
import Alamofire

class CustomerBookingController: ViewControllerCustom, AlertNoOptionViewControllerDelegate {

    // MARK: - Subview
    let customerInfoView = UINib(nibName: "CustomerInfomation", bundle: nil).instantiate(withOwner: CustomerBookingController.self, options: nil)[0] as! CustomerInfomation
    
    var statusToken = ""
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    
        setup()
        setupNavBar()
        
    }
    
    func setup(){
        // Set frame & height for customerInfoView
        customerInfoView.frame = CGRect(x: 0,
                            y: 0,
                            width: UIScreen.main.bounds.width,
                            height: UIScreen.main.bounds.height)
        
        customerInfoView.delegate = self
        
        self.view.addSubview(customerInfoView)
        self.hideKeyboardWhenTappedAround()
    }
    
}


// MARK: - Extension
extension CustomerBookingController: CustomerInfomationDelegate, AddingDocIdProtocol, AddingPetIdProtocol{
    
    
    func passingPetID(data: String) {
        customerInfoView.petID.text = data
    }
    
    func passingDocID(data: String) {
        customerInfoView.doctorID.text = data
    }
    
    func didDocIDtfTapped() {
        let docIDController = AddingDocIDController()
        self.setTfDefaultWhenError()
        docIDController.delegate = self
     
        self.navigationController?.pushViewController(docIDController, animated: true)
    }
    
    func didPetIDtfTapped() {
        let petIDController = AddingPetIDController()
        self.setTfDefaultWhenError()
        
        petIDController.delegate = self
        self.navigationController?.pushViewController(petIDController, animated: true)
    }
    
    
    // MARK: - POST
    func register(doctorID: String, petID: String, date: String, time: String, status: String, symptom: String, note: String) {

        showLoadingView()
        
        guard let userID =  UserDefaults.standard.string(forKey: "userID") else { return }
        
        let headers = APIService.shared.getHeader()
        
        let param = ["date": date,
                     "time": time,
                     "symptom": symptom,
                     "note": note,
                     "status": "2",
                     "planDocId":doctorID,
                     "realDocId":nil,
                     "petId": petID,
                     "userId":userID] as [String : Any?]
        
        let parameters = param.compactMapValues { $0 }
        
        
        APIService.shared.requestWith(url: "\(BaseURL.scheduleURL)",
                                      method: .post,
                                      parameters: parameters,
                                      objectTyple: Base.self,
                                      headers: headers,
                                      encoding: JSONEncoding.default) { [self] isSuccess, json, statusCode in
            
            guard let status = statusCode, let data = json as? Base else {
                statusToken = "AccessTokenDied"
                self.presentAlertNoOptionOnMainThread(content: "Hết phiên đăng nhập") { vc in
                    vc.delegate = self
                }
                return
                
            }
            
            self.dismissLoadingView()

            if status == 200 {
                self.presentAlertNoOptionOnMainThread(content: "Thêm thành công") { [self] vc in
                    statusToken = "Success"
                    let bookingID = data.data[0].id
                    self.sendingNotiToDoctor(userID: doctorID, bookingID: bookingID)
                    vc.delegate = self
                    
                }

            }
        }
    }
    
    
    // Sending Remote Notification to Doctor
    func sendingNotiToDoctor(userID: String, bookingID: Int) {
        
        guard let userName = UserDefaults.standard.string(forKey: "name") else { return }
        
        let header = APIService.shared.getHeader()
        
        let url = "\(BaseURL.sendingNotiURL)?userId=\(userID)"
        
        let currentTime = Date()
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-dd-mm HH:mm"
        let currentTimeFormat = dateformatter.string(from: currentTime)
        
        let param = ["title": "Yêu cầu nhận lịch khám",
                     "message": "Nhân viên \(userName) đã gửi yêu cầu đặt lịch khám",
                     "topic": "\(bookingID)",
                     "dateString": "\(currentTimeFormat)"] as [String : Any?]
        
        let parameters = param.compactMapValues { $0 }
        
        APIService.shared.requestWith(url: url,
                                      method: .post,
                                      parameters: parameters,
                                      objectTyple: validateError.self,
                                      headers: header,
                                      encoding: JSONEncoding.default) { isSuccess, json, statusCode in
            guard let data = json else { return }
            
            print(data)
       
        }
        
        
    }
  
    
    func setupNavBar(){
            title = "Thông tin khách hàng"
            let button = NavCustomButton(image: Image.backButton)
            button.addTarget(self, action: #selector(popView), for: .touchUpInside)
            let leftItemButton = UIBarButtonItem(customView: button)
            self.navigationItem.leftBarButtonItem = leftItemButton
    }
    
    
    func didOKButtonTapped() {
        if statusToken == "statusToken" {
            self.dismiss(animated: true)
            self.setTfEmptyWhenSuccess()
            self.navigationController?.popToRootViewController(animated: true)
        } else {
        self.dismiss(animated: true)
        self.setTfEmptyWhenSuccess()
        self.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    @objc func popView() {
        setTfDefaultWhenError()
        setTfEmptyWhenSuccess()
        self.navigationController?.popViewController(animated: true)

    }

    func setTfEmptyWhenSuccess() {
        customerInfoView.petID.text = ""
        customerInfoView.doctorID.text = ""
        customerInfoView.date.text = ""
        customerInfoView.time.text = ""
        customerInfoView.symptom.text = ""
        customerInfoView.status.text = ""
        customerInfoView.note.text = "Ghi chú"
        customerInfoView.note.textColor = .lightGray
    }
    
    func setTfDefaultWhenError() {
        customerInfoView.petID.withImage(direction: .Nil, image: nil)
        customerInfoView.doctorID.withImage(direction: .Nil, image: nil)
        customerInfoView.date.withImage(direction: .Nil, image: nil)
        customerInfoView.time.withImage(direction: .Nil, image: nil)
        customerInfoView.symptom.withImage(direction: .Nil, image: nil)
        customerInfoView.status.withImage(direction: .Nil, image: nil)
    }
}
