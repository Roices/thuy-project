//
//  ButtonCustom.swift
//  ThuY-Project
//
//  Created by AnhLe on 04/05/2022.
//

import UIKit

class ButtonCustom: UIButton {
    // MARK: - Subview
    
    // MARK: - Properties
    // MARK: - Lifecycle
    convenience init(title: String, color: UIColor) {
        self.init(frame: .zero)
        self.setTitle(title, for: .normal)
        backgroundColor = color
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //give default size, if they put into stackview, this view will know how to size itself
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: 42)
    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension

extension ButtonCustom {
    private func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        self.layer.cornerRadius = 5
        self.setTitleColor(.white, for: .normal)
        
        self.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
    }
    
    private func layout(){
        
    }
}

