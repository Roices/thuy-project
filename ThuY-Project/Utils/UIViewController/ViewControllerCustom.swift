//
//  ViewControllerCustom.swift
//  ThuY-Project
//
//  Created by AnhLe on 27/04/2022.
//
//.
import UIKit

class ViewControllerCustom: UIViewController {
    // MARK: - Subview
    
    // MARK: - Properties
    var containerView: UIView!

    
    // MARK: - Lifecycle
    

    
    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
 
    func bringVCtoParent(from vc: UIViewController, to container: UIView){
        addChild(vc)
        container.addSubview(vc.view)
        vc.view.frame = container.bounds
        didMove(toParent: self)
    }
    
    func showLoadingView(){
        containerView = UIView()

        view.addSubview(containerView)
        
        containerView.backgroundColor = .systemBackground
        containerView.alpha = 0
        
        UIView.animate(withDuration: 0.25) {
            self.containerView.alpha = 0.8
        }
        
        containerView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: view.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        
        ])
        
        let activityIndicator = UIActivityIndicatorView(style: .large)
        containerView.addSubview(activityIndicator)
        
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            activityIndicator.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        
        ])
        
        activityIndicator.startAnimating()
    }
    
    func dismissLoadingView(){
        DispatchQueue.main.async {
            self.containerView?.removeFromSuperview()
//            self.containerView = nil
        }
    }
}
// MARK: - Extension

