//
//  Constant.swift
//  ThuY-Project
//
//  Created by AnhLe on 27/04/2022.
//

import UIKit

enum Image {
    static let home = UIImage(systemName: "house.fill")!
    static let bell = UIImage(systemName: "bell.fill")!
    static let calendar = UIImage(systemName: "calendar")!
    static let people = UIImage(systemName: "person.2.fill")!
    static let threeLine = UIImage(named: "threeLine")!
    static let backButton = UIImage(named: "btn_Back")!
    static let branch = UIImage(named: "branch")!
    static let pet = UIImage(named: "pet")!
    static let multiply = UIImage(systemName: "multiply")!
    static let logout = UIImage(named: "logout")!
    static let search = UIImage(systemName: "magnifyingglass")!
    static let add = UIImage(systemName: "plus")!
    static let back = UIImage(systemName: "chevron.left")!
    static let warning = UIImage(systemName: "exclamationmark.triangle.fill")!
    static let edit = UIImage(systemName: "edit")
    static let circle = UIImage(systemName: "circle")!
    
    static let checkmark = UIImage(systemName: "checkmark.circle.fill")!
    static let filter = UIImage(systemName: "slider.horizontal.3")!
}



