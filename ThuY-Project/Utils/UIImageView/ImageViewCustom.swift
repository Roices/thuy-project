//
//  ImageViewCustom.swift
//  ThuY-Project
//
//  Created by AnhLe on 30/04/2022.
//

import UIKit

class ImageViewCustom: UIImageView {
    // MARK: - Subview
    
    // MARK: - Properties
    
    // MARK: - Lifecycle
//    convenience init(img: UIImage) {
//        self.init(frame: .zero)
//        let configuration = UIImage.SymbolConfiguration(scale: .large)
//        image = img.withConfiguration(configuration)
//    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension
extension ImageViewCustom {
    
    func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        contentMode = .scaleAspectFit
        tintColor = .white
        let configuration = UIImage.SymbolConfiguration(scale: .large)
        image?.withConfiguration(configuration)
    }
    func layout(){
        
    }
}
