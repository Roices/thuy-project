//
//  LabelCustom.swift
//  ThuY-Project
//
//  Created by AnhLe on 27/04/2022.
//

import UIKit

class LabelCustom: UILabel {
    // MARK: - Subview
    
    // MARK: - Properties
    
    // MARK: - Lifecycle
    convenience init(color: UIColor, fontFamily: UIFont, alignment: NSTextAlignment) {
        self.init(frame: .zero)
        textColor = color
        font = fontFamily
        textAlignment = alignment
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension
extension LabelCustom {
    
    func setup(){
        translatesAutoresizingMaskIntoConstraints = false
    }
    func layout(){
        
    }
}

