//
//  NavButtonCustom.swift
//  ThuY-Project
//
//  Created by AnhLe on 27/04/2022.
//

import UIKit

class NavButtonCustom: UIButton {
    // MARK: - Subview
    
    // MARK: - Properties
    
    // MARK: - Lifecycle
    convenience init(image: UIImage) {
        self.init(frame: .zero)
        setImage(image, for: .normal)
        imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        tintColor = .darkGrayTY
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    // MARK: - Selector
    
    // MARK: - API
    
    // MARK: - Helper
}
// MARK: - Extension
extension NavButtonCustom {
    
    func setup(){
        translatesAutoresizingMaskIntoConstraints = false
        layer.cornerRadius = 5
        backgroundColor = .lightSecondaryGrayTY
        
        
    }
    func layout(){
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: 32),
            self.widthAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1)
        
        ])
    }
}

