//
//  HandlePush.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 29/04/2022.
//

import Foundation
import UIKit

class HandlePush: NSObject {
    func gotoAddBreedViewController(root : UIViewController, arr: [String]) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "AddBreedViewController") as! AddBreedViewController
        vc.arrDropdown = arr
        root.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoUpdateBreedViewController(root : UIViewController,obj : BreedDataModel, id: String, arr : [String]) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "UpdateBreedViewController") as! UpdateBreedViewController
        vc.obj = obj
        vc.id = id
        vc.arrDropdown = arr
        root.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoAddSpeciesViewController(root : UIViewController) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "AddSpeciesViewController") as! AddSpeciesViewController
        root.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoUpdateSpeciesViewController(root : UIViewController , obj: SpeciesDataModel, id: String) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "UpdateSpeciesViewController") as! UpdateSpeciesViewController
        vc.obj = obj
        vc.id = id
        root.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoCustomerViewController(root : UIViewController) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "CustomerViewController") as! CustomerViewController
        root.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoAddCustomerViewController(root : UIViewController) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "AddCustomerViewController") as! AddCustomerViewController
        root.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoUpdateCustomerViewController(root : UIViewController, obj: CustomerDataModel) {
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "UpdateCustomerViewController") as! UpdateCustomerViewController
        vc.obj = obj
        root.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoSpeciesViewController(root: UIViewController){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "SpeciesViewController") as! SpeciesViewController
        root.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoUpdatePetsViewController(root: UIViewController, obj: PetDataModel){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "UpdatePetsViewController") as! UpdatePetsViewController
        vc.obj = obj
        root.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoAddPetsViewController(root: UIViewController, id: String){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "AddPetsViewController") as! AddPetsViewController
        vc.id = id
        root.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoDetailCustomerViewController(root: UIViewController, id: String, obj: CustomerDataModel){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "DetailCustomerViewController") as! DetailCustomerViewController
        vc.obj = obj
        vc.idCustomer = id
        root.navigationController?.pushViewController(vc, animated: true)
    }
    
    func gotoViewControllerSpecies(obj: SpeciesModel){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "SpeciesViewController") as! SpeciesViewController
        vc.objSpecies = obj
        vc.viewSpecies.tbView.reloadData()
    }
    
    func gotoViewControllerBreed(obj: BreedModel){
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc  = storyboard.instantiateViewController(withIdentifier: "SpeciesViewController") as! SpeciesViewController
        vc.objBreed = obj
        vc.viewSpecies.tbView.reloadData()
    }
}


