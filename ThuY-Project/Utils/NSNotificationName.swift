//
//  NSNotificationName.swift
//  ThuY-Project
//
//  Created by AnhLe on 29/04/2022.
//

import Foundation
extension Notification.Name {
    static let sideMenu = Notification.Name("sideMenu")
    static let updated = Notification.Name("updated")
    static let longCellPressed = Notification.Name("longPress")
    static let cancelLongPressed = Notification.Name("cancelLongPress")
    static let selectAllTapped = Notification.Name("selectAllTapped")
    static let unSelectAllTapped = Notification.Name("unSelectAllTapped")
    static let didTokenExpired = Notification.Name("didTokenExpired")
    
}
