//
//  String+Utils.swift
//  ThuY-Project
//
//  Created by AnhLe on 09/05/2022.
//

import UIKit

extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {

        let font = font
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: .max))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.text = self
        label.sizeToFit()
        return label.frame.height
    }
    
    func findNumberWordInSentence() -> Int{
        let res = self.split(separator: " ")
        return res.count
    }
    
    func widthOfText(font: UIFont) -> CGFloat{
        let fontAttributes = [NSAttributedString.Key.font: font]
        let text = self
        let size = (text as NSString).size(withAttributes: fontAttributes)
        return size.width
    }
    func splitAddressToAddressArray() -> (detail: String, ward: String, district: String, province: String){
        let array = self.split(separator: ",")
            
        let result = array.map({String($0)})
        if result.count < 4 {
            return ("","","","");
        }
        return (result[0], result[1], result[2], result[3])
    }
    
    
    func getNumberFromID() -> Int{
        let array = Array(self)
        let number = array[2..<array.count]
        return Int(String(number)) ?? 0
        
    }
}
