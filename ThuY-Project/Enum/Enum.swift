//
//  Enum.swift
//  ThuY-Project
//
//  Created by Lê Đình Đạt on 28/04/2022.
//

import Foundation
import UIKit

enum species {
    case giong
    case loai
}

enum customer {
    case customers
    case pets
}

enum MethodApi
{
    case PostApi
    case GetApi
    case PutApi
    case DeleteApi
    case OptionsApi
}
