//
//  Overview.swift
//  ThuY-Project
//
//  Created by AnhLe on 28/04/2022.
//

import UIKit

enum Overview: CaseIterable {
    case branch, pet, staff, treat, owner, booking
    
    var title: String {
        switch self {
        case .branch:
            return "Chi nhánh  "
        case .pet:
            return "Thú nuôi     "
        case .staff:
            return "Nhân viên  "
        case .treat:
            return "Khách hàng"
        case .owner:
            return "Chủ nuôi & vật nuôi"
        case .booking:
            return "Lịch khám"
        }
    }
    
    var color: UIColor {
        switch self {
        case .branch:
            return .greenTY
        case .pet:
            return .blueTY
        case .staff:
            return .yellowTY
        case .treat:
            return .orangeTY
        default:
            return .greenTY
        }
    }
    
    var image: UIImage {
        switch self {
        
        case .branch:
            return Image.branch
        case .pet:
            return Image.pet
        case .staff:
            return Image.people
        case .treat:
            return Image.pet
        case .booking:
            return Image.calendar
        default:
            return Image.people
        }
    }
}
